module.exports = function (grunt) {
  grunt.loadNpmTasks("grunt-contrib-sass");
  grunt.loadNpmTasks("grunt-contrib-uglify");
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks("grunt-contrib-clean");

  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),
    sass: {
      theme: {
        options: {
          style: "compressed",
          compass: false,
        },
        files: {
          "assets/scss/styles.min.css": "assets/scss/styles.scss",
          "templates-blocks-acf/blocks-acf-styles.min.css":
            "templates-blocks-acf/blocks-acf-styles.scss",
        },
      },
      blocks: {
        options: {
          style: "compressed",
          sourceMap: true,
        },
        files: [
          {
            src: [
              "templates-blocks-acf/**/*.scss",
              "!templates-blocks-acf/**/*.min.css",
            ],
            expand: true,
            flatten: false,
            ext: ".min.css",
          },
        ],
      },
    },
    uglify: {
      // ALL INTO ONE FILE
      //   t1: {
      //     options: {
      //       sourceMap: true,
      //     },
      //     files: {
      //       "assets/js/all.min.js": ["assets/js/*.js"],
      //     },
      //   },
      // EVERY FILE IN ONE FOLDER
      theme: {
        options: {
          sourceMap: true,
        },
        files: [
          {
            src: ["assets/js/*.js", "!assets/js/*.min.js"],
            expand: true,
            flatten: false,
            ext: ".min.js",
          },
        ],
      },
      // EVERY FILE IN SUB FOLDER
      blocks: {
        options: {
          sourceMap: true,
        },
        files: [
          {
            src: [
              "templates-blocks-acf/**/*.js",
              "!templates-blocks-acf/**/*.min.js",
            ],
            expand: true,
            flatten: false,
            ext: ".min.js",
          },
        ],
      },
    },
    watch: {
      options: {
        livereload: true,
      },
      cssTheme: {
        files: ["assets/scss/**/*.scss"],
        tasks: ["sass:theme"],
        options: {
          interrupt: true,
        },
      },
      cssBlocks: {
        files: [
          "templates-blocks-acf/**/*.scss",
          "!templates-blocks-acf/**/*.min.css",
        ],
        tasks: ["sass:blocks"],
        options: {
          interrupt: true,
        },
      },
      jsTheme: {
        files: ["assets/js/*.js", "!assets/js/*.min.js"],
        tasks: ["uglify:theme"],
        options: {
          interrupt: true,
        },
      },
      jsBlocks: {
        files: [
          "templates-blocks-acf/**/*.js",
          "!templates-blocks-acf/**/*.min.js",
        ],
        tasks: ["uglify:blocks"],
        options: {
          interrupt: true,
        },
      },
    },
    clean: {
      // dist: ["assets/build/app.min.css", "assets/build/app.min.js"],
    },
  });
};
