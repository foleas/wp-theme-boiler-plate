<?php get_header(); ?>

<?php
$currentObject = get_queried_object();
//print_r( $currentObject );
$taxonomy = $currentObject->taxonomy;
$category = '';
$tags     = '';
$titulo   = $currentObject->name;

if ( $taxonomy === 'post_tag' ) {
	$tags = array( $currentObject->term_id );
} else if ( $taxonomy === 'catgory' ) {
	$category = $currentObject->term_id;
}
$block_custom_id = $currentObject->taxonomy . '_' . $currentObject->term_id;
?>

<?php if ( have_posts() ) :
	//while ( have_posts() ) : the_post(); ?>

	<?php
	// GRID
	$post_grid_block_array = array(
		'categoria'         => $category,
		'tags'              => $tags,
		'mostrar_descatado' => false,
		'mostrar_header'    => true,
		'info_header'       => array(
			'titulo' => $titulo,
		)
	);

	render_acf_block( 'acf/text-domain-posts-grid', $block_custom_id, $post_grid_block_array );
	?>

	<?php //endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>