<?php

/**
 * Two Columns Block Template
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'two-columns-' . $block['id'];
if (!empty($block['anchor'])) {
	$id = $block['anchor'];
}
$className = 'two-columns-block-wrapper';
if (!empty($block['className'])) {
	$className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
	$className .= ' align' . $block['align'];
}

if (!empty($block['textColor'])) {
	//	$className .= ' has-text-color';
	$className .= ' has-' . $block['textColor'] . '-color';
}
if (!empty($block['backgroundColor'])) {
//	$className .= ' has-background';
	$className .= ' has-' . $block['backgroundColor'] . '-background-color';
}
if ($is_preview) {
	$className .= ' is-admin';
}

$info = get_field('info');
$showInfo = false;
foreach ($info as $key => $value) {
	if ($value !== '' && !empty($value)) {
		$showInfo = true;
	}
}
?>

<section id="<?php echo esc_attr($id); ?>" class="with-paddings <?php echo esc_attr($className); ?>">

	<div class="container-custom">
		<div class="two-columns-wrapper">

			<?php
			if ($showInfo):
				echo '<div class="col col-left">';
				$partialsArray = [
					'info' => $info,
//				'buttonColor' => 'primary'
				];
				get_template_part('templates-blocks-acf/partials/info', 'col', $partialsArray);
				echo '</div>';
			endif;
			?>

			<div class="col col-right">
				<InnerBlocks/>
			</div>

		</div>
	</div>
</section>