<?php
//===================================================
// TWO COLUMNS
//===================================================

acf_register_block_type(array(
	'name' => 'text-domain-two-columns',
	'title' => __('text-domain Two Columns', 'text-domain'),
	'description' => __('Bloque para dos columnas', 'text-domain'),
	'render_template' => 'templates-blocks-acf/two-columns/template.php',
	'category' => 'text-domain',
	'icon' => 'columns',
	'keywords' => array('col', 'columns', 'columnas'),
	'mode' => 'preview',
	'supports' => array(
		'align' => false,
		'anchor' => true,
		'jsx' => true,
		'color' => [
			'background' => true,
			'gradients' => false,
			'text' => true,

		],
	),
	'enqueue_assets' => function () {
		// BLOCK
		if (!wp_style_is('text-domain-two-columns-block', 'enqueued')) {
			wp_enqueue_style('text-domain-two-columns-block', get_stylesheet_directory_uri() . '/templates-blocks-acf/two-columns/styles.min.css');
		}
	},
));

//===================================================
// FIELDS
//===================================================

if (function_exists('acf_add_local_field_group')):

	acf_add_local_field_group(array(
		'key' => 'group_6304c78a97248',
		'title' => 'Bloque Two Columns',
		'fields' => array(
			array(
				'key' => 'field_6305174de39ec',
				'label' => 'Info',
				'name' => 'info',
				'type' => 'group',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'layout' => 'block',
				'sub_fields' => array(
					array(
						'key' => 'field_63051ad78622c',
						'label' => 'Título',
						'name' => 'titulo',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_63051ae98622d',
						'label' => 'Texto',
						'name' => 'texto',
						'type' => 'textarea',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'rows' => '',
						'new_lines' => 'br',
					),
					array(
						'key' => 'field_63051af28622e',
						'label' => 'Botón',
						'name' => 'boton',
						'type' => 'link',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',
					),
				),
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'block',
					'operator' => '==',
					'value' => 'acf/text-domain-two-columns',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
		'show_in_rest' => 0,
	));

endif;