import { loadmoreHandler } from "../common/js/loadmore.min.js";
(function ($) {
  //===================================================
  // POSTS GRID BLOCK
  //===================================================
  const initializePostsGrid = function ($blockSection) {
    // console.log($blockSection);
    const postGridWrapper = $blockSection.find(".posts-grid-wrapper");

    // LOAD MORE
    $(".loadmore-btn", postGridWrapper).on("click", function () {
      loadmoreHandler(
        $(this),
        postGridWrapper,
        posts_grid_block_params.ajax_url
      );
    });
  };

  // Initialize each block on page load (front end).
  $(window).on("load", function () {
    if ($(".posts-grid-block-wrapper").length > 0) {
      $(".posts-grid-block-wrapper").each(function () {
        initializePostsGrid($(this));
      });
    }
  });

  // Initialize dynamic block preview (editor).
  if (window.acf) {
    window.acf.addAction(
      "render_block_preview/type=quiniela-posts-grid",
      initializePostsGrid
    );
  }
})(jQuery);
