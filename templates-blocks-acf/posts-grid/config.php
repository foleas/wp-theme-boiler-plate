<?php
//===================================================
// POSTS GRID BLOCK
//===================================================

acf_register_block_type(array(
	'name' => 'text-domain-posts-grid',
	'title' => __('text-domain Posts Grid', 'text-domain'),
	'description' => __('Bloque para Grilla de posts', 'text-domain'),
	'render_template' => 'templates-blocks-acf/posts-grid/template.php',
	'category' => 'text-domain',
	'icon' => 'images-alt',
	'keywords' => array('carrousel', 'gallery'),
	'mode' => 'preview',
	'supports' => array(
		'align' => false,
		'anchor' => true,
	),
	'enqueue_assets' => function () {
		// BLOCK
		if (!is_admin()) {
			if (!wp_script_is('text-domain-posts-grid-block', 'enqueued') && !is_admin()) {
				wp_register_script('text-domain-posts-grid-block', get_stylesheet_directory_uri() . '/templates-blocks-acf/posts-grid/scripts.min.js', '', '', true);
				wp_localize_script('text-domain-posts-grid-block', 'posts_grid_block_params', array(
					'template_directory' => get_bloginfo('template_directory'),
					'ajax_url' => admin_url('admin-ajax.php'),
				));
				wp_enqueue_script('text-domain-posts-grid-block');
			}
		}
		if (!wp_style_is('text-domain-posts-grid-block', 'enqueued')) {
			wp_enqueue_style('text-domain-posts-grid-block', get_stylesheet_directory_uri() . '/templates-blocks-acf/posts-grid/styles.min.css');
		}
	},
));

//===================================================
// FIELDS
//===================================================

if (function_exists('acf_add_local_field_group')):

	acf_add_local_field_group(array(
		'key' => 'group_6297bdd11394f',
		'title' => 'Bloque Post Grid',
		'fields' => array(
			array(
				'key' => 'field_6307915eb9f1b',
				'label' => 'Mostrar Descatado',
				'name' => 'mostrar_descatado',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 0,
				'ui' => 1,
				'ui_on_text' => '',
				'ui_off_text' => '',
			),
			array(
				'key' => 'field_629a3e668e45b',
				'label' => 'Cantidad por Página',
				'name' => 'cantidad_por_pagina',
				'type' => 'number',
				'instructions' => 'Por defecto 12',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => 12,
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => 1,
				'max' => 20,
				'step' => 1,
			),
			array(
				'key' => 'field_629a3e850ee8d',
				'label' => 'Categoría',
				'name' => 'categoria',
				'type' => 'taxonomy',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'taxonomy' => 'category',
				'field_type' => 'select',
				'allow_null' => 1,
				'add_term' => 0,
				'save_terms' => 0,
				'load_terms' => 0,
				'return_format' => 'id',
				'multiple' => 0,
			),
			array(
				'key' => 'field_62a201b80a0b3',
				'label' => 'Tags',
				'name' => 'tags',
				'type' => 'taxonomy',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'wpml_cf_preferences' => 1,
				'taxonomy' => 'post_tag',
				'field_type' => 'multi_select',
				'allow_null' => 1,
				'add_term' => 0,
				'save_terms' => 0,
				'load_terms' => 0,
				'return_format' => 'id',
				'multiple' => 0,
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'block',
					'operator' => '==',
					'value' => 'acf/text-domain-posts-grid',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
		'show_in_rest' => 0,
	));

endif;
