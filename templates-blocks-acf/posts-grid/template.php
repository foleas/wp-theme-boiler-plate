<?php

/**
 * Posts Grid Block Template
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'posts-grid-' . $block['id'];
if ( ! empty( $block['anchor'] ) ) {
	$id = $block['anchor'];
}
$className = 'posts-grid-block-wrapper';
if ( ! empty( $block['className'] ) ) {
	$className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
	$className .= ' align' . $block['align'];
}
if ( $is_preview ) {
	$className .= ' is-admin';
}

$blockData = $block['data'];

$cantidad_por_pagina = ( array_key_exists( 'cantidad_por_pagina', $blockData ) ) ? $blockData['cantidad_por_pagina'] : get_field( 'cantidad_por_pagina' );
$category            = ( array_key_exists( 'categoria', $blockData ) ) ? $blockData['categoria'] : get_field( 'categoria' );
$tags                = ( array_key_exists( 'tags', $blockData ) ) ? $blockData['tags'] : get_field( 'tags' );
$mostrarDescatado    = ( array_key_exists( 'mostrar_descatado', $blockData ) ) ? $blockData['mostrar_descatado'] : get_field( 'mostrar_descatado' );
$mostrarHeader       = ( array_key_exists( 'mostrar_header', $blockData ) ) ? $blockData['mostrar_header'] : false;
$infoHeader          = ( array_key_exists( 'info_header', $blockData ) ) ? $blockData['info_header'] : null;

$postFeaturedID = '';
if ( $mostrarDescatado ) {
	$postFeatured = get_posts(
		array(
			'post_type'   => 'post',
			'numberposts' => 1,
			'category'    => $category,
			'tag__and'    => ( $tags ) ? implode( ", ", $tags ) : array(),
		)
	);
	if ( $postFeatured ) {
		$postFeaturedID = $postFeatured[0]->ID;
	}
}

$posts_query = ( array_key_exists( 'custom_query', $blockData ) ) ? $blockData['custom_query'] : new WP_Query(
	array(
		'post_type'      => 'post',
		'posts_per_page' => $cantidad_por_pagina,
		'cat'            => $category,
		'tag__and'       => ( $tags ) ? implode( ", ", $tags ) : array(),
		'post__not_in'   => array( $postFeaturedID ),
	)
);
?>

<section id="<?php echo esc_attr( $id ); ?>" class="with-paddings <?php echo esc_attr( $className ); ?>">
	<div class="container-custom">

	  <?php
	  if ( $mostrarHeader ):
		  $partialsArray = [
			  'info' => $infoHeader,
		  ];
		  get_template_part( 'templates-blocks-acf/partials/info', 'header', $partialsArray );
	  endif;
	  ?>

	  <?php
	  if ( $mostrarDescatado ) {
		  $postFeaturedArray = array(
			  'postID' => $postFeaturedID
		  );
		  get_template_part( 'includes/post/post', 'featured', $postFeaturedArray );
	  }
	  ?>

	  <?php if ( $posts_query->have_posts() ) : ?>
				<div class="posts-grid-wrapper">
					<div class="posts-grid">
			  <?php
			  while ( $posts_query->have_posts() ) :
				  $posts_query->the_post();
				  echo '<div class="posts-grid-item">';
				  $postItemArgs = array();
				  get_template_part( 'includes/post/post', 'item', $postItemArgs );
				  echo '</div>';
			  endwhile;
			  ?>
					</div>

			<?php
			$paged = $posts_query->query_vars['paged'] ? $posts_query->query_vars['paged'] : 1;
			if ( $paged < $posts_query->max_num_pages ): ?>
							<div class="custom-btn-wrapper with-loader">
								<a class="custom-btn primary loadmore-btn">
									<p><?php _e( 'Cargar más', 'quiniela' ); ?></p>
									<input type="hidden" name="action" value="loadmore"/>
									<input type="hidden" name="nonce" value="<?php echo wp_create_nonce( 'loadmore_nonce' ); ?>"/>
									<input type="hidden" name="post_type" value="post"/>
									<input type="hidden" name="query_vars"
												 value='<?php echo json_encode( $posts_query->query_vars, JSON_HEX_TAG ); ?>'/>
									<input type="hidden" name="paged"
												 value="<?php echo $paged; ?>"/>
									<input type="hidden" name="max_page" value="<?php echo $posts_query->max_num_pages; ?>"/>
								</a>
								<div class="custom-loader"></div>
							</div>
			<?php endif; ?>

				</div>
	  <?php
	  endif;
	  wp_reset_postdata();
	  ?>

	</div>
</section>