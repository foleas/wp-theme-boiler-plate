<?php

/**
 * Google Maps Block Template
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'google-maps-' . $block['id'];
if ( ! empty( $block['anchor'] ) ) {
	$id = $block['anchor'];
}
$className = 'google-maps-block-wrapper';
if ( ! empty( $block['className'] ) ) {
	$className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
	$className .= ' align' . $block['align'];
}
if ( $is_preview ) {
	$className .= ' is-admin';
}

if ( ! get_field( 'google_map' ) ) {
	return;
}

$google_map = get_field( 'google_map' );
?>

<section id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $className ); ?>">
	<div class="container-custom-fluid">
		<InnerBlocks allowedBlocks="<?php echo esc_attr( wp_json_encode( array( 'core/heading' ) ) ); ?>"/>
	</div>
	<div class="google-maps-wrapper">

		<div class="custom-google-map" data-zoom="16">
			<div class="marker" data-lat="<?php echo esc_attr( $google_map['lat'] ); ?>"
					 data-lng="<?php echo esc_attr( $google_map['lng'] ); ?>"></div>
		</div>
	</div>
</section>