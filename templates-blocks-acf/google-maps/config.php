<?php
//===================================================
// GOOGLE MAPS
//===================================================

acf_register_block_type( array(
	'name'            => 'espartalesnorte-google-maps',
	'title'           => __( 'Espartales Norte Google Maps', 'espartalesnorte' ),
	'description'     => __( 'Bloque para mapa de google', 'espartalesnorte' ),
	'render_template' => 'templates-blocks-acf/google-maps/template.php',
	'category'        => 'espartalesnorte',
	'icon'            => 'location-alt',
	'keywords'        => array( 'maps' ),
	'mode'            => 'preview',
	'supports'        => array(
		'align'  => false,
		'anchor' => true,
		'jsx'    => true,
	),
	'enqueue_assets'  => function () {
		// PLUGIN
//		if ( ! is_admin() ) {
		if ( ! wp_script_is( 'google-api-key', 'enqueued' ) ) {
			if ( ENVIRONMENT === 'PRODUCTION' ) {
				wp_enqueue_script( 'google-api-key', 'https://maps.googleapis.com/maps/api/js?key=', '', '', true );
			} else {
				wp_enqueue_script( 'google-api-key', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAo2xtJrMmSOfH6XbX8BBXP-DoolM3qfT0', '', '', true );
			}
		}
//		}
		// BLOCK
//		if ( ! is_admin() ) {
		if ( ! wp_script_is( 'espartalesnorte-google-maps-block', 'enqueued' ) ) {
			wp_enqueue_script( 'espartalesnorte-google-maps-block', get_stylesheet_directory_uri() . '/templates-blocks-acf/google-maps/scripts.min.js', '', '', true );
		}
//		}
		if ( ! wp_style_is( 'espartalesnorte-google-maps-block', 'enqueued' ) ) {
			wp_enqueue_style( 'espartalesnorte-google-maps-block', get_stylesheet_directory_uri() . '/templates-blocks-acf/google-maps/styles.min.css' );
		}
	},
) );

//===================================================
// FIELDS
//===================================================

if ( function_exists( 'acf_add_local_field_group' ) ):

	acf_add_local_field_group( array(
		'key'                   => 'group_630cc3aa2ed75',
		'title'                 => 'Bloque Google Maps',
		'fields'                => array(
			array(
				'key'               => 'field_630cc3b596269',
				'label'             => 'Google Map',
				'name'              => 'google_map',
				'type'              => 'google_map',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'center_lat'        => '',
				'center_lng'        => '',
				'zoom'              => '',
				'height'            => '',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'block',
					'operator' => '==',
					'value'    => 'acf/espartalesnorte-google-maps',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
		'show_in_rest'          => 0,
	) );

endif;