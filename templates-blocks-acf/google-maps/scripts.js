import { initMap } from "../common/js/googleMapsAPI.min.js";

(function ($) {
  //===================================================
  // GOOGLE MAPS
  //===================================================
  const initializeGoogleMaps = function ($blockSection) {
    // console.log($blockSection);
    const googleMapsWrapper = $(".google-maps-wrapper", $blockSection);
    if (googleMapsWrapper.length > 0) {
      initMap($(".custom-google-map", this));
    }
  };

  // Initialize each block on page load (front end).
  $(window).on("load", function () {
    const blocks = $(".google-maps-block-wrapper");
    if (blocks.length > 0) {
      blocks.each(function () {
        initializeGoogleMaps($(this));
      });
    }
  });

  // Initialize dynamic block preview (editor).
  if (window.acf) {
    window.acf.addAction(
      "render_block_preview/type=espartalesnorte-google-maps",
      initializeGoogleMaps
    );
  }
})(jQuery);
