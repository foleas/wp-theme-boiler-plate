<?php
////===================================================
// PRODUCTS BY CATEGORIES AJAX - ACF BLOCK
////===================================================

add_action('wp_ajax_products_by_categories_ajax', 'products_by_categories_ajax');
add_action('wp_ajax_nopriv_products_by_categories_ajax', 'products_by_categories_ajax');

function products_by_categories_ajax()
{

	$status = 'success';
	$html_return = '';
	$html_link_return = '';

	//check_ajax_referer('contact_form_ajax_nonce', 'nonce');

	foreach ($_POST as $key => $value) {
		$clean_data[$key] = htmlspecialchars($value);
		$clean_data[$key] = nl2br($clean_data[$key]);
		$clean_data[$key] = urldecode($clean_data[$key]);
	}

	global $sitepress;
	$sitepress->switch_lang(getCurrentLang());

	$products_by_categories_query = new WP_Query(
		array(
			'post_type' => 'product',
			'posts_per_page' => 8,
			'tax_query' => array(
				array(
					'taxonomy' => 'product_cat',
					'field' => 'term_id',
					'terms' => $clean_data['termId'],
				),
			),
		)
	);

	if ($products_by_categories_query->have_posts()) {

		while ($products_by_categories_query->have_posts()) {
			$products_by_categories_query->the_post();
			ob_start();
			wc_get_template_part('content', 'product');
			$html_return .= ob_get_clean();
		}
	} else {
		$html_return .= '<p>' . __('Spiacente, nessun post corrisponde ai tuoi criteri.', 'text-domain') . '</p>';
	}

	$term_object = get_term_by('id', $clean_data['termId'], 'product_cat');
	$term_url = get_term_link($term_object, 'product_cat');
	$term_text = __('Vedi tutti gli', 'text-domain') . ' ' . $term_object->name;
	$html_link_return .= '<div class="custom-btn-wrapper">';
	$html_link_return .= '<a class="custom-btn" href="' . $term_url . '">';
	$html_link_return .= $term_text;
	$html_link_return .= '</a>';
	$html_link_return .= '</div>';

	response(array(
		'status' => $status,
		'html_return' => $html_return,
		'html_link_return' => $html_link_return,
	));

	die();
}