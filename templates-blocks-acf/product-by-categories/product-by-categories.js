(function ($) {

    //===================================================
    // PRODUCTS BY CATEGORIES
    //===================================================
    var initializeProductByCategories = function ($block) {

        // LINKS - AJAX
        $block.find(".products-by-categories-cats a").bind("click", function (e) {
            var button = $(this);
            if (button.hasClass('active')) return false;

            $(".products-by-categories-cats a").removeClass("active");
            button.addClass("active");

            var termId = button.attr('data-id');
            var parentWrapper = button.closest(".products-by-categories-wrapper");
            var productsWrapper = parentWrapper.find(".products-by-categories-prods");
            var loaderElem = productsWrapper.find(".custom-loader-wrapper");

            $.ajax({
                url: ajax_url,
                data: {
                    action: 'products_by_categories_ajax',
                    termId: termId,
                },
                type: 'POST',
                beforeSend: function () {
                    loaderElem.addClass("show-loader");
                    productsWrapper.addClass("loading-products");
                },
                complete: function () {
                    loaderElem.removeClass("show-loader");
                    productsWrapper.removeClass("loading-products");
                },
                success: function (response) {
                    // console.log(response);
                    if (response.status == 'success') {
                        $(".post-grid", productsWrapper).html('<div class="row"></div>');

                        if( response.html_link_return )
                        $(".post-grid", productsWrapper).append(response.html_link_return);

                        $(".post-grid .row", productsWrapper).html(response.html_return);
                        if( $(".post-grid .row .product", productsWrapper).length > 4 ) create_slick_slider($(".post-grid .row", productsWrapper));
                    }
                }
            });
        });

        // SLIDER
        var sliderBlock = $block.find('.products.post-grid .row');
        if( $(".product" ,sliderBlock ).length > 4 ) create_slick_slider(sliderBlock);

    }//initializeProductByCategories


    // Initialize each block on page load (front end).
    $(document).ready(function () {
        if ($(".products-by-categories-wrapper").length > 0) {
            $(".products-by-categories-wrapper").each(function () {
                initializeProductByCategories($(this));
            })
            $(".products-by-categories-cats a:nth-child(1)").trigger('click');
        }
    });

    // Initialize dynamic block preview (editor).
    if (window.acf) {
        window.acf.addAction('render_block_preview/type=products-by-categories', initializeProductByCategories);
    }

    function create_slick_slider(sliderBlock_) {
        sliderBlock_.on('init', function (event, slick) {
        });

        sliderBlock_.slick({
            rows: 0,
            dots: false,
            arrows: true,
            prevArrow: '<div class="slick-prev slick-arrow"></div>',
            nextArrow: '<div class="slick-next slick-arrow"></div>',
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1,
                        centerMode: true,
                        centerPadding: '50px',
                    }
                },
            ],
        });

        // $(document).on('click', '.slider-main-arrow', function () {
        //     if ($(this).hasClass('left')) sliderBlock_.slick("slickPrev");
        //     if ($(this).hasClass('right')) sliderBlock_.slick("slickNext");
        // });
    }

})(jQuery);