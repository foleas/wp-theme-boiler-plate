<?php

/**
 * Block Products by Categories Template
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'productsByCategories-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$className = 'products-by-categories-wrapper';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}
if ($is_preview) {
    $className .= ' is-admin';
}
?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="container-fluid">

        <div class="products-by-categories-title">
            <h2><?php echo get_field('titolo'); ?></h2>
            <h3><?php echo get_field('sottotitolo'); ?></h3>
        </div>


        <?php
        $categories = get_field('categorie');
        if ($categories):
            ?>

            <div class="products-by-categories-cats">
                <?php
                $first_term_id = '';
                foreach ($categories as $key => $value):
                    ?>
                    <a class="custom-btn black-btn"
                       data-id="<?php echo $value['categoria']; ?>"><?php echo $value['testo']; ?></a>
                <?php endforeach; ?>
            </div>

            <div class="products-by-categories-prods">

                <div class="custom-loader-wrapper">
                    <div class="custom-loader"></div>
                </div>

                <div class="products post-grid">
                </div><!-- post grid -->
            </div><!-- products-by-categories-prods -->

        <?php endif; ?>

    </div>

    </div>
</section>