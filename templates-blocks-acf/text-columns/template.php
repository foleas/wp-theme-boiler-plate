<?php

/**
 * Text Columns Block Template
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'text-columns-' . $block['id'];
if ( ! empty( $block['anchor'] ) ) {
	$id = $block['anchor'];
}
$className = 'text-columns-block-wrapper';
if ( ! empty( $block['className'] ) ) {
	$className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
	$className .= ' align' . $block['align'];
}

if ( ! empty( $block['textColor'] ) ) {
	//	$className .= ' has-text-color';
	$className .= ' has-' . $block['textColor'] . '-color';
}
if ( ! empty( $block['backgroundColor'] ) ) {
//	$className .= ' has-background';
	$className .= ' has-' . $block['backgroundColor'] . '-background-color';
}
if ( $is_preview ) {
	$className .= ' is-admin';
}

if ( ! get_field( 'columnas_columnas' ) ) {
	return;
}

$columnsGroup = get_field( 'columnas' );
$columns      = get_field( 'columnas_columnas' );

$info     = get_field( 'info_header' );
$showInfo = false;
foreach ( $info as $key => $value ) {
	if ( $value !== '' && ! empty( $value ) ) {
		$showInfo = true;
	}
}
?>

<section id="<?php echo esc_attr( $id ); ?>" class="with-paddings <?php echo esc_attr( $className ); ?>">

	<?php
	if ( get_field( 'imagen_fondo' ) ) {
		echo '<div class="bg-img">';
		echo wp_get_attachment_image( get_field( 'imagen_fondo' ), 'full' );
		echo '</div>';
	}
	?>

	<div class="container-custom">

	  <?php
	  if ( $showInfo ):
		  $partialsArray = [
			  'info' => $info,
		  ];
		  get_template_part( 'templates-blocks-acf/partials/info', 'header', $partialsArray );
	  endif;
	  ?>

		<div class="text-columns-wrapper">
		<?php
		$bgColor    = $columnsGroup['color_fondo'];
		$titleColor = $columnsGroup['color_titulo'];
		$textColor  = $columnsGroup['color_texto'];
		foreach ( $columns as $column ):
			echo '<div class="text-column-col">';
			echo '<div class="text-column" style="background-color: ' . $bgColor . '">';

			echo ( $column['imagen'] ) ? wp_get_attachment_image( $column['imagen'], 'medium' ) : '';
			echo ( $column['titulo'] ) ? '<h5 style="color:' . $titleColor . '">' . $column['titulo'] . '</h5>' : '';
			echo ( $column['texto'] ) ? '<div class="text" style="color:' . $textColor . '">' . $column['texto'] . '</div>' : '';

			echo '</div>';
			echo '</div>';
		endforeach;
		?>
		</div>

	  <?php
	  $button = get_field( 'boton' );
	  if ( ! empty( $button ) ) {
		  echo '<div class="custom-btn-wrapper">';

		  $buttonArray = [
			  'button'      => $button,
			  'buttonColor' => get_field( 'boton_color' ),
		  ];
		  get_template_part( 'templates-blocks-acf/partials/custom-btn', '', $buttonArray );
		  echo '</div>';
	  }
	  ?>

	</div>
</section>