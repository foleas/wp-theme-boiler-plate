export const loadmoreHandler = (item, wrapper, ajaxUrl) => {
  const itemWrapper = item.closest(".custom-btn-wrapper");
  const max_page = jQuery("input[name=max_page]", itemWrapper).val();

  const ajaxData = {
    action: jQuery("input[name=action]", itemWrapper).val(),
    nonce: jQuery("input[name=nonce]", itemWrapper).val(),
    post_type: jQuery("input[name=post_type]", itemWrapper).val(),
    query_vars: jQuery("input[name=query_vars]", itemWrapper).val(),
    paged: jQuery("input[name=paged]", itemWrapper).val(),
    max_page: max_page,
  };

  jQuery.ajax({
    url: ajaxUrl,
    data: ajaxData,
    type: "POST",
    beforeSend: function () {
      item.addClass("disabled");
      itemWrapper.addClass("loading");
    },
    complete: function () {
      item.removeClass("disabled");
      itemWrapper.removeClass("loading");
    },
    success: function (response) {
      // console.log("response", response);
      // console.log("html_return", response.html_return);

      if (parseInt(response.new_paged) === parseInt(max_page)) {
        itemWrapper.addClass("hide");
      } else {
        itemWrapper.removeClass("hide");
      }

      if (response.html_return) {
        jQuery("input[name=paged]", itemWrapper).val(response.new_paged);
        jQuery(".posts-grid", wrapper).append(response.html_return);
      }
    },
  });
};
