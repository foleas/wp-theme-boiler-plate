// VIDEO
let player = null;
let currentVideoSection = null;

export const modalVideoInit = (videoPlayer, blockSection) => {
  const videoPlayerId = videoPlayer.attr("data-id");
  const videoHost = videoPlayer.attr("data-video-host");
  const videoId = videoPlayer.attr("data-video-id");
  const modal = jQuery(".modal-wrapper.modal-wrapper-video");

  const videoPreview = blockSection.find(".video-preview");

  if (modal.length > 0) {
    videoPreview.click(() => {
      // console.log("CLICK");
      currentVideoSection = blockSection;
      if (!blockSection.hasClass("video-loaded")) {
        // console.log("video loaded");
        jQuery(".half-image-text-block-wrapper").removeClass("video-loaded");
        jQuery(".modal-content-video", modal).html(
          `<div id="${videoPlayerId}"/>`
        );
        if (videoHost === "youtube") {
          loadYouTubeIframeAPI(videoPlayerId, videoId);
        } else if (videoHost === "vimeo") {
          loadVimeoIframeAPI(videoPlayerId, videoId);
        }
      }

      jQuery("body").addClass("no-scroll");
      modal.fadeIn(500, "easeInOutCirc", () => {
        modal.addClass("show");
        if (blockSection.hasClass("video-loaded")) {
          playVideo(videoHost);
        }
      });
    });

    jQuery(".modal-close", modal).on("click", () => {
      pauseVideo(videoHost);
    });
  }
};

// YOUTUBE
export const loadYouTubeIframeAPIScript = () => {
  const tag = document.createElement("script");
  tag.src = "https://www.youtube.com/iframe_api";
  const firstScriptTag = document.getElementsByTagName("script")[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
};

export const loadYouTubeIframeAPI = (playerID, videoID) => {
  // console.log("youtube api");
  player = new YT.Player(playerID, {
    width: "960",
    height: "540",
    videoId: videoID,
    events: {
      onReady: onPlayerReady,
      // 'onStateChange': onPlayerStateChange
    },
  });
};

export const onPlayerReady = (event) => {
  currentVideoSection.addClass("video-loaded");
  event.target.playVideo();
};

// VIMEO
export const loadVimeoIframeAPIScript = () => {
  const tag = document.createElement("script");
  tag.src = "https://player.vimeo.com/api/player.js";
  const firstScriptTag = document.getElementsByTagName("script")[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
};

export const loadVimeoIframeAPI = (playerID, videoID) => {
  player = new Vimeo.Player(playerID, { id: videoID, width: 960 });
  // player.setVolume(0);
  player.play();
};

export const playVideo = (host) => {
  if (host === "youtube") {
    player.playVideo();
  } else {
    player.play();
  }
};

export const stopVideo = (host) => {
  if (host === "youtube") {
    player.stopVideo();
  } else {
    player.stop();
  }
};

export const pauseVideo = (host) => {
  if (host === "youtube") {
    player.pauseVideo();
  } else {
    player.pause();
  }
};
