// GALLERY
let currentGallerySection = null;
let modalSwiper = null;
export const modalGalleryInit = (
  galleryItem, // DONDE HACER EL LOOP PARA CONSEGUIR CADA IMAGEN DEL MODAL
  blockSection, // CONTENEDOR PARA SABER SI YA ESTA INICIADA LA GALERIA DEL MODAL
  clickItem = null // CLICK ITEMA CUANDO DIFIERE DEL ITEM DE GALERIA
) => {
  const modal = jQuery(".modal-wrapper.modal-wrapper-gallery");
  if (modal.length > 0) {
    // ABRIR MODAL Y CREAR GALERIA
    let clickSelector = galleryItem;
    if (clickItem !== null) {
      clickSelector = clickItem;
    }
    clickSelector.on("click", function () {
      // console.log("click", jQuery(this));

      // JUNTAR HTML PARA EL SLIDER DEL MODAL
      let imagesFullHtml = getHtmlForModalSlider(galleryItem);
      // console.log(galleryItem, "imagesFullHtml", imagesFullHtml);

      currentGallerySection = blockSection;
      if (!blockSection.hasClass("gallery-loaded")) {
        jQuery(".carrousel-block-wrapper").removeClass("gallery-loaded");
        jQuery(".swiper-wrapper", modal).html(imagesFullHtml);
        modalSwiper = initModalSwiper(modal);
      }

      if (clickItem === null) {
        const index = jQuery(this).index();
        // console.log("index" , index);
        modalSwiper.slideTo(index);
      }

      jQuery("body").addClass("no-scroll");
      modal.fadeIn(500, "easeInOutCirc", () => {
        modal.addClass("show");
      });
    });
  }
};

export const getHtmlForModalSlider = (itemSelector) => {
  let returnHtml = "";
  itemSelector.each(function () {
    // console.log("each", jQuery(this));
    const imageFull = jQuery(".modal-full-img", this).html();
    const imageCaption = jQuery(".modal-caption", this).html();
    returnHtml += '<div class="swiper-slide">';
    returnHtml += '<div class="slide-img">';
    returnHtml += "<figure>";
    returnHtml += '<div class="slide-overlay"></div>';
    returnHtml += imageFull;
    returnHtml +=
      '<figcaption class="slide-caption"><p>' +
      imageCaption +
      "</p></figcaption>";
    returnHtml += "</figure>";
    returnHtml += "</div>";
    returnHtml += "</div>";
  });

  return returnHtml;
};

export const initModalSwiper = (modal) => {
  const swiperElem = jQuery(".swiper", modal)[0];
  const pageNext = jQuery(".swiper-button-next", modal)[0];
  const pageLeft = jQuery(".swiper-button-prev", modal)[0];

  return new Swiper(swiperElem, {
    slidesPerView: 1,
    spaceBetween: 0,
    grabCursor: true,
    navigation: {
      nextEl: pageNext,
      prevEl: pageLeft,
    },
    keyboard: {
      enabled: true,
      onlyInViewport: false,
      pageUpDown: false,
    },
  });
};
