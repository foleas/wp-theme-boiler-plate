<?php
//===================================================
// TWO COLUMNS
//===================================================

acf_register_block_type(array(
	'name' => 'text-domain-video-iframe',
	'title' => __('text-domain Video Iframe', 'text-domain'),
	'description' => __('Bloque para custom video iframe', 'text-domain'),
	'render_template' => 'templates-blocks-acf/video-iframe/template.php',
	'category' => 'text-domain',
	'icon' => 'format-video',
	'keywords' => array('video', 'iframe'),
	'mode' => 'preview',
	'supports' => array(
		'align' => false,
		'anchor' => true,
	),
	'enqueue_assets' => function () {
		// BLOCK
		if (!is_admin()) {
			if (!wp_script_is('text-domain-video-iframe-block', 'enqueued') && !is_admin()) {
				wp_enqueue_script('text-domain-video-iframe-block', get_stylesheet_directory_uri() . '/templates-blocks-acf/video-iframe/scripts.min.js', '', '', true);
			}
		}
		if (!wp_style_is('text-domain-video-iframe-block', 'enqueued')) {
			wp_enqueue_style('text-domain-video-iframe-block', get_stylesheet_directory_uri() . '/templates-blocks-acf/video-iframe/styles.min.css');
		}
	},
));

//===================================================
// FIELDS
//===================================================

if (function_exists('acf_add_local_field_group')):

	acf_add_local_field_group(array(
		'key' => 'group_63053c4ee73e1',
		'title' => 'Bloque Video Iframe',
		'fields' => array(
			array(
				'key' => 'field_63053caf216ec',
				'label' => 'Video Url',
				'name' => 'video_url',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_63054759df88b',
				'label' => 'Imagen Preview',
				'name' => 'imagen_preview',
				'type' => 'image',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'id',
				'preview_size' => 'medium',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'block',
					'operator' => '==',
					'value' => 'acf/text-domain-video-iframe',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
		'show_in_rest' => 0,
	));

endif;