<?php

/**
 * Video Iframe Block Template
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'video-iframe-' . $block['id'];
if ( ! empty( $block['anchor'] ) ) {
	$id = $block['anchor'];
}
$className = 'video-iframe-block-wrapper';
if ( ! empty( $block['className'] ) ) {
	$className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
	$className .= ' align' . $block['align'];
}
if ( $is_preview ) {
	$className .= ' is-admin';
}

if ( ! get_field( 'video_url' ) ) {
	return;
}

$video        = get_field( 'video_url' );
$videoData    = getVideoDataFromUrl( $video );
$videoHost    = $videoData['host'];
$videoID      = $videoData['videoID'];
$imagePreview = get_field( 'imagen_preview' );
?>

<section id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $className ); ?>">
	<div class="video-iframe-wrapper">

	  <?php if ( $imagePreview ) {
		  echo '<div class="video-preview">';
		  echo wp_get_attachment_image( $imagePreview, 'large' );
		  echo '<span class="icon-icon-play"><span class="path1"></span><span class="path2"></span></span>';
		  echo '</div>';
	  }
	  ?>
		<div class="video-player" data-id="video-player-<?php echo $block['id']; ?>"
				 data-video-id="<?php echo $videoID; ?>"></div>
	</div>

	</div>
</section>