import {
  loadYouTubeIframeAPIScript,
  loadVimeoIframeAPIScript,
  modalVideoInit,
} from "../common/js/videoIframe.min.js";

(function ($) {
  //===================================================
  // PAGE MAIN
  //===================================================
  const initializeVideoIframe = function ($blockSection) {
    const videoPlayer = $blockSection.find(".video-player");
    if (videoPlayer.length > 0) {
      modalVideoInit(videoPlayer, $blockSection);
    }
  };

  // Initialize each block on page load (front end).
  $(window).on("load", function () {
    const blocks = $(".video-iframe-block-wrapper");
    if (blocks.length > 0) {
      blocks.each(function () {
        initializeVideoIframe($(this));
      });
    }
  });

  loadYouTubeIframeAPIScript();
  loadVimeoIframeAPIScript();

  // Initialize dynamic block preview (editor).
  if (window.acf) {
    window.acf.addAction(
      "render_block_preview/type=video-iframe",
      initializeVideoIframe
    );
  }
})(jQuery);
