<?php
$info = '';
if ( array_key_exists( 'info', $args ) ) {
	if ( $args['info'] !== '' ) {
		$info = $args['info'];
	}
}
$buttonColor = 'primary';
if ( array_key_exists( 'buttonColor', $args ) ) {
	if ( $args['buttonColor'] !== '' ) {
		$buttonColor = $args['buttonColor'];
	}
}
?>
<div class="info-col-wrapper">
	<div class="info">

	  <?php
	  // TITLE
	  if ( array_key_exists( 'titulo', $info ) && ! empty( $info['titulo'] ) ) {
		  echo '<h2 class="title">' . $info['titulo'] . '</h2>';
	  }
	  ?>

	  <?php
	  // TEXT
	  if ( array_key_exists( 'texto', $info ) && ! empty( $info['texto'] ) ) {
		  echo '<h3 class="text">' . $info['texto'] . '</h3>';
	  }
	  ?>

	  <?php
	  if ( array_key_exists( 'boton', $info ) ) {
		  if ( $info['boton'] ) {
			  $button = $info['boton'];
			  ?>
						<a href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>"
							 class="custom-btn <?php echo $buttonColor; ?>">
							<p><?php echo $button['title']; ?></p>
						</a>
		  <?php }
	  } ?>
	</div>
</div>