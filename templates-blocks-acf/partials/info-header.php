<?php
$info = '';
if ( array_key_exists( 'info', $args ) ) {
	if ( $args['info'] !== '' ) {
		$info = $args['info'];
	}
}
if ( ! $info || empty( $info ) ) {
	return;
}
?>
<div class="info-header-wrapper">
	<div class="info">

	  <?php
	  // TITLE
	  if ( array_key_exists( 'titulo', $info ) && ! empty( $info['titulo'] ) ) {
		  echo '<h2 class="title">' . $info['titulo'] . '</h2>';
	  }
	  ?>

	  <?php
	  // SUB TITLE
	  if ( array_key_exists( 'sub_titulo', $info ) && ! empty( $info['sub_titulo'] ) ) {
		  echo '<h3 class="sub-title">' . $info['sub_titulo'] . '</h3>';
	  }
	  ?>

	  <?php
	  // TEXT
	  if ( array_key_exists( 'texto', $info ) && ! empty( $info['texto'] ) ) {
		  echo '<h4 class="text">' . $info['texto'] . '</h4>';
	  }
	  ?>
	</div>
</div>