<?php
$button = '';
if ( array_key_exists( 'button', $args ) ) {
	if ( $args['button'] !== '' ) {
		$button = $args['button'];
	}
}
$buttonColor = 'primary';
if ( array_key_exists( 'buttonColor', $args ) ) {
	if ( $args['buttonColor'] !== '' ) {
		$buttonColor = $args['buttonColor'];
	}
}

if ( empty( $button ) ) {
	return;
}

echo '<a href="' . $button['url'] . '" target="' . $button['target'] . '" class="custom-btn ' . $buttonColor . '">';
echo '<p>' . $button['title'] . '</p>';
echo '</a>';