<?php

/**
 * Gallery Block Template
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'gallery-' . $block['id'];
if (!empty($block['anchor'])) {
	$id = $block['anchor'];
}
$className = 'gallery-block-wrapper';
if (!empty($block['className'])) {
	$className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
	$className .= ' align' . $block['align'];
}
if ($is_preview) {
	$className .= ' is-admin';
}

if (!get_field('galeria')) {
	return;
}
?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<?php
	$galleryType = get_field('type');
	switch ($galleryType):
		case "slide":
			get_template_part('templates-blocks-acf/gallery/type', 'slider');
			break;
		case "masonry":
			get_template_part('templates-blocks-acf/gallery/type', 'masonry');
			break;
		case "single":
			get_template_part('templates-blocks-acf/gallery/type', 'single');
			break;
	endswitch;
	?>

</section>