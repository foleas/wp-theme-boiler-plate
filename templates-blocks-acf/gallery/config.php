<?php
//===================================================
// GALLERY BLOCK
//===================================================

acf_register_block_type( array(
	'name'            => 'text-domain-gallery-block',
	'title'           => __( 'Text Domain Norte Galería', 'text-domain' ),
	'description'     => __( 'Bloque para Galería de imagenes.', 'text-domain' ),
	'category'        => 'text-domain',
	'icon'            => 'format-gallery',
	'keywords'        => array( 'galeria', 'imagenes' ),
	'mode'            => 'preview',
	'supports'        => array(
		'mode'   => false,
		'align'  => false,
		'anchor' => true,
	),
	'render_template' => 'templates-blocks-acf/gallery/template.php',
	'enqueue_assets'  => function () {
		// PLUGIN
//		if ( ! is_admin() ) {
		if ( ! wp_script_is( 'swiper', 'enqueued' ) ) {
			wp_enqueue_script( 'swiper', 'https://unpkg.com/swiper@8/swiper-bundle.min.js', '', '', true );
		}
//		}
		if ( ! wp_style_is( 'swiper', 'enqueued' ) ) {
			wp_enqueue_style( 'swiper', 'https://unpkg.com/swiper@8/swiper-bundle.min.css' );
		}

		// BLOCK
		if ( ! is_admin() ) {
			if ( ! wp_script_is( 'text-domain-gallery-block', 'enqueued' ) ) {
				wp_enqueue_script( 'text-domain-gallery-block', get_stylesheet_directory_uri() . '/templates-blocks-acf/gallery/scripts.min.js', '', '', true );
			}
		} else {
			if ( ! wp_script_is( 'text-domain-gallery-block-admin', 'enqueued' ) ) {
				wp_enqueue_script( 'text-domain-gallery-block-admin', get_stylesheet_directory_uri() . '/templates-blocks-acf/gallery/scripts-admin.min.js', '', '', true );
			}
		}
		if ( ! wp_style_is( 'text-domain-gallery-block', 'enqueued' ) ) {
			wp_enqueue_style( 'text-domain-gallery-block', get_stylesheet_directory_uri() . '/templates-blocks-acf/gallery/styles.min.css' );
			wp_enqueue_style( 'text-domain-gallery-block', get_stylesheet_directory_uri() . '/templates-blocks-acf/gallery/styles.min.css' );
		}
	},
) );

//===================================================
// FIELDS
//===================================================

if ( function_exists( 'acf_add_local_field_group' ) ):

	acf_add_local_field_group( array(
		'key'                   => 'group_6290fa1c38fa9',
		'title'                 => 'Bloque Galería',
		'fields'                => array(
			array(
				'key'               => 'field_6290fa2b653de',
				'label'             => 'Type',
				'name'              => 'type',
				'type'              => 'select',
				'instructions'      => 'Tipo de Galería',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'choices'           => array(
					'slide'   => 'Slide',
					'masonry' => 'Masonry',
					'single'  => 'Single',
				),
				'default_value'     => false,
				'allow_null'        => 0,
				'multiple'          => 0,
				'ui'                => 1,
				'ajax'              => 0,
				'return_format'     => 'value',
				'placeholder'       => '',
			),
			array(
				'key'               => 'field_6294bface343d',
				'label'             => 'Galería',
				'name'              => 'galeria',
				'type'              => 'repeater',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'collapsed'         => 'field_6294bfc5e343e',
				'min'               => 1,
				'max'               => 0,
				'layout'            => 'block',
				'button_label'      => 'Agregar Imagen',
				'sub_fields'        => array(
					array(
						'key'               => 'field_6294bfc5e343e',
						'label'             => 'Imagen',
						'name'              => 'imagen',
						'type'              => 'image',
						'instructions'      => '',
						'required'          => 1,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'return_format'     => 'id',
						'preview_size'      => 'medium',
						'library'           => 'all',
						'min_width'         => '',
						'min_height'        => '',
						'min_size'          => '',
						'max_width'         => '',
						'max_height'        => '',
						'max_size'          => '',
						'mime_types'        => '',
					),
					array(
						'key'               => 'field_6294bfd5e343f',
						'label'             => 'Pie Imagen',
						'name'              => 'pie_imagen',
						'type'              => 'text',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'placeholder'       => '',
						'prepend'           => '',
						'append'            => '',
						'maxlength'         => '',
					),
					array(
						'key'               => 'field_6294c054e3440',
						'label'             => 'Copyright',
						'name'              => 'copyright',
						'type'              => 'text',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'placeholder'       => '',
						'prepend'           => '',
						'append'            => '',
						'maxlength'         => '',
					),
					array(
						'key'               => 'field_6294c05ce3441',
						'label'             => 'Copyright Url',
						'name'              => 'copyright_url',
						'type'              => 'text',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'placeholder'       => '',
						'prepend'           => '',
						'append'            => '',
						'maxlength'         => '',
					),
				),
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'block',
					'operator' => '==',
					'value'    => 'acf/text-domain-gallery-block',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => true,
		'description'           => '',
		'show_in_rest'          => 0,
	) );

endif;