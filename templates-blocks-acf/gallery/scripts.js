import { modalGalleryInit } from "../common/js/gallery.min.js";

(function ($) {
  //===================================================
  // GALLERY BLOCK
  //===================================================
  const initializeGallery = function ($blockSection) {
    // console.log($blockSection);
    const galleryWrapper = $blockSection.find(".gallery-wrapper");
    if (galleryWrapper.length > 0) {
      // SI ES GALLERY DEL TIPO SLIDER
      if (galleryWrapper.hasClass("swiper")) {
        const nextEl = $(".swiper-button-next", galleryWrapper)[0];
        const prevEl = $(".swiper-button-prev", galleryWrapper)[0];
        const galleryScrollbar = galleryWrapper.find(".swiper-scrollbar");
        const swiper = new Swiper(galleryWrapper[0], {
          slidesPerView: 1,
          spaceBetween: 50,
          grabCursor: true,
          scrollbar: {
            el: galleryScrollbar[0],
            // hide: true,
            draggable: true,
          },
          navigation: {
            nextEl,
            prevEl,
          },
          on: {
            init: function (swiper) {
              // console.log("swiper initialized", swiper);
              $(".custom-loader", swiper.$el[0]).remove();
            },
          },
          breakpoints: {
            768: {
              slidesPerView: 1.5,
            },
          },
        });
      }

      modalGalleryInit(
        $(".gallery-slide", galleryWrapper),
        $blockSection,
        $(".gallery-slide .fullscreen", galleryWrapper)
      );
    }
  };

  // Initialize each block on page load (front end).
  $(window).on("load", function () {
    if ($(".gallery-block-wrapper").length > 0) {
      $(".gallery-block-wrapper").each(function () {
        initializeGallery($(this));
      });
    }
  });

  // Initialize dynamic block preview (editor).
  if (window.acf) {
    window.acf.addAction(
      "render_block_preview/type=gallery-block",
      initializeGallery
    );
  }
})(jQuery);
