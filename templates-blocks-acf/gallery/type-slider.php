<div class="gallery-wrapper swiper">
	<div class="custom-loader"></div>
	<?php
	$gallery = get_field( 'galeria' );
	if ( $gallery ):
		?>
			<div class="swiper-wrapper">
		  <?php foreach ( $gallery as $item ):
			  echo '<div class="gallery-slide swiper-slide">';
			  echo '<div class="slide-img">';
			  // HTML MODAL
			  echo '<div class="modal-full-img">' . wp_get_attachment_image( $item['imagen'], 'full' ) . '</div>';
			  // FULLSCREEN
			  echo '<div class="fullscreen">';
			  echo '<span class="icon normal">' . file_get_contents( get_template_directory() . '/assets/images/icons/icon-fullscreen.svg' ) . '</span>';
			  echo '<span class="icon hover">' . file_get_contents( get_template_directory() . '/assets/images/icons/icon-fullscreen-hover.svg' ) . '</span>';
			  echo '</div>';

			  echo '<figure>';
			  // GRADIENT
			  echo '<div class="gradient"></div>';
			  // IMAGE
			  echo wp_get_attachment_image( $item['imagen'], 'large' );
			  // CAPTION
			  echo '<figcaption class="modal-caption slide-caption">';
			  echo '<p>' . $item['pie_imagen'] . '</p>';
			  echo '<p>';
			  if ( $item['copyright_url'] ) {
				  echo '<a href="' . $item['copyright_url'] . '" target="_blank">';
			  }
			  echo '<small>' . $item['copyright'] . '</small>';
			  if ( $item['copyright_url'] ) {
				  echo '</a>';
			  }
			  echo '</p>';
			  echo '</figcaption>';
			  echo '</figure>';
			  echo '</div>';
			  echo '</div>';
		  endforeach;
		  ?>
			</div>

			<div class="swiper-controls">
				<div class="swiper-scrollbar"></div>
				<div class="swiper-button-wrapper">
					<div class="swiper-button-prev disable-text-selector">
									<span
										class="icon"><?php echo file_get_contents( get_template_directory() . '/assets/images/icons/icon-arrow-left.svg' ); ?></span>
					</div>
					<div class="swiper-button-next disable-text-selector">
									<span
										class="icon"><?php echo file_get_contents( get_template_directory() . '/assets/images/icons/icon-arrow-right.svg' ); ?></span>
					</div>
				</div>
			</div>
	<?php
	endif;
	?>
</div>
