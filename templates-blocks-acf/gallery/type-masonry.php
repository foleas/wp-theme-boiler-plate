<div class="gallery-wrapper masonry">
	<?php
	$gallery     = get_field( 'galeria' );
	$galleryQty  = sizeof( $gallery );
	$galleryCont = 0;
	if ( $gallery ):
	?>

	<div class="masonry-wrapper">
		<div class="masonry-grid">
		<?php
		foreach ( $gallery as $item ):
			echo '<div class="gallery-slide masonry-slide">';

			if ( $galleryCont === 3 && $galleryQty > 4 ) {
				echo '<div class="overlay"><h1>+' . ( $galleryQty - 4 ) . '</h1></div>';
			}

			echo '<div class="slide-img fullscreen">';
			// HTML MODAL
			echo '<div class="modal-full-img">' . wp_get_attachment_image( $item['imagen'], 'full' ) . '</div>';
			echo '<figure>';
			// GRADIENT
			echo '<div class="gradient"></div>';
			// IMAGE
			echo wp_get_attachment_image( $item['imagen'], 'large' );
			// CAPTION
			echo '<figcaption class="modal-caption slide-caption">';
			echo '<p>' . $item['pie_imagen'] . '</p>';
			echo '<p>';
			if ( $item['copyright_url'] ) {
				echo '<a href="' . $item['copyright_url'] . '" target="_blank">';
			}
			echo '<small>' . $item['copyright'] . '</small>';
			if ( $item['copyright_url'] ) {
				echo '</a>';
			}
			echo '</p>';
			echo '</figcaption>';
			echo '</figure>';
			echo '</div>';
			echo '</div>';
			$galleryCont ++;
		endforeach;
		?>
		</div>

	  <?php
	  endif;
	  ?>
	</div>
