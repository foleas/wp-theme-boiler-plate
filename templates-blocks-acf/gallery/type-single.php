<div class="gallery-wrapper single">
	<?php $gallery = get_field( 'galeria' ); ?>
	<div class="single-wrapper">
	  <?php
	  echo '<div class="gallery-slide single-slide">';
	  echo '<div class="slide-img">';
	  // HTML MODAL
	  echo '<div class="modal-full-img">' . wp_get_attachment_image( $gallery[0]['imagen'], 'full' ) . '</div>';
	  // FULLSCREEN
	  echo '<div class="fullscreen">';
	  echo '<span class="icon normal">' . file_get_contents( get_template_directory() . '/assets/images/icons/icon-fullscreen.svg' ) . '</span>';
	  echo '<span class="icon hover">' . file_get_contents( get_template_directory() . '/assets/images/icons/icon-fullscreen-hover.svg' ) . '</span>';
	  echo '</div>';

	  echo '<figure>';
	  // GRADIENT
	  echo '<div class="gradient"></div>';
	  // IMAGE
	  echo wp_get_attachment_image( $gallery[0]['imagen'], 'large' );
	  // CAPTION
	  echo '<figcaption class="modal-caption slide-caption">';
	  echo '<p>' . $gallery[0]['pie_imagen'] . '</p>';
	  echo '<p>';
	  if ( $gallery[0]['copyright_url'] ) {
		  echo '<a href="' . $gallery[0]['copyright_url'] . '" target="_blank">';
	  }
	  echo '<small>' . $gallery[0]['copyright'] . '</small>';
	  if ( $gallery[0]['copyright_url'] ) {
		  echo '</a>';
	  }
	  echo '</p>';
	  echo '</figcaption>';
	  echo '</figure>';
	  echo '</div>';
	  echo '</div>';
	  ?>
	</div>
</div>
