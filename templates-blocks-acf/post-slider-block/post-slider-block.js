(function ($) {
  //===================================================
  // POST SLIDER BLOCK
  //===================================================
  const initializePostSlider = function ($blockSection) {
    // console.log($blockSection);
    const postSlider = $blockSection.find(".post-slider.swiper");
    if (postSlider.length > 0) {
      const postSliderScrollbar = postSlider.find(".swiper-scrollbar");
      const swiper = new Swiper(postSlider[0], {
        slidesPerView: 1,
        spaceBetween: 0,
        grabCursor: true,
        scrollbar: {
          el: postSliderScrollbar[0],
          // hide: true,
          draggable: true,
        },
        breakpoints: {
          768: {
            slidesPerView: 2,
            spaceBetween: 30,
          },
          1024: {
            slidesPerView: 3,
            spaceBetween: 30,
          },
          1439: {
            slidesPerView: 4,
            spaceBetween: 30,
          },
          2000: {
            slidesPerView: 5,
            spaceBetween: 50,
          },
        },
      });
    }
  };

  // Initialize each block on page load (front end).
  // $(document).ready(function () {
  $(window).on("load", function () {
    if ($(".post-slider-block-wrapper").length > 0) {
      $(".post-slider-block-wrapper").each(function () {
        initializePostSlider($(this));
      });
    }
  });

  // Initialize dynamic block preview (editor).
  if (window.acf) {
    window.acf.addAction(
      "render_block_preview/type=post-slider-block",
      initializePostSlider
    );
  }
})(jQuery);
