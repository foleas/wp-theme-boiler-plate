<?php

/**
 * Block Post Slider Template
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'post-slider-' . $block['id'];
if ( ! empty( $block['anchor'] ) ) {
	$id = $block['anchor'];
}
$className = 'post-slider-block-wrapper';
if ( ! empty( $block['className'] ) ) {
	$className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
	$className .= ' align' . $block['align'];
}
if ( $is_preview ) {
	$className .= ' is-admin';
}

$blockData = $block['data'];

$layout = $blockData['layout'];
$tags   = ( $blockData['tags'] ) ?: get_field( 'tags' );

$posts_query = ( $blockData['custom_query'] ) ?: new WP_Query(
	array(
		'post_type'      => 'post',
		'posts_per_page' => ( $blockData['total_de_posts'] ) ?: get_field( 'total_de_posts' ),
		'cat'            => ( $blockData['categoria'] ) ?: get_field( 'categoria' ),
		'tag__and'       => ( $tags ) ? implode( ", ", $tags ) : array(),
		'post__not_in'   => array( ( $blockData['post__not_in'] ) ?: '' ),
	)
);

if ( $posts_query->have_posts() ) :
	?>

	<section id="<?php echo esc_attr( $id ); ?>"
					 class="<?php echo esc_attr( $className ); ?> layout-<?php echo $layout; ?>">
		<div class="post-slider-wrapper">

			<div class="post-slider-header">
				<?php if ( $blockData['titulo'] ) { ?>
					<div class="tag-wrapper">
						<h2><?php echo $blockData['titulo']; ?></h2>
					</div>
				<?php } ?>

				<?php if ( $blockData['link'] ) {
					$link = $blockData['link']; ?>
					<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"
						 class="custom-btn ghost with-icon <?php echo ( get_field( 'layout' ) === 'light' ) ? 'dark' : 'light'; ?>">
						<p><?php echo $link['title']; ?></p>
						<span class="icon-arrow-right"></span>
					</a>
				<?php } ?>
			</div>


			<div class="post-slider swiper">
				<div class="swiper-wrapper">
					<?php
					while ( $posts_query->have_posts() ) :
						$posts_query->the_post();
						echo '<div class="swiper-slide">';
						$postItemArgs = array();
						get_template_part( 'includes/post/post', 'item', $postItemArgs );
						echo '</div>';
					endwhile;
					?>
				</div>
				<div class="swiper-scrollbar"></div>
			</div>

		</div>
	</section>
<?php
endif;
wp_reset_postdata();
?>