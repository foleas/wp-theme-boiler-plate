<?php

	/**
	 * Block Iframe Template
	 *
	 * @param array $block The block settings and attributes.
	 * @param string $content The block inner HTML (empty).
	 * @param bool $is_preview True during AJAX preview.
	 * @param   (int|string) $post_id The post ID this block is saved to.
	 */

	$id = 'iframe-' . $block['id'];
	if ( ! empty( $block['anchor'] ) ) {
		$id = $block['anchor'];
	}
	$className = 'iframe-block-wrapper';
	if ( ! empty( $block['className'] ) ) {
		$className .= ' ' . $block['className'];
	}
	if ( ! empty( $block['align'] ) ) {
		$className .= ' align' . $block['align'];
	}
	if ( $is_preview ) {
		$className .= ' is-admin';
	}

	if ( ! get_field( 'iframe' ) ) {
		return;
	}

	$layout = get_field( 'layout' );
?>

<section id="<?php echo esc_attr( $id ); ?>"
				 class="<?php echo esc_attr( $className ); ?> layout-<?php echo $layout; ?>">
	<div class="iframe-wrapper">
		<?php echo get_field( 'iframe' ); ?>
	</div>
</section>