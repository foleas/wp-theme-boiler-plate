<?php
$post_type = 'post';
if (array_key_exists('post_type', $args)) {
    if ($args['post_type'] !== '') {
        $post_type = $args['post_type'];
    }
}
?>

<div class="custom-btn-wrapper with-loader">
	<a class="custom-btn loadmore-btn">
		<?php _e('Cargar más' , 'text-domain'); ?>

		<input type="hidden" name="action" value="loadmore"/>
		<input type="hidden" name="nonce" value="<?php echo wp_create_nonce( 'loadmore_nonce' ); ?>"/>
		<input type="hidden" name="post_type" value="<?php echo $post_type; ?>"/>
	</a>
    <div class="custom-loader"></div>
</div>