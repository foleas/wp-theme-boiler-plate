<div class="modal-wrapper modal-wrapper-menu layout-v2">
	<div class="modal-content-wrapper">
		<div class="overlay"></div>

		<div class="modal-header">
			<div class="container-custom-fluid">
				<div class="row">
					<div class="col-auto">
						<a class="logo light" href="<?php bloginfo('url'); ?>">
							<h4><span>Start</span><span>Today</span></h4>
						</a>
					</div>
					<div class="col-auto">
						<div class="modal-close"><span class="icon-close"></span></div>
					</div>
				</div>
			</div>
		</div>

		<?php $menusModal = get_field('menus_modal', 'options'); ?>

		<div class="modal-content">
			<div class="modal-content-menu">
				<div class="container-custom-fluid">

					<div class="menu-cols-wrapper">

						<?php
						$menuUno = $menusModal['menu_uno'];
						if ($menuUno):
							?>
							<div class="menu-col">

								<div class="tag-wrapper">
									<h2><?php echo $menuUno['titulo']; ?></h2>
								</div>

								<?php
								$menuDosItems = wp_get_nav_menu_items($menuUno['menu_select']);
								if ($menuDosItems) {
									echo '<nav class="modal-menu-nav">';
									echo '<ul>';
									foreach ($menuDosItems as $menuItem) {
										$classes = implode(' ', $menuItem->classes);
										$menuId = "modal-menu-item-" . $menuItem->ID;
										echo '<li class="' . $classes . ' ' . $menuId . '"><a href="' . $menuItem->url . '">' . $menuItem->title . '<span class="icon-plus"></span></a></li>';
									}
									echo '</ul>';
									echo '</nav>';
								}
								?>
							</div>
						<?php
						endif;
						?>

						<?php
						$menuDos = $menusModal['menu_dos'];
						if ($menuDos):
							?>

							<div class="menu-col">
								<div class="tag-wrapper">
									<h2><?php echo $menuDos['titulo']; ?></h2>
								</div>

								<?php
								$menuDosItems = wp_get_nav_menu_items($menuDos['menu_select']);
								if ($menuDosItems) {
									echo '<nav class="modal-menu-nav vertical">';
									echo '<ul>';
									foreach ($menuDosItems as $menuItem) {
										$classes = implode(' ', $menuItem->classes);
										$menuId = "modal-menu-item-" . $menuItem->ID;
										echo '<li class="' . $classes . ' ' . $menuId . '"><a href="' . $menuItem->url . '">' . $menuItem->title . '</a></li>';
									}
									echo '</ul>';
									echo '</nav>';
								}
								?>
							</div>
						<?php
						endif;
						?>

					</div>

				</div>
			</div>
		</div>

	</div>
</div>