<div class="modal-wrapper modal-wrapper-gallery">
	<div class="modal-content-wrapper">
		<div class="overlay"></div>

		<div class="modal-close">
			<span
				class="icon"><?php echo file_get_contents( get_template_directory() . '/assets/images/icons/icon-close-circle.svg' ); ?></span>
		</div>

		<div class="modal-content">
			<div class="container-custom-fluid">
				<div class="modal-content-gallery">
					<div class="swiper">
						<div class="swiper-wrapper">
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="swiper-button-prev">
			<span
				class="icon"><?php echo file_get_contents( get_template_directory() . '/assets/images/icons/icon-arrow-left.svg' ); ?></span>
		</div>
		<div class="swiper-button-next">
			<span
				class="icon"><?php echo file_get_contents( get_template_directory() . '/assets/images/icons/icon-arrow-right.svg' ); ?></span>
		</div>

	</div>
</div>