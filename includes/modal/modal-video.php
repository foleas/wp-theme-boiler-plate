<div class="modal-wrapper modal-wrapper-video">

	<div class="modal-content-wrapper">
		<div class="overlay"></div>

		<div class="modal-close">
			<span
				class="icon"><?php echo file_get_contents( get_template_directory() . '/assets/images/icons/icon-close-circle.svg' ); ?></span>
		</div>

		<div class="modal-content full-width">
			<div class="container-custom-fluid">
				<div class="modal-content-video">

				</div>
			</div>
		</div>

	</div>
</div>