<div class="modal-wrapper modal-wrapper-search layout-v2">
	<div class="modal-content-wrapper">
		<div class="overlay"></div>

		<div class="modal-header">
			<div class="container-custom-fluid">
				<div class="row">
					<div class="col-auto">
						<a class="logo light" href="<?php bloginfo('url'); ?>">
							<h4><span>Start</span><span>Today</span></h4>
						</a>
					</div>
					<div class="col-auto">
						<div class="modal-close"><span class="icon-close"></span></div>
					</div>
				</div>
			</div>
		</div>

		<div class="modal-content">
			<div class="modal-content-search">
				<div class="container-custom-fluid">
					<form class="autocomplete-search">
						<div class="input-wrapper with-icon with-pre-icon">

							<div class="pre-icon"><span class="icon-search"></span></div>

							<input type="text" name="s" id="search" value=""
										 placeholder="<?php _e('Buscar en', 'eaeblog');
										 echo ' ';
										 echo get_bloginfo('name'); ?>"/>

							<div class="autocomplete-erase"><span class="icon-close"></span></div>

							<button class="custom-btn only-icon" type="submit">
								<span class="icon-arrow-right"></span>
							</button>
						</div>

						<div class="results-wrapper">
							<div class="results-content">
								<div class="results-items">

								</div>
							</div>
							<div class="results-footer">
								<p class="qty"><span></span> <?php _e('resultados de búsqueda', 'eaeblog'); ?></p>
								<a href=""
									 class="custom-btn with-icon dark">
									<p><?php _e('Ver todo', 'eaeblog'); ?></p>
									<span class="icon-plus"></span>
								</a>
							</div>
						</div>

						<input type="hidden" name="action" value="search_autocomplete">
						<input type="hidden" name="nonce" value="<?php echo wp_create_nonce('search_autocomplete_nonce'); ?>">
					</form>
				</div>
			</div>
		</div>

	</div>
</div>