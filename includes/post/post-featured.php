<?php
$postID = '';
if ( array_key_exists( 'postID', $args ) ) {
	if ( $args['postID'] !== '' ) {
		$postID = $args['postID'];
	}
}
if ( $postID === '' ) {
	return;
}
?>
<section class="post-featured-wrapper">
	<div class="post-col img-col">
		<a href="<?php echo get_the_permalink( $postID ); ?>">
			<figure>
				<div class="gradient"></div>
		  <?php echo get_the_post_thumbnail( $postID, 'large' ); ?>
				<figcaption>
			<?php
			//echo '<p>' . get_post( get_post_thumbnail_id( $postID ) )->post_title . '</p>';
			echo '<p>' . __( 'Sello', 'quiniela' ) . '</p>';
			echo '<p>' . get_post( get_post_thumbnail_id( $postID ) )->post_excerpt . '</p>';
			//echo '<p>' . get_post( get_post_thumbnail_id( $postID ) )->post_content . '</p>';
			?>
				</figcaption>
			</figure>
		</a>
	</div>
	<div class="post-col info-col">
		<div class="info-wrapper">
			<h5 class="date"><?php echo get_the_date( 'd \d\e F, Y', $postID ); ?></h5>

			<h2><a href="<?php echo get_the_permalink( $postID ); ?>"><?php echo get_the_title( $postID ); ?></a></h2>

			<p class="excerpt"><?php echo get_the_excerpt( $postID ); ?></p>

			<div class="custom-btn-wrapper">
				<a href="<?php echo get_the_permalink( $postID ); ?>" class="custom-btn black">
					<p><?php _e( 'Ver Artículo', 'quiniela' ); ?></p>
				</a>
			</div>
		</div>
	</div>
</section>