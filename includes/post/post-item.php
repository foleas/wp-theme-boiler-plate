<?php
//print_r( $post );
$postID = get_the_ID();
if ( array_key_exists( 'postID', $args ) ) {
	if ( $args['postID'] !== '' ) {
		$postID = $args['postID'];
	}
}
if ( $postID === '' ) {
	return;
}
?>

<div class="post-item-wrapper item-type-<?php echo get_post_type( $postID ); ?>">
	<a href="<?php echo get_the_permalink( $postID ); ?>"></a>
	<article class="post-item">

		<section class="post-media">
			<figure><?php echo get_the_post_thumbnail( $postID, 'medium' ); ?></figure>
		</section>

		<section class="post-description">
			<div>
				<h4><?php echo get_the_title( $postID ); ?></h4>
				<p><?php echo get_the_excerpt( $postID ); ?></p>
			</div>
		</section>

	</article>
</div>