<?php
$titulo = '';
if ( array_key_exists( 'titulo', $args ) ) {
	if ( $args['titulo'] !== '' ) {
		$titulo = $args['titulo'];
	}
}

//	$shareTitle = ( $titulo ) ?: get_the_title();
//	$shareUrl   = ( is_category() ) ? get_term_link( get_queried_object()->term_id, 'category' ) : get_the_permalink();

if ( is_category() ) {
	$shareTitle = get_queried_object()->name;
	$shareUrl   = get_term_link( get_queried_object()->term_id, 'category' );
} elseif ( is_single() ) {
	$shareTitle = get_the_title();
	$shareUrl   = get_the_permalink();
} elseif ( is_page() ) {
	$shareTitle = get_the_title();
	$shareUrl   = get_page_link();
} elseif ( is_search() ) {
	$shareTitle = get_the_title();
	$shareUrl   = get_search_link();
}

if ( $titulo ) {
	$shareTitle = $titulo;
}

$facebookShareUrl = 'http://www.facebook.com/sharer/sharer.php?u=' . $shareUrl . '&t=' . $shareTitle;
$twitterShareUrl  = 'https://twitter.com/intent/tweet/?text=' . $shareTitle . '&url=' . $shareUrl;
$linkedinShareUrl = 'https://www.linkedin.com/sharing/share-offsite/?url=' . $shareUrl;
?>

<div class="share-wrapper">
	<p><?php _e( 'Compartir', 'quiniela' ); ?>:</p>
	<a class="share" data-share="facebook" href="<?php echo $facebookShareUrl; ?>" target="_blank" rel="noopener">
		<span class="icon-icon-facebook"></span>
	</a>
	<a class="share" data-share="twitter" href="<?php echo $twitterShareUrl; ?>" target="_blank" rel="noopener">
		<span class="icon-icon-twitter"></span>
	</a>
</div>