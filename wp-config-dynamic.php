<?php

/*** ENVIRONMENTS ************************************************************************/

switch ($_SERVER['HTTP_HOST']) {
    case ".test":
        define("ENVIRONMENT", "DEVELOPMENT");
        break;
    case "":
        define("ENVIRONMENT", "TESTING");
        break;
    default:
        define("ENVIRONMENT", "PRODUCTION");
}


switch (ENVIRONMENT) {
    case "DEVELOPMENT":
        $db_name = '_local_db';
        $db_user = 'root';
        $db_pass = '';
        $db_host = 'localhost';

        $wp_siteurl = 'http://.test/';
        $wp_home = 'http://.test/';
        break;
    case "TESTING":
        $db_name = '';
        $db_user = '';
        $db_pass = '';
        $db_host = 'localhost';

        $wp_siteurl = '/';
        $wp_home = '';
        break;
    case "PRODUCTION":
        $db_name = '';
        $db_user = '';
        $db_pass = '';
        $db_host = 'localhost';

        $wp_siteurl = '';
        $wp_home = '';

        break;
}


/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', $db_name);

/** MySQL database username */
define('DB_USER', $db_user);

/** MySQL database password */
define('DB_PASSWORD', $db_pass);

/** MySQL hostname */
define('DB_HOST', $db_host);

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'V~q_dt#9NFi+CE`f|9/0BHLwD(VrF`Qj* t^{}DSMFsfNoWpf(!dhJ%~]et7tFAq' );
define( 'SECURE_AUTH_KEY',  'm&pculov]cD.pONE*MVX3G%ucVIIgo7_fJg^DRS5{K+y66QWbuDF_cKt1fU]GeN%' );
define( 'LOGGED_IN_KEY',    'XPLSEg2~qsw]?XPN?Mf?.0lJ4^R4=W/<2W&6&*H +B&;:h2V{3%kF/mO<k*|8y{k' );
define( 'NONCE_KEY',        '#`WBi|[Qh&ECjI$XczhagvBS9a!+nr:=Eh_whKegK!wNu,)5C{3 ptw;Eiy6Ya$s' );
define( 'AUTH_SALT',        '[}O6P]zcr9G n>X>ZZ/^PvJSTY3I/}DPKPEh.99d_D8,Z^YS<Grsyi7{$Oz<Zp}V' );
define( 'SECURE_AUTH_SALT', '{5l VD?<Sd2w_2]%,A/w&LGuPTRV|SgT&Vh`jW3!z[G-7i?ux vf;@1>x`dQ_8^b' );
define( 'LOGGED_IN_SALT',   'nz_:<]f@Q^NoCDp@#jX;Cf*mkHBTJzyx@63a!L] ?X13vXx:&S;qJbk$tTDtbXV5' );
define( 'NONCE_SALT',       'Gpu[<.UkfRggJam!yT@)rEH`Ue4a(2: 9Efm6ouQ+HfC;0?XM~Wp#+>A$l>=#oSG' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
if (ENVIRONMENT === 'DEVELOPMENT') {
    define('WP_DEBUG', true);
}else{
    define('WP_DEBUG', false);
}

define('WC_TEMPLATE_DEBUG_MODE', false);
define( 'WP_AUTO_UPDATE_CORE', false );

define( 'WP_SITEURL', $wp_siteurl );
define( 'WP_HOME', $wp_home );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
