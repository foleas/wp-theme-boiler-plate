<!DOCTYPE html>
<html <?php language_attributes(); ?> data-scrolldir="down">

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php wp_title('|', true, 'right'); ?><?php bloginfo('name'); ?></title>
	<meta name="viewport" content="width=device-width, minimum-scale=1">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>

	<?php wp_head(); ?>

	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">

	<!-- Favicons
	================================================== -->
	<?php get_template_part('includes/favicon'); ?>


</head>
<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<header class="header-theme">

</header>

<main class="theme-main">