<?php
get_template_part('_functions/f', 'style-scripts');
get_template_part('_functions/f', 'basics');
get_template_part('_functions/f', 'general');

get_template_part('_functions/f', 'theme');
get_template_part('_functions/f', 'ajax');
get_template_part('_functions/f', 'acf-blocks');


if ( class_exists( 'WooCommerce' ) ) :
get_template_part('_functions/f', 'wc-login-register');
get_template_part('_functions/f', 'woocommerce');
get_template_part('woocommerce/custom-hooks/hooks', 'product');
get_template_part('woocommerce/custom-hooks/hooks', 'archive');
get_template_part('woocommerce/custom-hooks/hooks', 'cart');
get_template_part('woocommerce/custom-hooks/hooks', 'checkout');
get_template_part('woocommerce/custom-hooks/hooks', 'account');
get_template_part('woocommerce/custom-hooks/hooks', 'general');
endif;
