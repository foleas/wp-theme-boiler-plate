<?php get_header(); ?>

<?php
global $wp_query;
//print_r( $wp_query );
$block_custom_id = 'search_page';
$titulo          = __( 'Resultados para', 'text-domain' ) . ': ' . get_search_query();
$subtitulo       = $wp_query->found_posts . ' ' . __( 'resultados', 'text-domain' );
?>

<?php if ( have_posts() ) :
	//while ( have_posts() ) : the_post(); ?>

	<?php
	// GRID
	$post_grid_block_array = array(
		'custom_query'   => $wp_query,
		'mostrar_header' => true,
		'info_header'    => array(
			'titulo'     => $titulo,
			'sub_titulo' => $subtitulo
		)
	);

	render_acf_block( 'acf/text-domain-posts-grid', $block_custom_id, $post_grid_block_array );
	?>

	<?php //endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>