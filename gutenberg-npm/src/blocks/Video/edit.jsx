import { __ } from "@wordpress/i18n";
import {
  InspectorControls,
  MediaUpload,
  MediaUploadCheck,
} from "@wordpress/block-editor";
import {
  Button,
  PanelBody,
  ResponsiveWrapper,
  Spinner,
  TextControl,
} from "@wordpress/components";
import { compose } from "@wordpress/compose";
import { withSelect } from "@wordpress/data";
import FeaturedMedia from "../../components/FeaturedMedia";

const ALLOWED_MEDIA_TYPES = ["image"];

const VideoBlockEdit = (props) => {
  const { attributes, setAttributes, previewImage } = props;
  const { videoUrl, previewImageId } = attributes;

  const instructions = (
    <p>
      {__(
        "Para editar la imagen de fondo, necesita permiso para cargar medios.",
        "eaeblog"
      )}
    </p>
  );

  const onUpdateImage = (image) => {
    setAttributes({
      previewImageId: image.id,
    });
  };
  const onRemoveImage = () => {
    setAttributes({
      previewImageId: undefined,
    });
  };

  // console.log({ previewImage });

  return (
    <>
      <InspectorControls style={{ marginBottom: "40px" }}>
        <PanelBody
          title={__("Configuración del Video", "eaeblog")}
          initialOpen={true}
        >
          <TextControl
            label={__("Video URL ", "eaeblog")}
            placeholder={__("Ingrese la URL del Video", "eaeblog")}
            value={videoUrl}
            onChange={(videoUrl) => setAttributes({ videoUrl })}
          />

          <div className="">
            <MediaUploadCheck fallback={instructions}>
              <MediaUpload
                title={__("Imagen preview", "eaeblog")}
                allowedTypes={ALLOWED_MEDIA_TYPES}
                onSelect={onUpdateImage}
                type="image"
                value={previewImageId}
                render={({ open }) => {
                  return (
                    <Button
                      className={
                        !previewImageId
                          ? "editor-post-featured-image__toggle"
                          : "editor-post-featured-image__preview"
                      }
                      onClick={open}
                    >
                      {!previewImageId &&
                        __("Elegir imagen para preview", "eaeblog")}

                      {previewImageId && !previewImage && <Spinner />}
                      {previewImageId && previewImage && (
                        <ResponsiveWrapper
                          naturalWidth={previewImage?.media_details.width}
                          naturalHeight={previewImage?.media_details.height}
                        >
                          <img
                            src={previewImage?.source_url}
                            alt={__("Imagen Preview", "eaeblog")}
                          />
                        </ResponsiveWrapper>
                      )}
                    </Button>
                  );
                }}
              />
            </MediaUploadCheck>
            {previewImageId && (
              <MediaUploadCheck>
                <Button onClick={onRemoveImage} isLink isDestructive>
                  {__("Remover imagen para preview", "eaeblog")}
                </Button>
              </MediaUploadCheck>
            )}
          </div>
        </PanelBody>
      </InspectorControls>
      <div className="eaeblog-block-wrapper eaeblog-custom-video-block">
        <div className="block-content-wrapper">
          <p>{videoUrl}</p>
          <div className="block-preview-image-wrapper">
            {previewImageId && previewImage && (
              <FeaturedMedia media={previewImage} />
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default compose(
  withSelect((select, props) => {
    const { getMedia } = select("core");
    const { attributes, setAttributes } = props;
    const { previewImageId } = attributes;

    return {
      previewImage: previewImageId ? getMedia(previewImageId) : null,
    };
  })
)(VideoBlockEdit);
