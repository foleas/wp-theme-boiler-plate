import FeaturedMedia from "../../components/FeaturedMedia";

const VideoBlockSave = (props) => {
  console.log({ props });
  const { attributes, previewImage } = props;
  const { videoUrl, previewImageId } = attributes;

  let image;

  const imgAPI = "http://eaeblog.test/wp-json/wp/v2/media/" + previewImageId;
  fetch(imgAPI)
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      image = data;
    });

  console.log({ imgAPI });

  return (
    <div className="eaeblog-block-wrapper eaeblog-custom-video-block">
      <div className="block-content-wrapper">
        <p>{videoUrl}</p>
        <div className="block-preview-image-wrapper">
          {previewImageId && image && <FeaturedMedia media={image} />}
        </div>
      </div>
    </div>
  );
};

export default VideoBlockSave;
