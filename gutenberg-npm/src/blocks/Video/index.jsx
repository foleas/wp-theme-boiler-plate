import edit from "./edit";
import save from "./save";
import { __ } from "@wordpress/i18n";
import { registerBlockType } from "@wordpress/blocks";

registerBlockType("eaeblog/custom-video-block", {
  title: __("Custom Video Block", "eaeblog"),
  icon: "format-video",
  category: "eaeblog",
  keywords: ["eaeblog", "custom", "video"],
  attributes: {
    videoUrl: {
      type: "string",
    },
    previewImageId: {
      type: "number",
      default: null,
    },
    previewImageObj: {
      type: "string",
      default: null,
    },
  },
  edit,
  save,
});
