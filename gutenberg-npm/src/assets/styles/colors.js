export const colors = {
  mainColor: "#EE2737",
  secondaryColor: "#25282A",
  black: "#000000",
  white: "#FFFFFF",
  gray: "#BBBCBC",
  darkGray: "#97999B",
  lightGray: "#D9D9D6",
};

export const paletteColors = [
  {
    name: "mainColor",
    color: "#EE2737",
  },
  { name: "secondaryColor", color: "#25282A" },
  { name: "black", color: "#000000" },
  { name: "white", color: "#FFFFFF" },
  { name: "gray", color: "#BBBCBC" },
  { name: "darkGray", color: "#97999B" },
  { name: "lightGray", color: "#D9D9D6" },
];
