// import React from "react";

const FeaturedMedia = (props) => {
  const { media, size = "full", onLoaded } = props;
  if (!media) return null;

  const mediaDetails = media.media_details;
  const mediaSize = mediaDetails.sizes[size];
  let mediaSrc = "";
  let mediaWidht = "";
  let mediaHeight = "";
  let srcset = "";
  let sizes = "";
  let limit = 1600;

  mediaSrc = mediaSize ? mediaSize.source_url : media.source_url;
  mediaWidht = mediaSize ? mediaSize.width : mediaDetails.width;
  mediaHeight = mediaSize ? mediaSize.height : mediaDetails.height;
  sizes = `(max-width: ${mediaWidht}px) 100vw, ${mediaWidht}px`;

  if (mediaWidht > limit) limit = mediaWidht;

  const newMediaSizes = Object.keys(mediaDetails.sizes).reduce(
    (object, key) => {
      if (key !== "thumbnail") {
        object[key] = mediaDetails.sizes[key];
      }
      return object;
    },
    {}
  );

  srcset =
    Object.values(newMediaSizes)
      .filter((item) => item.width <= limit)
      // Get the url and width of each size.
      .map((item) => [item.source_url, item.width])
      // Recude them to a string with the format required by `srcset`.
      .reduce(
        (final, current, index, array) =>
          final.concat(
            `${current.join(" ")}w${index !== array.length - 1 ? ", " : ""}`
          ),
        ""
      ) || null;
  return (
    <img
      width={mediaWidht}
      height={mediaHeight}
      src={mediaSrc}
      className={`attachment-${size} size-${size}`}
      alt={media.alt_text}
      // loading="lazy"
      srcSet={srcset}
      sizes={sizes}
      // title={media.title.rendered}
      onLoad={onLoaded}
    />
  );
};

export default FeaturedMedia;
