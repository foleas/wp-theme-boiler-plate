import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import {
  RichText,
  InspectorControls,
  ColorPalette,
  MediaUpload,
  InnerBlocks,
  BlockControls,
  AlignmentToolbar,
} from "@wordpress/block-editor";
import { Button, PanelBody, RangeControl } from "@wordpress/components";
import { paletteColors } from "./assets/styles/colors";

const ALLOWED_BLOCKS = [
  "core/paragraph",
  "core/heading",
  "core/list",
  "core/button",
];

registerBlockType("eaeblog/custom-tutorial-block", {
  title: __("Custom Tutorial Block", "eaeblog"),
  icon: "media-text",
  category: "eaeblog",
  keywords: ["eaeblog", "custom", "text"],
  attributes: {
    title: {
      type: "string",
      source: "html",
      selector: "h2",
    },
    body: {
      type: "string",
      source: "html",
      selector: "p",
    },
    alignment: {
      type: "string",
      default: "left",
    },
    backgroundColor: {
      type: "string",
      default: null,
    },
    backgroundImage: {
      type: "string",
      default: null,
    },
    overlayColor: {
      type: "string",
      default: null,
    },
    overlayOpacity: {
      type: "number",
      default: 0.1,
    },
  },
  edit: ({ attributes, setAttributes }) => {
    const {
      title,
      body,
      alignment,
      backgroundColor,
      backgroundImage,
      overlayColor,
      overlayOpacity,
    } = attributes;
    return [
      <InspectorControls style={{ marginBottom: "40px" }}>
        <PanelBody title={__("Configuración del bloque", "eaeblog")}>
          <p>
            <strong>{__("Seleccione el color de fondo", "eaeblog")}</strong>
          </p>
          <ColorPalette
            value={backgroundColor}
            colors={paletteColors}
            onChange={(value) => setAttributes({ backgroundColor: value })}
          />
          <p style={{ marginTop: "20px" }}>
            <strong>{__("Seleccione la imagen de fondo", "eaeblog")}</strong>
          </p>
          <MediaUpload
            onSelect={(media) => {
              console.log(media);
              setAttributes({ backgroundImage: media.sizes.full.url });
            }}
            type="image"
            value={backgroundImage}
            render={({ open }) => {
              return (
                <Button
                  onClick={open}
                  icon="upload"
                  className="button"
                  style={{ display: "inline-flex" }}
                >
                  {__("Elegir imagen de fondo", "eaeblog")}
                </Button>
              );
            }}
          />
          <p style={{ marginTop: "20px" }}>
            <strong>{__("Seleccione el color del overlay", "eaeblog")}</strong>
          </p>
          <ColorPalette
            value={overlayColor}
            colors={paletteColors}
            onChange={(value) => setAttributes({ overlayColor: value })}
          />
          <RangeControl
            label={__("Opacidad del overlay")}
            value={overlayOpacity}
            min={0}
            max={1}
            step={0.05}
            onChange={(value) => setAttributes({ overlayOpacity: value })}
          />
        </PanelBody>
      </InspectorControls>,
      <div
        className="eaeblog-block-wrapper eaeblog-custom-tutorial-block"
        style={{ backgroundColor: backgroundColor }}
      >
        <div className="block-bgimage-wrapper">
          {backgroundImage && <img src={backgroundImage} alt="background" />}
          {overlayColor && (
            <div
              className="block-overlay"
              style={{
                backgroundColor: overlayColor,
                opacity: overlayOpacity,
              }}
            />
          )}
        </div>
        <div className="block-content-wrapper">
          {
            <BlockControls>
              <AlignmentToolbar
                value={alignment}
                onChange={(value) => setAttributes({ alignment: value })}
              />
            </BlockControls>
          }
          <RichText
            key="editable"
            tagName="h2"
            placeholder={__("Ingrese el Título", "eaeblog")}
            value={title}
            onChange={(title) => setAttributes({ title })}
            style={{ textAlign: alignment }}
          />
          <RichText
            key="editable"
            tagName="p"
            placeholder={__("Ingrese el Texto", "eaeblog")}
            value={body}
            onChange={(body) => setAttributes({ body })}
            style={{ textAlign: alignment }}
          />
          <InnerBlocks allowedBlocks={ALLOWED_BLOCKS} />
        </div>
      </div>,
    ];
  },
  save: ({ attributes }) => {
    const {
      title,
      body,
      alignment,
      backgroundColor,
      backgroundImage,
      overlayColor,
      overlayOpacity,
    } = attributes;
    return (
      <div
        className="eaeblog-block-wrapper eaeblog-custom-tutorial-block"
        style={{ backgroundColor: backgroundColor }}
      >
        <div className="block-bgimage-wrapper">
          {backgroundImage && <img src={backgroundImage} alt="background" />}
          {overlayColor && (
            <div
              className="block-overlay"
              style={{
                backgroundColor: overlayColor,
                opacity: overlayOpacity,
              }}
            />
          )}
        </div>
        <div className="block-content-wrapper">
          <RichText.Content
            tagName="h2"
            value={title}
            style={{ textAlign: alignment }}
          />
          <RichText.Content
            tagName="p"
            value={body}
            style={{ textAlign: alignment }}
          />
          <InnerBlocks.Content />
        </div>
      </div>
    );
  },
});
