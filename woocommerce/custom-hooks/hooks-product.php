<?php
//===================================================
// SINGLE PRODUCT
//===================================================

// AGREGAR CONTAINER DIV
add_action('woocommerce_before_single_product_summary', 'woocommerce_product_container_div_open', 1);
if (!function_exists('woocommerce_product_container_div_open')) {
    function woocommerce_product_container_div_open()
    {
        echo '<div class="container-fluid">';
    }
}
add_action('woocommerce_after_single_product_summary', 'woocommerce_product_container_div_close', 50);
if (!function_exists('woocommerce_product_container_div_close')) {
    function woocommerce_product_container_div_close()
    {
        echo '</div>';
    }
}

// REDIRECT TO CART WHEN ADD TO CART
add_filter('woocommerce_add_to_cart_redirect', 'custom_add_to_cart_redirect');
if (!function_exists('custom_add_to_cart_redirect')) {
    function custom_add_to_cart_redirect($url)
    {
        $url = wc_get_cart_url();
        return $url;
    }
}

// REMOVER NOTICES
//remove_action('woocommerce_before_single_product', 'woocommerce_output_all_notices', 10);

// REMOVER ON SALE
remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);

// REMOVER IMAGENES
remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20);

// REMOVER TODO EL SUMMARY
//remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
//remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
//remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
//remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
//remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
//remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
//remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 20);

remove_action('woocommerce_single_variation', 'woocommerce_single_variation', 10);
//add_action('woocommerce_after_single_variation' , 'woocommerce_single_variation' , 10);

// ADD CUSTOM SINGLE PRODUCT MAIN
add_action('woocommerce_before_single_product_summary', 'woocommerce_template_single_product_main', 10);
if (!function_exists('woocommerce_template_single_product_main')) {
    function woocommerce_template_single_product_main()
    {
        global $product;
        $echo_html = '';

        $echo_html .= '<section class="single-product-main-wrapper">';
        $echo_html .= '<div class="row">';

        $echo_html .= '<div class="col-12 col-md-6">';
        $echo_html .= '<div class="single-product-main-img">';
        ob_start();
        wc_get_template('single-product/product-gallery.php');
        $echo_html .= ob_get_contents();
        ob_end_clean();
        $echo_html .= '</div>';
        $echo_html .= '</div>';

        $echo_html .= '<div class="col-12 col-md-6">';
        $echo_html .= '<div class="single-product-main-info">';
        ob_start();
        woocommerce_template_single_title();
        wc_get_template('single-product/custom-extra-info.php');
        woocommerce_template_single_excerpt();
        woocommerce_template_single_add_to_cart();
//        wc_get_template('single-product/custom-share.php');
        get_template_part('includes/general/general', 'share');
        wc_get_template('single-product/custom-extra-media.php');

        $echo_html .= ob_get_contents();
        ob_end_clean();
        $echo_html .= '</div>';
        $echo_html .= '</div>';

        $echo_html .= '</div>';
        $echo_html .= '</section>';

        echo $echo_html;
    }
}

// REMOVER EXTRA INFO
//remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
//remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

// FORCE PRICE SHOWING - WHEN HAS THE SAME PRICE
add_filter('woocommerce_show_variation_price',      function() { return TRUE;});


// PRODUCT CUSTOM TABS
add_filter('woocommerce_product_tabs', 'my_shipping_tab');
function my_shipping_tab($tabs)
{
//    var_dump($tabs);
    global $product;
    $current_product_id = $product->get_id();

    unset($tabs['additional_information']);

    $tabs['description'] = array(
        'title' => __('Dettagli', 'airbag-professional'),
        'priority' => 10,
        'callback' => 'description_custom_content_tab',
    );

    if (get_field('garanzia', $current_product_id)) {
        $tabs['garanzia'] = array(
            'title' => __('Garanzia', 'airbag-professional'),
            'priority' => 50,
            'callback' => 'garanzia_content_tab',
        );
    }

    if (get_field('garanzia_commerciale', $current_product_id)) {
        $tabs['garanzia_commerciale'] = array(
            'title' => __('Garanzia commerciale', 'airbag-professional'),
            'priority' => 60,
            'callback' => 'garanzia_commerciale_content_tab',
        );
    }

    if (get_field('garanzia_di_conformita', $current_product_id)) {
        $tabs['garanzia_di_conformita'] = array(
            'title' => __('Garanzia di Conformita', 'airbag-professional'),
            'priority' => 70,
            'callback' => 'garanzia_di_conformita_content_tab',
        );
    }

    if (get_field('azionamento', $current_product_id)) {
        $tabs['azionamiento'] = array(
            'title' => __('Azionamento', 'airbag-professional'),
            'priority' => 80,
            'callback' => 'azionamento_content_tab',
        );
    }

    if (get_field('help', $current_product_id)) {
        $tabs['help'] = array(
            'title' => __('Help', 'airbag-professional'),
            'priority' => 90,
            'callback' => 'help_content_tab',
        );
    }

    if (get_field('taglia', $current_product_id)) {
        $tabs['taglia'] = array(
            'title' => __('Taglia', 'airbag-professional'),
            'priority' => 100,
            'callback' => 'taglia_content_tab',
        );
    }

    return $tabs;
}

function description_custom_content_tab()
{
    the_content();
}

function garanzia_content_tab()
{
    global $product;
    echo get_field('garanzia', $product->get_id());
}

function garanzia_commerciale_content_tab()
{
    global $product;
    echo get_field('garanzia_commerciale', $product->get_id());
}

function garanzia_di_conformita_content_tab()
{
    global $product;
    echo get_field('garanzia_di_conformita', $product->get_id());
}

function azionamento_content_tab()
{
    global $product;
    echo get_field('azionamento', $product->get_id());
}


function help_content_tab()
{
    global $product;
    echo get_field('help', $product->get_id());
}

function taglia_content_tab()
{
    global $product;
    echo get_field('taglia', $product->get_id());
}

// SHOP FEATURES
add_action('woocommerce_after_single_product_summary', 'woocommerce_output_shop_features', 1);
if (!function_exists('woocommerce_output_shop_features')) {
    function woocommerce_output_shop_features()
    {
        get_template_part('includes/general/general', 'shop_features');
    }
}