<?php

//===================================================
// ATTRIBUTE SELECT CUSTOM FUNCTION
//===================================================

if (!function_exists('wc_dropdown_variation_attribute_options_CUSTOM')) {

    /**
     * Output a list of variation attributes for use in the cart forms.
     *
     * @param array $args Arguments.
     * @since 2.4.0
     */
    function wc_dropdown_variation_attribute_options_CUSTOM($args = array())
    {
        $args = wp_parse_args(
            apply_filters('woocommerce_dropdown_variation_attribute_options_args', $args),
            array(
                'options' => false,
                'attribute' => false,
                'product' => false,
                'selected' => false,
                'name' => '',
                'id' => '',
                'class' => '',
                'show_option_none' => false,
                //'show_option_none' => __('Choose an option', 'woocommerce'),
            )
        );

        // Get selected value.
        if (false === $args['selected'] && $args['attribute'] && $args['product'] instanceof WC_Product) {
            $selected_key = 'attribute_' . sanitize_title($args['attribute']);
            // phpcs:disable WordPress.Security.NonceVerification.Recommended
            $args['selected'] = isset($_REQUEST[$selected_key]) ? wc_clean(wp_unslash($_REQUEST[$selected_key])) : $args['product']->get_variation_default_attribute($args['attribute']);
            // phpcs:enable WordPress.Security.NonceVerification.Recommended
        }

        $options = $args['options'];
        $product = $args['product'];
        $attribute = $args['attribute'];
        $name = $args['name'] ? $args['name'] : 'attribute_' . sanitize_title($attribute);
        $id = $args['id'] ? $args['id'] : sanitize_title($attribute);
        $class = $args['class'];
        $show_option_none = (bool)$args['show_option_none'];
        $show_option_none_text = $args['show_option_none'] ? $args['show_option_none'] : __('Choose an option', 'woocommerce'); // We'll do our best to hide the placeholder, but we'll need to show something when resetting options.

        if (empty($options) && !empty($product) && !empty($attribute)) {
            $attributes = $product->get_variation_attributes();
            $options = $attributes[$attribute];
        }

        $html = '<select id="' . esc_attr($id) . '" class="' . esc_attr($class) . '" name="' . esc_attr($name) . '" data-attribute_name="attribute_' . esc_attr(sanitize_title($attribute)) . '" data-show_option_none="' . ($show_option_none ? 'yes' : 'no') . '">';
        $html .= '<option value="">' . esc_html($show_option_none_text) . '</option>';

        if (!empty($options)) {
            if ($product && taxonomy_exists($attribute)) {
                // Get terms if this is a taxonomy - ordered. We need the names too.
                $terms = wc_get_product_terms(
                    $product->get_id(),
                    $attribute,
                    array(
                        'fields' => 'all',
                    )
                );

                // CAMBIO
                $available_variations = $product->get_available_variations();

                foreach ($terms as $term) {
                    if (in_array($term->slug, $options, true)) {

                        // CAMBIO
                        $option_value = esc_html(apply_filters('woocommerce_variation_option_name', $term->name, $term, $attribute, $product));

                        $variation_desc = get_variation_description_from_current_attr($available_variations, $attribute, $term);
                        if ($variation_desc != '') $option_value = $variation_desc;

                        $html .= '<option value="' . esc_attr($term->slug) . '" ' . selected(sanitize_title($args['selected']), $term->slug, false) . '>' . $option_value . '</option>';
                    }
                }
            } else {
                foreach ($options as $option) {
                    // This handles < 2.4.0 bw compatibility where text attributes were not sanitized.
                    $selected = sanitize_title($args['selected']) === $args['selected'] ? selected($args['selected'], sanitize_title($option), false) : selected($args['selected'], $option, false);
                    $html .= '<option value="' . esc_attr($option) . '" ' . $selected . '>' . esc_html(apply_filters('woocommerce_variation_option_name', $option, null, $attribute, $product)) . '</option>';
                }
            }
        }

        $html .= '</select>';

        // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
        echo apply_filters('woocommerce_dropdown_variation_attribute_options_html', $html, $args);
    }
}

function get_variation_description_from_current_attr($variations, $attribute, $term)
{
    $return = '';

    foreach ($variations as $variation) {

        $flag = true;

        if ($variation['variation_description']) {
            //print_r($variation);

            $variation_attrs = wc_get_product_variation_attributes($variation['variation_id']);

            if ($variation_attrs['attribute_' . $attribute] === $term->slug) {
//                print_r($variation);
                foreach ($variation_attrs as $key => $value) {
                    if ($key !== 'attribute_' . $attribute) {
                        if ($value !== '' && $value !== 'nessuna' && $value !== 'none') {
                            $flag = false;
                            break;
                        }
                    }
                }

                if ($flag) $return = $variation['variation_description'];
            }

        }

    }// FOREACH VARIATIONS

    return $return;
}


//===================================================
// Shipping by Weight
//===================================================

/**
 * @snippet       Shipping by Weight | WooCommerce
 * @sourcecode    https://businessbloomer.com/?p=21432
 * @author        Rodolfo Melogli
 * @compatible    WooCommerce 3.7
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */

add_filter('woocommerce_package_rates', 'woocommerce_calculate_shipping_by_weight', 9999, 2);

function woocommerce_calculate_shipping_by_weight($rates, $package)
{

//    var_dump($rates);
//    print_r($package);
    $destination = $package['destination'];
    $country = $destination['country'];

    $total_weight = WC()->cart->get_cart_contents_weight();

    $shipping_packages = WC()->cart->get_shipping_packages();
    $shipping_zone = wc_get_shipping_zone(reset($shipping_packages));
    $zone_id = $shipping_zone->get_id(); // Get the zone ID
//    $zone_name = $shipping_zone->get_zone_name(); // Get the zone name

    //ZONE ID 4 ZONE NAME Italia Resto - Spedizione a peso

    if ($country !== 'IT') {

        if ($total_weight > 0 && $total_weight < 25) {
            if (isset($rates['flat_rate:2'])) unset($rates['flat_rate:3']);
        } else {
            if (isset($rates['flat_rate:3'])) unset($rates['flat_rate:2']);
        }

    } else {
        // resto italia 6 - 7 - 8
        if ($zone_id == 4) {
            if ($total_weight > 0 && $total_weight < 15) {
                if (isset($rates['flat_rate:6'])) unset($rates['flat_rate:7'] , $rates['flat_rate:8']);
            } elseif ($total_weight >= 15 && $total_weight < 25) {
                if (isset($rates['flat_rate:7'])) unset($rates['flat_rate:6'] , $rates['flat_rate:8']);
            } else {
                if (isset($rates['flat_rate:8'])) unset($rates['flat_rate:6'] , $rates['flat_rate:7']);
            }
        }

        // italia zona 1 - 4 - 5
        if ($zone_id == 1) {
            if ($total_weight > 0 && $total_weight < 15) {
                if (isset($rates['flat_rate:1'])) unset($rates['flat_rate:4'] , $rates['flat_rate:5']);
            } elseif ($total_weight >= 15 && $total_weight < 25) {
                if (isset($rates['flat_rate:4'])) unset($rates['flat_rate:1'] , $rates['flat_rate:5']);
            } else {
                if (isset($rates['flat_rate:5'])) unset($rates['flat_rate:1'] , $rates['flat_rate:4']);
            }
        }

    }


    return $rates;

}