<?php

//===================================================
// CHECKOUT
//===================================================

// REMOVE COUPONG FORM
remove_action('woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10);

// REMOVE LOGIN FORM
remove_action('woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10);

function get_icon_url($country)
{
    $url = 'https://www.paypal.com/' . strtolower($country);
    $home_counties = array('BE', 'CZ', 'DK', 'HU', 'IT', 'JP', 'NL', 'NO', 'ES', 'SE', 'TR', 'IN');
    $countries = array('DZ', 'AU', 'BH', 'BQ', 'BW', 'CA', 'CN', 'CW', 'FI', 'FR', 'DE', 'GR', 'HK', 'ID', 'JO', 'KE', 'KW', 'LU', 'MY', 'MA', 'OM', 'PH', 'PL', 'PT', 'QA', 'IE', 'RU', 'BL', 'SX', 'MF', 'SA', 'SG', 'SK', 'KR', 'SS', 'TW', 'TH', 'AE', 'GB', 'US', 'VN');

    if (in_array($country, $home_counties, true)) {
        return $url . '/webapps/mpp/home';
    } elseif (in_array($country, $countries, true)) {
        return $url . '/webapps/mpp/paypal-popup';
    } else {
        return $url . '/cgi-bin/webscr?cmd=xpt/Marketing/general/WIPaypal-outside';
    }
}

add_filter('woocommerce_gateway_icon', 'change_gateway_icon', 10, 2);
function change_gateway_icon($icon, $id)
{
    $return_html = '';
    if ($id === 'paypal') {

        $return_html .= '<span>' . __('Paga con PayPal oppure Carta di credito', 'airbag-professional') . '</span>';
        $return_html .= '<div class="supported-payments">';
        $return_html .= '<img src="' . get_stylesheet_directory_uri() . '/assets/images/cards/paypal_small.jpg" title="PayPal" alt="PayPal">';
        $return_html .= ' / ';
        $return_html .= '<img src="' . get_stylesheet_directory_uri() . '/assets/images/cards/visa.jpg" title="Visa" alt="Visa">';
        $return_html .= '<img src="' . get_stylesheet_directory_uri() . '/assets/images/cards/mastercard.jpg" title="Mastercard" alt="Mastercard">';
        $return_html .= '<img src="' . get_stylesheet_directory_uri() . '/assets/images/cards/american_express.jpg" title="American Express" alt="Amex">';
        $return_html .= '<img src="' . get_stylesheet_directory_uri() . '/assets/images/cards/postepay.jpg" title="PostePay" alt="PostePay">';
        $return_html .= '<img src="' . get_stylesheet_directory_uri() . '/assets/images/cards/discover.jpg" title="Discover" alt="Discover">';
        $return_html .= '<img src="' . get_stylesheet_directory_uri() . '/assets/images/cards/aura.jpg" title="Aura" alt="Aura">';
        $return_html .= '<img src="' . get_stylesheet_directory_uri() . '/assets/images/cards/prepagata_paypal.jpg" title="PayPal Prepagata" alt="PayPal Prepagata">';
        $return_html .= '</div>';
        $base_country = WC()->countries->get_base_country();
        $return_html .= sprintf('<a href="%1$s" class="about_paypal" onclick="javascript:window.open(\'%1$s\',\'WIPaypal\',\'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700\'); return false;">' . esc_attr__('What is PayPal?', 'woocommerce') . '</a>', esc_url(get_icon_url($base_country)));

    } else {
        return $icon;
    }

    return $return_html;
}


add_action('woocommerce_checkout_before_customer_details', 'back_to_cart_checkout', 20);
if (!function_exists('back_to_cart_checkout')) {
    function back_to_cart_checkout()
    {
        echo '<a href="' . wc_get_cart_url() . '" class="go-back-btn"><span>' . __('Torna al carrello', 'airbag-professional') . '</span></a>';
    }
}

remove_action('woocommerce_checkout_order_review', 'woocommerce_order_review', 10);
remove_action('woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20);

add_action('woocommerce_checkout_order_review', 'woocommerce_order_review_CUSTOM', 10);
if (!function_exists('woocommerce_order_review_CUSTOM')) {
    function woocommerce_order_review_CUSTOM()
    {
        wc_get_template(
            'checkout/review-order-custom.php',
            array(
                'checkout' => WC()->checkout(),
            )
        );
    }
}
// UPDATE CHECKOUT REVIEW
add_filter('woocommerce_update_order_review_fragments', 'filter_woocommerce_update_order_review_fragments', 10, 1);
if (!function_exists('filter_woocommerce_update_order_review_fragments')) {
    function filter_woocommerce_update_order_review_fragments($fragments)
    {
        ob_start();
        wc_get_template('checkout/review-order-custom.php');
        $fragments['.woocommerce-checkout-review-order-table-wrapper'] = ob_get_clean();
        // CHECKOUT

        return $fragments;
    }
}


// TERMS AND CONDITION
remove_action('woocommerce_checkout_terms_and_conditions', 'wc_checkout_privacy_policy_text', 20);

//===================================================
// CUSTOM FIELDS CHECKOUT ORDER
//===================================================

// REMOVE OPTIONS SPAN
add_filter('woocommerce_form_field', 'elex_remove_checkout_optional_text', 10, 4);
function elex_remove_checkout_optional_text($field, $key, $args, $value)
{
    if (is_checkout() && !is_wc_endpoint_url()) {
        $optional = '&nbsp;<span class="optional">(' . esc_html__('optional', 'woocommerce') . ')</span>';
        $field = str_replace($optional, '', $field);
    }
    return $field;
}

//add_filter('woocommerce_checkout_fields', 'custom_add_checkout_fields');


add_filter( 'woocommerce_default_address_fields' , 'override_default_address_fields' );
function override_default_address_fields( $address_fields ) {
    $address_fields['address_1']['label'] = __('Indirizzo' , 'airbag-professional');
    return $address_fields;
}

// CUSTOM FIELDS

add_action('woocommerce_billing_fields', 'custom_billing_fields');
if (!function_exists('custom_billing_fields')) {
    function custom_billing_fields($fields)
    {
        //print_r($fields);

        $fields['billing_professional'] = array(
            'type' => 'select',
            'label' => __('Sei un professionista?', 'airbag-professional'),
            'required' => true,
            'class' => array('form-row-first'),
            'input_class' => array('select2'),
            'options' => array('yes' => __('Sì', 'airbag-professional'), 'no' => __('No', 'airbag-professional')),
            'priority' => '21',
        );

        $fields['billing_fiscal_code'] = array(
            'type' => 'text',
            'label' => __('Codice Fiscale', 'airbag-professional'),
//            'required' => true,
            'class' => array('form-row-last'),
            'priority' => '22',
        );

        $fields['billing_company']['class'] = array('form-row-first', 'hidden-custom-field');
//        $fields['billing_company']['required'] = 'false';

        $fields['billing_vat_number'] = array(
            'type' => 'text',
            'label' => __('Numero Partita Iva', 'airbag-professional'),
//            'required' => true,
            'class' => array('form-row-last', 'hidden-custom-field'),
            'priority' => '31',
        );

        $fields['billing_pec_email'] = array(
            'type' => 'email',
            'label' => __('PEC email', 'airbag-professional'),
//            'required' => true,
            'class' => array('form-row-first', 'hidden-custom-field'),
            'priority' => '32',
        );

        $fields['billing_sdi_code'] = array(
            'type' => 'text',
            'label' => __('Codice Dest. SDI', 'airbag-professional'),
//            'required' => true,
            'class' => array('form-row-last', 'hidden-custom-field'),
            'priority' => '33',
        );

        $fields['billing_excluded_invoice'] = array(
            'type' => 'checkbox',
            'label' => '<span>' . __('Escluso dalla Fatturazione Elettronica', 'airbag-professional') . '</span>',
            'required' => false,
            'class' => array('form-row-wide', 'checkbox-wrapper', 'hidden-custom-field'),
            //'label_class' => array('checkbox-wrapper'),
            //'input_class' => array('show-extra-fields'),
            'priority' => '33',
        );

//        $fields['billing_address_1']['label'] = __('Indirizzo' , 'airbag-professional');

        unset($fields['billing_address_2']);

        $fields['billing_country']['priority'] = '61';
        $fields['billing_country']['class'] = array('form-row-first');
        $fields['billing_country']['description'] = '<a href="/contatti" target="_blank">(<strong>' . __('Non trovi il tuo paese?', 'airbag-professional') . '</strong> ' . __('Clicca qui e chiedici un preventivo personalizzato', 'airbag-professional') . ')</a>';

        $fields['billing_email']['priority'] = '70';
        $fields['billing_email']['class'] = array('form-row-first');

        $fields['billing_phone']['priority'] = '71';
        $fields['billing_phone']['class'] = array('form-row-last');

        return $fields;
    }
}

// SCRIPTS PARA MOSTRAR/ESCONDE - REQUERIDOS - CAMPOS DEL CHECKOUT
add_action('woocommerce_after_checkout_form', 'scripts_for_show_hide_fields_checkout', 6);
add_action('woocommerce_after_edit_account_address_form', 'scripts_for_show_hide_fields_checkout', 6);
function scripts_for_show_hide_fields_checkout()
{
    ?>
    <script>
        (function ($) {

            $(document).ready(function () {

                var required_html = '<abbr class="required" title="<?php echo esc_attr__('required', 'woocommerce'); ?>>">*</abbr>'; // Required html

                // BILLING PROFESSIONAL
                var billingProfessional = $("#billing_professional");
                billing_professional_change(billingProfessional.val(),0);

                billingProfessional.on("change", function () {
                    var esteVal = $(this).val();
                    billing_professional_change(esteVal,150);
                });

                function billing_professional_change( value , animTime){
                    if (value === 'yes') {
                        $(".hidden-custom-field").slideDown(animTime, 'easeInOutCubic');
                        field_remove_required($("#billing_fiscal_code_field"));
                        field_make_required($("#billing_company_field"));
                        field_make_required($("#billing_vat_number_field"));
                    } else {
                        $(".hidden-custom-field").slideUp(animTime, 'easeInOutCubic');
                        field_make_required($("#billing_fiscal_code_field"));
                        field_remove_required($("#billing_company_field"));
                        field_remove_required($("#billing_vat_number_field"));
                    }
                }

                function field_make_required( field_elem) {
                    field_elem.removeClass("woocommerce-validated woocommerce-invalid woocommerce-invalid-required-field");
                    field_elem.addClass('validate-required');
                    $("label abbr" , field_elem).remove();
                    $("label" , field_elem).append(" " + required_html);
                }

                function field_remove_required( field_elem ) {
                    field_elem.removeClass("woocommerce-validated woocommerce-invalid woocommerce-invalid-required-field");
                    field_elem.removeClass('validate-required');
                    $("label abbr" , field_elem).remove();
                }

            });


        })(jQuery);

    </script>
    <?php
}


// CHECKOUT ON SUBMIT - EXTRA VALIDATIONS
add_action('woocommerce_checkout_process', 'checkout_validate_custom_field');
function checkout_validate_custom_field()
{
    validation_custom_fields($_POST);
}

// SAVE ACCOUNT ADDRESS - EXTRA VALIDATIONS
add_action('woocommerce_after_save_address_validation', 'account_address_validate_custom_field', 10, 4);
function account_address_validate_custom_field($user_id, $load_address, $address)
{
    validation_custom_fields($_POST);
}

function validation_custom_fields($form_data)
{
//    print_r($form_data);
    $billing_professional = '';
    if (isset($form_data['billing_professional'])) $billing_professional = $form_data['billing_professional'];

    if ( $billing_professional === 'no') {
        if (isset($form_data['billing_fiscal_code'])) {
            if (empty($form_data['billing_fiscal_code'])) {
                wc_add_notice(__("<strong>Codice Fiscale di fatturazione</strong> è un campo obbligatorio.", 'airbag-professional'), "error");
            }
        }
    }

    if ( $billing_professional === 'yes') {
        if (isset($form_data['billing_company'])) {
            if (empty($form_data['billing_company'])) {
                wc_add_notice(__("<strong>Nome della società</strong> è un campo obbligatorio.", 'airbag-professional'), "error");
            }
        }
        if (isset($form_data['billing_vat_number'])) {
            if (empty($form_data['billing_vat_number'])) {
                wc_add_notice(__("<strong>Numero Partita Iva</strong> è un campo obbligatorio.", 'airbag-professional'), "error");
            }
        }
    }
}


add_action('woocommerce_shipping_fields', 'custom_shipping_fields');
if (!function_exists('custom_shipping_fields')) {
    function custom_shipping_fields($fields)
    {
        //print_r($fields);

        unset($fields['shipping_company']);
        unset($fields['shipping_address_2']);

        $fields['shipping_country']['priority'] = '61';
        $fields['shipping_country']['class'] = array('form-row-first');
        $fields['shipping_country']['description'] = '<a href="/contatti" target="_blank">(<strong>' . __('Non trovi il tuo paese?', 'airbag-professional') . '</strong> ' . __('Clicca qui e chiedici un preventivo personalizzato', 'airbag-professional') . ')</a>';

        return $fields;
    }
}

add_filter('woocommerce_default_address_fields', 'custom_override_default_locale_fields', 1);
if (!function_exists('custom_override_default_locale_fields')) {
    function custom_override_default_locale_fields($fields)
    {

        $fields['state']['priority'] = '62';
        $fields['state']['class'] = array('form-row-last');

        $fields['city']['priority'] = '63';
        $fields['city']['class'] = array('form-row-first');

        $fields['postcode']['priority'] = '64';
        $fields['postcode']['class'] = array('form-row-last', 'address-field');

        return $fields;
    }
}

add_action('woocommerce_admin_order_data_after_billing_address', 'custom_checkout_field_display_admin_order_meta', 10, 1);
if (!function_exists('custom_checkout_field_display_admin_order_meta')) {
    function custom_checkout_field_display_admin_order_meta($order)
    {
        $html_return = '';

        $isProfessional = get_post_meta($order->get_id(), '_billing_professional', true);
        $isProfessional_text = ($isProfessional === 'yes') ? __('Sì', 'airbag-professional') : __('No', 'airbag-professional');
        $electronicReceipt = (get_post_meta($order->get_id(), '_billing_excluded_invoice', true)) ? __('Sì', 'airbag-professional') : __('No', 'airbag-professional');

        $html_return .= '<p><strong>' . __('Professionista', 'airbag-professional') . ':</strong> ' . $isProfessional_text . '</p>';
        if(get_post_meta($order->get_id(), '_billing_fiscal_code', true)) $html_return .= '<p><strong>' . __('Codice Fiscale', 'airbag-professional') . ':</strong> ' . get_post_meta($order->get_id(), '_billing_fiscal_code', true) . '</p>';

        if( $isProfessional === 'yes') {
            if(get_post_meta($order->get_id(), '_billing_company', true)) $html_return .= '<p><strong>' . __('Company name', 'woocommerce') . ':</strong> ' . get_post_meta($order->get_id(), '_billing_company', true) . '</p>';
            if(get_post_meta($order->get_id(), '_billing_vat_number', true)) $html_return .= '<p><strong>' . __('Numero Partita Iva', 'airbag-professional') . ':</strong> ' . get_post_meta($order->get_id(), '_billing_vat_number', true) . '</p>';
            if(get_post_meta($order->get_id(), '_billing_pec_email', true)) $html_return .= '<p><strong>' . __('PEC email', 'airbag-professional') . ':</strong> ' . get_post_meta($order->get_id(), '_billing_pec_email', true) . '</p>';
            if(get_post_meta($order->get_id(), '_billing_sdi_code', true)) $html_return .= '<p><strong>' . __('Codice Dest. SDI', 'airbag-professional') . ':</strong> ' . get_post_meta($order->get_id(), '_billing_sdi_code', true) . '</p>';
            $html_return .= '<p><strong>' . __('Fatturazione Elettronica', 'airbag-professional') . ':</strong> ' . $electronicReceipt . '</p>';
        }

        echo $html_return;
    }
}

//add_filter('woocommerce_email_order_meta_keys', 'custom_order_meta_keys_email');
if (!function_exists('custom_order_meta_keys_email')) {
    function custom_order_meta_keys_email($keys)
    {
        $keys[__('Professionista', 'airbag-professional')] = '_billing_professional';
        $keys[__('Codice Fiscale', 'airbag-professional')] = '_billing_fiscal_code';
        $keys[__('Numero Partita Iva', 'airbag-professional')] = '_billing_vat_number';
        $keys[__('PEC email', 'airbag-professional')] = '_billing_pec_email';
        $keys[__('Codice Dest. SDI', 'airbag-professional')] = '_billing_sdi_code';
        $keys[__('Fatturazione Elettronica', 'airbag-professional')] = '_billing_excluded_invoice';
        return $keys;
    }
}

add_action('woocommerce_email_order_meta', 'custom_order_meta_keys_email_two', 10, 3);
if (!function_exists('custom_order_meta_keys_email_two')) {
    function custom_order_meta_keys_email_two($order, $sent_to_admin, $plain_text)
    {
        $fields = apply_filters('woocommerce_email_order_meta_fields', array(), $sent_to_admin, $order);

        $_fields[__('Professionista', 'airbag-professional')] = '_billing_professional';
        $_fields[__('Codice Fiscale', 'airbag-professional')] = '_billing_fiscal_code';

        $isProfessional = get_post_meta($order->get_id(), '_billing_professional', true);

        if( $isProfessional === 'yes') {
            $_fields[__('Company name', 'woocommerce')] = '_billing_company';
            $_fields[__('Numero Partita Iva', 'airbag-professional')] = '_billing_vat_number';
            $_fields[__('PEC email', 'airbag-professional')] = '_billing_pec_email';
            $_fields[__('Codice Dest. SDI', 'airbag-professional')] = '_billing_sdi_code';
            $_fields[__('Fatturazione Elettronica', 'airbag-professional')] = '_billing_excluded_invoice';
        }

        if ($_fields) {
            foreach ($_fields as $key => $field) {
                if (is_numeric($key)) {
                    $key = $field;
                }

                $fields[$key] = array(
                    'label' => wptexturize($key),
                    'value' => wptexturize(get_post_meta($order->get_id(), $field, true)),
                );
            }
        }

        $html_return = '';
        if ($fields) {

            $html_return .= '<div style="margin-bottom: 40px;">';
            $html_return .= '<table class="order-custom-table" width="100%" class="td" cellspacing="0" cellpadding="6" border="1">';

            $html_return .= '<thead>';
            $html_return .= '<tr>';
            $html_return .= '<th class="td" scope="col" colspan="2" align="center">' . __('Campi di fatturazione extra', 'airbag-professional') . '</th>';
            $html_return .= '</tr>';
            $html_return .= '</thead>';

            $html_return .= '<tbody>';

            if ($plain_text) {

                foreach ($fields as $field) {
                    if (isset($field['label']) && isset($field['value']) && $field['value']) {

                        $value = $field['value'];
                        if ($value == '1' || $value == 'yes') $value = __('Sì', 'airbag-professional');
                        if ($value == '0' || $value == 'no') $value = __('No', 'airbag-professional');

                        echo $field['label'] . ': ' . $value . "\n"; // WPCS: XSS ok.
                    }
                }

            } else {

                foreach ($fields as $field) {
                    if (isset($field['label']) && isset($field['value']) && $field['value']) {

                        $value = $field['value'];
                        if ($value == '1' || $value == 'yes') $value = __('Sì', 'airbag-professional');
                        if ($value == '0' || $value == 'no') $value = __('No', 'airbag-professional');

                        $html_return .= '<tr>';
                        $html_return .= '<th class="td" scope="row"><strong>' . $field['label'] . ':</strong></th>';
                        $html_return .= '<td class="td">' . $value . '</td>';
                        $html_return .= '</tr>';

                    }
                }
            }

            $html_return .= '</tbody>';

            $html_return .= '</table>';
            $html_return .= '</div>';

        }

        echo $html_return;

    }
}



// MAILCHIMP

if( has_filter('mailchimp_sync_user_mergetags') ) {
    add_filter('mailchimp_sync_user_mergetags', 'custom_user_merge_tags', 100, 2);
    function custom_user_merge_tags($merge_vars, $user)
    {

        if (get_user_meta($user->ID, 'billing_phone', true)) {
            $merge_vars['PHONE'] = get_user_meta($user->ID, 'billing_phone', true);
        }

        if (get_user_meta($user->ID, 'billing_company', true)) {
            $merge_vars['COMPANY'] = get_user_meta($user->ID, 'billing_company', true);
        }

        // return the merge tag array to be submitted.
        return $merge_vars;
    }
}
