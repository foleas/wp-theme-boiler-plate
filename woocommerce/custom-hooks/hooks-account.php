<?php

//===================================================
// ACCOUNT
//===================================================

// REMOVES NOTICES ( ESTAN DOBLES
//remove_action( 'woocommerce_before_customer_login_form', 'woocommerce_output_all_notices', 10 );

// REMOVE PRIVACY TEXT REGISTER FORM
remove_action( 'woocommerce_register_form', 'wc_registration_privacy_policy_text', 20 );

// WC ENDPOINTS
// REMOVER LINKS DE MY ACCOUNT PAGE
add_filter('woocommerce_account_menu_items', 'misha_remove_my_account_links', 10);
function misha_remove_my_account_links($menu_links)
{

    unset($menu_links['dashboard']); // Remove Dashboard
    //unset( $menu_links['orders'] ); // Remove Orders
    unset($menu_links['downloads']); // Disable Downloads
    // unset( $menu_links['edit-address'] ); // Addresses
    //unset( $menu_links['edit-account'] ); // Remove Account details tab
    unset($menu_links['payment-methods']); // Remove Payment Methods
//    unset($menu_links['customer-logout']); // Remove Logout link

    return $menu_links;
}

// CHANGE ORDER LINKS DE MY ACCOUNT PAGE
add_filter('woocommerce_account_menu_items', 'wpb_woo_my_account_order', 20);
function wpb_woo_my_account_order()
{
    $myorder = array(
        'informazioni-account' => __('Informazioni account', 'airbag-professional'),
        'indirizzi' => __('Indirizzi', 'airbag-professional'),
        'orders' => __('I miei ordini', 'airbag-professional'),
        'wishlist' => __('La mia Wishlist', 'airbag-professional'),
        'customer-logout' => __('Esci', 'airbag-professional'),
    );

    return $myorder;
}

// CHANGE TITLE LINKS DE MY ACCOUNT PAGE

add_filter('the_title', 'wpb_woo_endpoint_title', 10, 2);
function wpb_woo_endpoint_title($title, $id)
{

    if (is_wc_endpoint_url('informazioni-account') && in_the_loop()) { // add your endpoint urls
        $title = __('Informazioni account', 'airbag-professional');
    } elseif (is_wc_endpoint_url('indirizzi') && in_the_loop()) {
        $title = __('Indirizzi', 'airbag-professional');
    } elseif (is_wc_endpoint_url('orders') && in_the_loop()) {
        $title = __('Orders', 'woocommerce');
    }
    return $title;
}

//Redirects to the Orders List instead of Woocommerce My Account Dashboard
function wpmu_woocommerce_account_redirect()
{
    $current_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $dashboard_url = get_permalink(get_option('woocommerce_myaccount_page_id'));

    if (is_user_logged_in() && $dashboard_url == $current_url) {
        // $url = get_home_url() . '/mi-cuenta/pedidos';
        $url = wc_get_account_endpoint_url('informazioni-account');
        wp_redirect($url);
        exit;
    }
}

add_action('template_redirect', 'wpmu_woocommerce_account_redirect');



// ORDERS LIST

add_filter( 'woocommerce_account_orders_columns', 'new_orders_columns' );
function new_orders_columns( $columns = array() ) {

    // Hide the columns
    if( isset($columns['order-total']) ) {
        // Unsets the columns which you want to hide
        unset( $columns['order-number'] );
        unset( $columns['order-date'] );
        unset( $columns['order-status'] );
        unset( $columns['order-total'] );
        unset( $columns['order-actions'] );
    }

    // Add new columns
    $columns['order-date'] = __( 'Date', 'woocommerce' );
    $columns['order-number'] = __( 'Id Ordine', 'airbag-professional' );
    $columns['order-total'] = __( 'Valore', 'airbag-professional' );
    $columns['order-status'] = __( 'Status', 'woocommerce' );

    return $columns;
}


remove_action('woocommerce_account_orders_endpoint', 'woocommerce_account_orders');
add_action('woocommerce_account_orders_endpoint', 'woocommerce_account_orders_CUSTOM');

if (!function_exists('woocommerce_account_orders_CUSTOM')) {

    function woocommerce_account_orders_CUSTOM($current_page)
    {
        $current_page = empty($current_page) ? 1 : absint($current_page);
        $customer_orders = wc_get_orders(
            apply_filters(
                'woocommerce_my_account_my_orders_query',
                array(
                    'customer' => get_current_user_id(),
                    'page' => $current_page,
                    'paginate' => true,
                )
            )
        );

        wc_get_template(
            'myaccount/orders-custom.php',
            array(
                'current_page' => absint($current_page),
                'customer_orders' => $customer_orders,
                'has_orders' => 0 < $customer_orders->total,
            )
        );
    }
}


// ORDER DETAIL
remove_action('woocommerce_view_order', 'woocommerce_order_details_table', 10);
remove_action('woocommerce_thankyou', 'woocommerce_order_details_table', 10);

add_action('woocommerce_thankyou', 'woocommerce_order_details_table_CUSTOM', 10);
add_action('woocommerce_view_order', 'woocommerce_order_details_table_CUSTOM', 10);
if (!function_exists('woocommerce_order_details_table_CUSTOM')) {

    function woocommerce_order_details_table_CUSTOM($order_id)
    {
        if (!$order_id) {
            return;
        }

        wc_get_template(
            'order/order-details-custom.php',
            array(
                'order_id' => $order_id,
            )
        );
    }
}
