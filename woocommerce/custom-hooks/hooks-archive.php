<?php

//===================================================
// PRODUCT LIST
//===================================================

// HEADER
remove_action('woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10);
remove_action('woocommerce_archive_description', 'woocommerce_product_archive_description', 10);

add_filter('woocommerce_show_page_title', '__return_false'); // SACAR TITLE EN ARCHIVE

// AGREGAR HEADER IMAGE
add_action('woocommerce_archive_description', 'woocommerce_archive_header_image', 10);
if (!function_exists('woocommerce_archive_header_image')) {
    function woocommerce_archive_header_image()
    {
        wc_get_template_part('includes/archive/archive', 'wc_header');
    }
}

// AGREGAR DESCRIPTION
add_action('woocommerce_before_shop_loop', 'woocommerce_archive_long_description', 10);
if (!function_exists('woocommerce_archive_long_description')) {
    function woocommerce_archive_long_description()
    {
        wc_get_template_part('includes/archive/archive', 'wc_description');
    }
}

// REMOVE BREADCRUMBS
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);

// REMOVE ORDERING
remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);

// PAGINATION
remove_action('woocommerce_after_shop_loop', 'woocommerce_pagination', 10);
add_action('woocommerce_before_shop_loop', 'woocommerce_pagination_CUSTOM', 10);
add_action('woocommerce_after_shop_loop', 'woocommerce_pagination_CUSTOM', 10);
if (!function_exists('woocommerce_pagination_CUSTOM')) {

    function woocommerce_pagination_CUSTOM()
    {
        if (!wc_get_loop_prop('is_paginated') || !woocommerce_products_will_display()) {
            return;
        }

        $args = array(
            'total' => wc_get_loop_prop('total_pages'),
            'current' => wc_get_loop_prop('current_page'),
            'base' => esc_url_raw(add_query_arg('product-page', '%#%', false)),
            'format' => '?product-page=%#%',
        );

        if (!wc_get_loop_prop('is_shortcode')) {
            $args['format'] = '';
            $args['base'] = esc_url_raw(str_replace(999999999, '%#%', remove_query_arg('add-to-cart', get_pagenum_link(999999999, false))));
        }

        wc_get_template('loop/pagination-custom.php', $args);
    }
}

add_action('pre_get_posts', 'change_limit_for_products');
if (!function_exists('change_limit_for_products')) {
    function change_limit_for_products($query)
    {
        $per_page = filter_input(INPUT_GET, 'limit', FILTER_SANITIZE_NUMBER_INT);

        if ($query->is_main_query()) {
            if (is_post_type_archive('product') || is_product_category()) {
                $query->set('posts_per_page', $per_page);
            }
        }
    }
}

// REMOVER WRAPPING LINK - AND ADD CUSTOM WRAP
remove_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10);
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5);

add_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open_CUSTOM', 10);
if (!function_exists('woocommerce_template_loop_product_link_open_CUSTOM')) {
    function woocommerce_template_loop_product_link_open_CUSTOM()
    {
        echo '<div class="woocommerce-LoopProduct-link woocommerce-loop-product__link">';
    }
}
add_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close_CUSTOM', 10);
if (!function_exists('woocommerce_template_loop_product_link_close_CUSTOM')) {
    function woocommerce_template_loop_product_link_close_CUSTOM()
    {
        echo '</div>';
    }
}

// REMOVER PRICE
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);

// REMOVER ADD TO CART
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);

// REMOVER ON SALE
remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10);

// ADD CUSTOM IMAGE
remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail_CUSTOM', 10);
if (!function_exists('woocommerce_template_loop_product_thumbnail_CUSTOM')) {

    function woocommerce_template_loop_product_thumbnail_CUSTOM()
    {

        global $product;
        $size = 'medium';
        $echo_html = '';

        $image_size = apply_filters('single_product_archive_thumbnail_size', $size);
        $link = apply_filters('woocommerce_loop_product_link', get_the_permalink(), $product);

//        $gallery_img_ids = $product->get_gallery_image_ids();

        $echo_html .= '<a href="' . esc_url($link) . '" class="product-img-wrapper">';
        $echo_html .= '<figure class="product-img-wrapper">';

        if ($product->get_image($image_size)) {
            $echo_html .= '<div class="product-img">';
            $echo_html .= $product->get_image($image_size);
            $echo_html .= '</div>';
        }

        $echo_html .= '</figure>';
        $echo_html .= '</a>';

        echo $echo_html;

    }
}

// ADD CUSTOM TITLE
remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10);
add_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title_CUSTOM', 10);
if (!function_exists('woocommerce_template_loop_product_title_CUSTOM')) {

    function woocommerce_template_loop_product_title_CUSTOM()
    {
        global $product;
        $echo_html = '';

        $product_id = $product->get_id();

        $link = apply_filters('woocommerce_loop_product_link', get_the_permalink(), $product);

        $tags_terms = wp_get_post_terms($product_id, 'product_tag');
//        print_r($tags_terms);
        if ($tags_terms) {
            foreach ($tags_terms as $term) {
                if ($term->slug == 'new') {
                    $echo_html .= '<span class="tag-link">';
                    $echo_html .= $term->name;
                    $echo_html .= '</span>';
                }
            }
        } else {
            $echo_html .= '<div class="separator">&nbsp;</div>';
        }

        $echo_html .= '<a href="' . esc_url($link) . '">';
        if (is_shop()) {
            $echo_html .= '<h2 class="' . esc_attr(apply_filters('woocommerce_product_loop_title_classes', 'woocommerce-loop-product__title')) . '">';
            $echo_html .= get_the_title();
            $echo_html .= '</h2>';
        } else {
            $echo_html .= '<h3 class="' . esc_attr(apply_filters('woocommerce_product_loop_title_classes', 'woocommerce-loop-product__title')) . '">';
            $echo_html .= get_the_title();
            $echo_html .= '</h3>';
        }
        $echo_html .= '</a>';

//        $echo_html .= $product->get_price_html();

        $echo_html .= '<div class="price">';

        if ($product->is_type('variable')) {
            if ($product->is_on_sale()) {
                $echo_html .= wc_format_sale_price($product->get_variation_regular_price() , $product->get_variation_sale_price()) . $product->get_price_suffix();
            } else {
                $echo_html .= wc_price($product->get_variation_regular_price()) . $product->get_price_suffix();
            }
        }

        if ($product->is_type('simple')) {
            if ($product->is_on_sale()) {
                $echo_html .= wc_format_sale_price($product->get_regular_price() , $product->get_sale_price()) . $product->get_price_suffix();
            } else {
                $echo_html .= wc_price($product->get_regular_price()) . $product->get_price_suffix();
            }
        }

        $tax_status = $product->get_tax_status();
        $echo_html .= '<span class="tax '.$tax_status.'">';
        if( $tax_status === 'taxable'){
            $echo_html .= '(' . __('IVA esclusa', 'airbag-professional') . ')';
        }else{
            $echo_html .= '(' . __('IVA inclusa', 'airbag-professional') . ')';
        }
        $echo_html .= '</span>';

        $echo_html .= '</div>';

        if (is_account_page()) {
//            $echo_html .= woocommerce_template_loop_add_to_cart();
            //do_shortcode('[add_to_cart_url id="'.$product->get_id().'"]')
//            $echo_html .= '<button onclick="location.href=\'http://www.example.com\'" type="button">www.example.com</button>';


            /*
            $site_id = get_current_blog_id();
            $group_id = get_current_user_id();
            $favoritecount = get_favorites_count($product_id, $site_id , false);

            $echo_html .= '<div class="link-with-icon simplefavorite-button delete-from-wishlist active" data-postid="'.$product_id.'" data-siteid="'.$site_id.'"  data-groupid="'.$group_id.'" data-favoritecount="'.$favoritecount.'">';
            $echo_html .= '<span class="icon-custom">' . file_get_contents(get_template_directory() . '/assets/images/icon-trash.svg') . '</span>';
            $echo_html .= '<span>' . __('Elimina', 'airbag-professional') . '</span></div>';
            */

        }

        echo $echo_html;
    }
}

// ADD FAVORITE BUTTON
add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_product_favorite_button', 15);
if (!function_exists('woocommerce_template_loop_product_favorite_button')) {
    function woocommerce_template_loop_product_favorite_button()
    {

        wc_get_template('single-product/fav-btn.php');
    }
}

