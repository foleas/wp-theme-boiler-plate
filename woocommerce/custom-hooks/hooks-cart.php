<?php


//===================================================
// CART
//===================================================

// CART PAGE - custom html
remove_action('woocommerce_cart_collaterals', 'woocommerce_cart_totals', 10);
add_action('woocommerce_cart_collaterals', 'woocommerce_cart_totals_CUSTOM', 10);
if (!function_exists('woocommerce_cart_totals_CUSTOM')) {
    function woocommerce_cart_totals_CUSTOM()
    {
        wc_get_template('cart/cart-totals-custom.php');
    }
}

if (!function_exists('woocommerce_mini_cart_CUSTOM')) {
    function woocommerce_mini_cart_CUSTOM($args = array())
    {

        $defaults = array(
            'list_class' => '',
        );

        $args = wp_parse_args($args, $defaults);

        wc_get_template('cart/mini-cart-custom.php', $args);
    }
}

// MINI CART - update side cart y numero mini cart
add_filter('woocommerce_add_to_cart_fragments', 'update_mini_cart', 10, 1);
if (!function_exists('update_mini_cart')) {
    function update_mini_cart($fragments)
    {
        ob_start();
        wc_get_template('cart/mini-cart-custom.php');
        $fragments['div.widget_shopping_cart_content'] = ob_get_clean();
    }
}

remove_action('woocommerce_widget_shopping_cart_total', 'woocommerce_widget_shopping_cart_subtotal', 10);
add_action('woocommerce_widget_shopping_cart_total', 'woocommerce_widget_shopping_cart_subtotal_CUSTOM', 10);
if (!function_exists('woocommerce_widget_shopping_cart_subtotal_CUSTOM')) {

    function woocommerce_widget_shopping_cart_subtotal_CUSTOM()
    {
        $html_return = '';
        $html_return .= '<span class="total-col"><strong>' . esc_html__('Subtotal:', 'woocommerce') . '</strong></span>';
        $html_return .= '<span class="total-col">';
        $html_return .= WC()->cart->get_cart_subtotal();

        $cart_tax_status = get_cart_status(WC()->cart->get_cart());

        $html_return .= '<span class="tax '.$cart_tax_status.'">';
        if( $cart_tax_status === 'taxable'){
            $html_return .= '(' . __('IVA esclusa', 'airbag-professional') . ')';
        }else{
            $html_return .= '(' . __('IVA inclusa', 'airbag-professional') . ')';
        }
        $html_return .= '</span>';

        echo $html_return;
    }
}

remove_action('woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_button_view_cart', 10);
remove_action('woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_proceed_to_checkout', 20);
add_action('woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_button_view_cart_CUSTOM', 10);
add_action('woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_proceed_to_checkout_CUSTOM', 20);

if (!function_exists('woocommerce_widget_shopping_cart_button_view_cart_CUSTOM')) {
    function woocommerce_widget_shopping_cart_button_view_cart_CUSTOM()
    {
        echo '<a href="' . esc_url(wc_get_cart_url()) . '" class="button alt wc-forward">' . esc_html__('Vai al Carrello', 'airbag-professional') . '</a>';
    }
}

if (!function_exists('woocommerce_widget_shopping_cart_proceed_to_checkout_CUSTOM')) {
    function woocommerce_widget_shopping_cart_proceed_to_checkout_CUSTOM()
    {
        echo '<a href="' . esc_url(wc_get_checkout_url()) . '" class="button alt checkout wc-forward">' . esc_html__('Vai alla Cassa', 'airbag-professional') . '</a>';
    }
}