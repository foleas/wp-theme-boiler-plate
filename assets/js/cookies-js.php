<?php
$cookies_page_url = get_permalink( get_page_by_path( 'cookies' ) );
?>

<script>
	(function ($) {

		$(window).on('load', function () {

			// COOKIES
			var options = {
				title: '',
				message: "<?php _e( 'Este sitio utiliza cookies para mejorar su experiencia de navegación. Al pulsar el botón \"Aceptar\", usted consiente en su uso.', 'text-domain' ) ?>",
				delay: 600,
				expires: 30,
				link: '<?php echo $cookies_page_url; ?>',
				// onAccept: function(){
				//     var myPreferences = $.fn.ihavecookies.cookie();
				//     console.log('Yay! The following preferences were saved...');
				//     console.log(myPreferences);
				// },
				moreInfoLabel: '<?php _e( 'Leer Todo', 'text-domain' ) ?>',
				acceptBtnLabel: '<?php _e( 'Aceptar', 'text-domain' ) ?>',
				advancedBtnLabel: '<?php _e( 'Personalizar las Cookies', 'text-domain' ) ?>',
				uncheckBoxes: false,
				cookieTypesTitle: '<?php _e( 'Seleccione las cookies para aceptar', 'text-domain' ) ?>',
				fixedCookieTypeLabel: '<?php _e( 'Necesario', 'text-domain' ) ?>',
				fixedCookieTypeDesc: '<?php _e( 'Estas son cookies que son esenciales para el correcto funcionamiento del sitio.', 'text-domain' ) ?>.',
				cookieTypes: [
					{
						type: '<?php _e( 'Preferencias del Sitio', 'text-domain' ) ?>',
						value: 'preferences',
						description: '<?php _e( 'Estas son cookies que están relacionadas con las preferencias de su sitio, por ejemplo recordando su nombre de usuario, colores del sitio, etc.', 'text-domain' ) ?>'
					},
					{
						type: '<?php _e( 'Analytics', 'text-domain' ) ?>',
						value: 'analytics',
						description: '<?php _e( 'Cookies relacionadas con visitas al sitio, tipos de navegador, etc.', 'text-domain' ) ?>'
					},
					{
						type: '<?php _e( 'Marketing', 'text-domain' ) ?>',
						value: 'marketing',
						description: '<?php _e( 'Cookies relacionadas con marketing, boletines, redes sociales, etc.', 'text-domain' ) ?>'
					}
				],
			}

			$('body').ihavecookies(options);

			if ($.fn.ihavecookies.preference('marketing') === true) {
				// console.log('This should run because marketing is accepted.');
			}

			$('#ihavecookiesBtn').on('click', function () {
				// $('body').ihavecookies(options, 'reinit');
			});
			// COOKIES

		});

	})(jQuery);
</script>
