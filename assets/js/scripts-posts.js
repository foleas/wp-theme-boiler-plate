(function ($) {
  $(document).on("ready", function () {
    // LOAD MORE
    $(".loadmore-btn").on("click", function () {
      const item = $(this);
      const btnWrapper = item.closest(".custom-btn-wrapper");
      const postsAjaxWrapper = btnWrapper.siblings(".posts-ajax-wrapper");

      const ajaxData = {
        action: $("input[name=action]", btnWrapper).val(),
        nonce: $("input[name=nonce]", btnWrapper).val(),
        post_type: $("input[name=post_type]", btnWrapper).val(),
        posts_layout: $("input[name=posts_layout]", btnWrapper).val(),
        col_class: $("input[name=col_class]", btnWrapper).val(),
        query_vars: all_scripts_params.query_vars,
        paged: all_scripts_params.paged,
      };

      jQuery.ajax({
        url: all_scripts_params.ajax_url,
        data: ajaxData,
        type: "POST",
        beforeSend: function () {
          btnWrapper.addClass("disabled");
          btnWrapper.addClass("loading");
        },
        complete: function () {
          btnWrapper.removeClass("disabled");
          btnWrapper.removeClass("loading");
        },
        success: function (response) {
          // console.log("response", response);
          // console.log("html_return", response.html_return);

          if (
            parseInt(all_scripts_params.paged) ===
            parseInt(all_scripts_params.max_page)
          ) {
            btnWrapper.addClass("hide");
          } else {
            btnWrapper.removeClass("hide");
          }

          if (response.html_return) {
            all_scripts_params.paged++;

            const postList = $(".row", postsAjaxWrapper);
            postList.append(response.html_return);
          }
        },
      });
    });
  });
})(jQuery);
