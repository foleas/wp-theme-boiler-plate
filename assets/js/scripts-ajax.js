(function ($) {
  $(document).on("ready", function () {
    // RESEND VALIDATION MAIL
    $(document).on("click", ".resend-validation-mail", function (e) {
      e.preventDefault();

      const user_id = $(this).attr("data-id");

      $.ajax({
        url: all_scripts_params.ajax_url,
        data: {
          action: "send_custom_validation_email_ajax",
          user_id: user_id,
        },
        type: "POST",
        beforeSend: function (response) {},
        complete: function (response) {},
        success: function (response) {},
      });
    });

    $(document).on(
      "favorites-updated-single",
      function (event, favorites, post_id, site_id, status) {
        // console.log("event" , 'favorites-updated-single');
        // console.log("favorites" , favorites);
        // console.log("post_id" , post_id);
        // console.log("status" , status);
      }
    );
  });
})(jQuery);
