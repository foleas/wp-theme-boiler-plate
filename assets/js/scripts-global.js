// FIX PARA 100VH MOBILE
// css
// height: 100vh;
// height: calc(var(--vh, 1vh) * 100);
let vh = window.innerHeight * 0.01;
document.documentElement.style.setProperty("--vh", `${vh}px`);
window.addEventListener("resize", function () {
  let vhResize = window.innerHeight * 0.01;
  document.documentElement.style.setProperty("--vh", `${vhResize}px`);
});
let lastScrollTop = 0;

(function ($) {
  $(document).on("ready", function () {
    const scrolled = $(window).scrollTop();
    // header_change(scrolled);
    // detect_scroll_direction(scrolled, lastScrollTop);

    // MODAL
    $(".modal-close, .modal-wrapper .overlay").on("click", () => {
      $(".modal-wrapper.show").fadeOut(500, "easeInOutCirc");
      $("body").removeClass("no-scroll");
    });

    $(".modal-open-search").on("click", (e) => {
      e.preventDefault();
      $("body").addClass("no-scroll");
      $(".modal-wrapper-search").fadeIn(500, "easeInOutCirc", function () {
        $(this).addClass("show");
      });
    });

    $(".modal-open-menu").on("click", (e) => {
      e.preventDefault();
      $("body").addClass("no-scroll");
      $(".modal-wrapper-menu").fadeIn(500, "easeInOutCirc", function () {
        $(this).addClass("show");
      });
    });
  });

  $(window).on("scroll", function () {
    const scrolled = $(window).scrollTop();
    // header_change(scrolled);
    // detect_scroll_direction(scrolled, lastScrollTop);
  });

  // HEADER CHANGE
  function header_change(scrolled_) {
    const header_alt = $("header.header-theme").height();
    if (scrolled_ >= header_alt * 2) {
      $("body").addClass("header-fixed");
      $("main.theme-main").css("margin-top", header_alt);
    } else {
      $("body").removeClass("header-fixed");
      $("main.theme-main").css("margin-top", 0);
    }
  }

  // SCROLL DIRECTION
  function detect_scroll_direction(scrolled_, lastScrollTop_) {
    const st = window.scrollY;
    const htmlElement = $("html");
    if (st === 0) {
      htmlElement.attr("data-scrolldir", "down");
    } else if (st < lastScrollTop_) {
      htmlElement.attr("data-scrolldir", "up");
    } else {
      htmlElement.attr("data-scrolldir", "down");
    }
    lastScrollTop = st;
  }

  function reset_form(form) {
    $("input[type=text]", form).val("");
    $("textarea", form).val("");
    $("input[type=email]", form).val("");
    $("input[type=number]", form).val("");
    // $("input[type=radio]" , form).prop('checked',false);
    $("input[type=checkbox]", form)
      .not("input[type=checkbox].no-reset")
      .prop("checked", false);
    $("select", form).prop("selectedIndex", 0);
    // $("select.select2" , form).select2("val", "");
    $("select.select2", form).val("").trigger("change");

    // $(".select-out .select-items .select-item:nth-child(1)" , form).trigger('click');
  }
})(jQuery);
