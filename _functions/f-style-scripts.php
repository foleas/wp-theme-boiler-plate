<?php
add_action('wp_enqueue_scripts', 'theme_enqueue_styles', 999);
function theme_enqueue_styles()
{

	// JQUERY EASING
	wp_enqueue_script('jquery-easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js', array('jquery'), '', true);

	// BOOTSTRAP
//	wp_enqueue_style('bootstrap-grid', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap-grid.min.css');
//	wp_enqueue_style('bootstrap-reboot', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap-reboot.min.css');

	// SWIPER
//	wp_enqueue_script('swiper', 'https://unpkg.com/swiper@8/swiper-bundle.min.js', '', '', true);
//	wp_enqueue_style('swiper', 'https://unpkg.com/swiper@8/swiper-bundle.min.css');

	// COOKIES
//	wp_enqueue_script('cookies', get_stylesheet_directory_uri() . '/assets/plugins/jquery.ihavecookies.min.js', array('jquery'), '', true);

	// FONTS
//	wp_dequeue_style( 'font-awesome' );
//	wp_enqueue_style('font-awesome-custom', 'https://use.fontawesome.com/releases/v5.7.0/css/all.css');
//	wp_enqueue_style('roboto-condensed', 'https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@300;400;700&display=swap'); //font-family: 'Roboto Condensed', sans-serif;
//	wp_enqueue_style('roboto', 'https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,700;1,300;1,400&display=swap'); //font-family: 'Roboto', sans-serif;

	global $wp_query;

	wp_register_script('all-scripts', get_template_directory_uri() . '/assets/js/all-scripts.php', array('jquery'), '', true);
	wp_localize_script('all-scripts', 'all_scripts_params', array(
//        'template_directory' => get_bloginfo('template_directory'),
//        'ajax_url' => admin_url('admin-ajax.php'),
//        'query_vars' => json_encode($wp_query->query_vars),
//        'paged' => $wp_query->query_vars['paged'] ? $wp_query->query_vars['paged'] : 1,
//        'max_page' => $wp_query->max_num_pages,
	));
	wp_enqueue_script('all-scripts');

}
