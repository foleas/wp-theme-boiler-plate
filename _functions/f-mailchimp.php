<?php
/*
 	// MAILCHIMP
	$mailchimpStatus = '';
	if ( $_POST['mailchimp'] ) {
		$email = $_POST['email'];
		$mc_api_status = 'subscribed'; // "subscribed" or "unsubscribed" or "cleaned" or "pending"
		$list_id = ''; // where to get it read above
		$api_key = ''; // where to get it read above
		$merge_fields = array('FNAME' => $_POST['nombre'],'PHONE' => $_POST['telefono'],'COMPANY' => $_POST['empresa']);
        $tags = ['tag'];

		$newUserReturn = rudr_mailchimp_subscriber_status($email, $mc_api_status, $list_id, $api_key, $merge_fields, $tags );
		$mailchimpStatus = $newUserReturn;
	}
 */


//https://rudrastyh.com/mailchimp-api/subscription.html
function rudr_mailchimp_subscriber_status( $email, $status, $list_id, $api_key, $merge_fields = array('FNAME' => '','LNAME' => ''), $tags = [] ){
	$data = array(
		'apikey'        => $api_key,
		'email_address' => $email,
		'status'        => $status,
		'merge_fields'  => $merge_fields,
		"tags"          => $tags,
	);
	$mch_api = curl_init(); // initialize cURL connection

	curl_setopt($mch_api, CURLOPT_URL, 'https://' . substr($api_key,strpos($api_key,'-')+1) . '.api.mailchimp.com/3.0/lists/' . $list_id . '/members/' . md5(strtolower($data['email_address'])));
	curl_setopt($mch_api, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.base64_encode( 'user:'.$api_key )));
	curl_setopt($mch_api, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
	curl_setopt($mch_api, CURLOPT_RETURNTRANSFER, true); // return the API response
	curl_setopt($mch_api, CURLOPT_CUSTOMREQUEST, 'PUT'); // method PUT
	curl_setopt($mch_api, CURLOPT_TIMEOUT, 10);
	curl_setopt($mch_api, CURLOPT_POST, true);
	curl_setopt($mch_api, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($mch_api, CURLOPT_POSTFIELDS, json_encode($data) ); // send data in json

	$result = curl_exec($mch_api);
	return $result;
}
