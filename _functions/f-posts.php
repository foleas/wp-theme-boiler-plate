<?php
////===================================================
// LOAD MORE
////===================================================

add_action( 'wp_ajax_loadmore', 'loadmore_ajax_handler' );
add_action( 'wp_ajax_nopriv_loadmore', 'loadmore_ajax_handler' );

function loadmore_ajax_handler() {
	check_ajax_referer( 'loadmore_nonce', 'nonce' );

	$status      = 'success';
	$msg         = '';
	$html_return = '';

	$args                = json_decode( stripslashes( $_POST['query_vars'] ), true );
	$next_page           = $_POST['paged'] + 1; // we need next page to be loaded
	$args['paged']       = $next_page;
	$args['post_type']   = $_POST['post_type'];
	$args['post_status'] = 'publish';
	query_posts( $args );

	if ( have_posts() ) :
		ob_start();

		while ( have_posts() ): the_post();
			$postItemArgs = array();
			echo '<div class="posts-grid-item">';
			get_template_part( 'includes/post/post', 'item', $postItemArgs );
			echo '</div>';
		endwhile;

		$html_return = ob_get_contents();
		ob_end_clean();
	endif;


	response( array(
		'status'      => $status,
		'msg'         => $msg,
		'html_return' => $html_return,
		'new_paged'   => $next_page,
	) );
	die;
}