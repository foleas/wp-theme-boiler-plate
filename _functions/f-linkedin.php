<?php
// GET ACCESS TOKEN
function get_linkedin_access_token($client_id, $client_secret, $linkedin_code, $linkedin_redirect_url)
{
    $curl_url = 'https://www.linkedin.com/oauth/v2/accessToken?grant_type=authorization_code&client_id=' . $client_id . '&client_secret=' . $client_secret . '&code=' . $linkedin_code . '&redirect_uri=' . $linkedin_redirect_url;
    $accessTokenResponseArray = json_decode(get_curl_response($curl_url),true);
//    print_r($accessTokenResponseArray);
    if(array_key_exists('error', $accessTokenResponseArray)){
        return false;
    }else{
        return $accessTokenResponseArray['access_token'];
    }
}

// GET USER DATA
function get_linkedin_user_data($access_token)
{
    $curl_url = 'https://api.linkedin.com/v2/me?oauth2_access_token='.$access_token.'&projection=(id,firstName,lastName,emailAddress,profilePicture(displayImage~digitalmediaAsset:playableStreams))';
    $responseArray = json_decode(get_curl_response($curl_url),true);
//    print_r($responseArray);
    if(array_key_exists('error', $responseArray)){
        return false;
    }else{
        return $responseArray;
    }
}

// GET USER EMAIL
function get_linkedin_user_email($access_token){

    $curl_url = 'https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))&oauth2_access_token='.$access_token;
    $responseArray = json_decode(get_curl_response($curl_url),true);
//    print_r($userResponseArray);
    if(array_key_exists('error', $responseArray)){
        return false;
    }else{
        return $responseArray;
    }
}

// GET CURL RESPONSE
function get_curl_response($curl_url){
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $curl_url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Cookie: lidc="b=VB23:s=V:r=V:a=V:p=V:g=3588:u=97:i=1623784581:t=1623786207:v=2:sig=AQEmejgiEyNpiaUEgmSd2EajZm7ga7yz"; bcookie="v=2&1cde4a99-6a75-4ca1-8337-1c20dd8c7f99"; lang=v=2&lang=en-us; lidc="b=VB23:s=V:r=V:a=V:p=V:g=3588:u=97:i=1623781279:t=1623786207:v=2:sig=AQFS1E1ANokZ6VZsNdGZcYGYwvNTNLeZ"'
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}