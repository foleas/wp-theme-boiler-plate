<?php
//===================================================
// CREATE CATEGORY
//===================================================
function add_gutenberg_block_categories($categories)
{
	$category_slugs = wp_list_pluck($categories, 'slug');
	return in_array('text-domain', $category_slugs, true) ? $categories : array_merge(
		$categories,
		[
			[
				'slug' => 'text-domain',
				'title' => __('Theme Blocks', 'text-domain'),
				'icon' => null,
			],
		]
	);
}

add_action('block_categories_all', 'add_gutenberg_block_categories', 10, 2);


// ADD BLOCKS
add_action('init', 'add_gutenberg_blocks');
function add_gutenberg_blocks()
{
	wp_register_script('custom-blocks-js', get_template_directory_uri() . '/gutenberg/build/index.js', array('wp-blocks', 'wp-block-editor'));
	wp_register_style('custom-blocks-css', get_template_directory_uri() . '/gutenberg/css/blocks.min.css', array());
	register_block_type('eaeblog/custom-blocks', array(
		'editor_script' => 'custom-blocks-js',
		'style' => 'custom-blocks-css',
	));
}