<?php

////===================================================
// CONTACT FORM - ACF
////===================================================

add_action('wp_ajax_content_form_ajax', 'content_form_ajax');
add_action('wp_ajax_nopriv_content_form_ajax', 'content_form_ajax');

function content_form_ajax()
{
    $status = 'success';
    $msg_return = __('Il tuo messaggio è stato inviato con successo. Grazie mille', 'text-domain');

    check_ajax_referer('content_form_ajax_nonce', 'nonce');

    // print_r($_POST);

    foreach ($_POST as $key => $value) {
        $clean_data[$key] = htmlspecialchars($value);
        $clean_data[$key] = nl2br($clean_data[$key]);
        $clean_data[$key] = urldecode($clean_data[$key]);
    }

    $data = [
        'secret' => '6LcyjuYZAAAAAA3BAwwyXXDhq_BeJHBrcz-Y1-wc',
        'response' => $_POST['g-recaptcha-response']
    ];

    $curl = curl_init();

    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));

    $response = curl_exec($curl);
    $response = json_decode($response, true);
//        print_r($response);
    if (!$response['success']) {
        //$error_codes = $response['error-codes'];
        $status = 'error';
        $msg_return = __('Recaptcha non è stato convalidato', 'text-domain');
    }else{
        $mailer = WC()->mailer();

//        $recipient = array('filiwebtest@gmail.com');

        $mail_to = array(get_field('email', 'options'));
//        print_r($mail_to);
        if( $clean_data['mailto'] != '') $mail_to = array($clean_data['mailto']);
        $recipient = $mail_to;

        $subject = 'Form -'.get_bloginfo('name');
        if( $clean_data['subject'] != '') $subject = $clean_data['subject'];

        $content = get_custom_email_html($order, $subject, $mailer, $clean_data, $clean_data['email_type']);
        $headers = "Content-Type: text/html\r\n";

        //send the email through wordpress
        $return_send = $mailer->send($recipient, $subject, $content, $headers);
        if (!$return_send) {
            $status = 'error';
            $msg_return = __('Non è stato possibile inviare il tuo messaggio. Per favore riprova più tardi.', 'text-domain');
        }
    }

    response(array(
        'status' => $status,
        'msg_return' => $msg_return
    ));

    die();
}

