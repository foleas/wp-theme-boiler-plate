<?php
//===================================================
//Registrar Taxonomias
//===================================================

//add_action('init', 'custom_tax');

function custom_tax()
{

    register_taxonomy(
        'categoria-eventos',
        array('eventos'),
        array(
            'label' => __('Categoría Eventos', 'text-domain'),
            'rewrite' => array('slug' => 'categoria-eventos'),
            'hierarchical' => true,
            'publicly_queryable' => false,
            'show_in_rest' => true,
        )
    );
}


//===================================================
//Register Postype
//===================================================

//add_theme_support( 'post-thumbnails' );

// EVENTOS
function add_eventos()
{
    $labels = array(
        'name' => __('Eventos', 'text-domain'),
        'singular_name' => __('Evento', 'text-domain'),
        'add_new' => __('Agregar Evento', 'text-domain'),
        'add_new_item' => __('Agregar Nuevo Evento', 'text-domain'),
        'edit_item' => __('Editar Evento', 'text-domain'),
        'new_item' => __('Nuevo Evento', 'text-domain'),
        'all_items' => __('Todos los Eventos', 'text-domain'),
        'view_item' => __('Ver Evento', 'text-domain'),
        'search_items' => __('Buscar Evento', 'text-domain'),
        'not_found' => __('No hubo Eventos encontrados', 'text-domain'),
        'not_found_in_trash' => __('No hubo Eventos encontrados en la papelera', 'text-domain'),
        'parent_item_colon' => '',
        'menu_name' => __('Eventos', 'text-domain'),
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('title' => __('Agenda' , 'text-domain') , 'slug' => 'agenda'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'show_in_rest' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-calendar-alt',
        'supports' => array('title' , 'editor' , 'thumbnail')
    );

    register_post_type('eventos', $args);
}

//add_action('init', 'add_eventos');