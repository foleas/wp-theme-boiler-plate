<?php
//===================================================
// ADD ACF BLOCKS STYLES
//===================================================

function enqueue_acf_blocks_styles() {
	wp_enqueue_style( 'blocks-acf-styles', get_template_directory_uri() . '/templates-blocks-acf/blocks-acf-styles.min.css' );
}

add_action( 'wp_enqueue_scripts', 'enqueue_acf_blocks_styles' );

function enqueue_acf_blocks_styles_in_admin() {
	$current_screen = get_current_screen();
	//Check if we're on a Gutenberg Page
	if ( method_exists( $current_screen, 'is_block_editor' ) && $current_screen->is_block_editor() ) {
		wp_enqueue_style( 'blocks-acf-admin-styles', get_template_directory_uri() . '/templates-blocks-acf/blocks-acf-admin-styles.min.css' );
	}
}

add_action( 'admin_enqueue_scripts', 'enqueue_acf_blocks_styles_in_admin' );

//===================================================
// ADD TYPE MODULE TO BLOCK SCRIPTS
//===================================================

add_filter( 'script_loader_tag', 'add_type_attribute', 10, 3 );
function add_type_attribute( $tag, $handle, $src ) {
//	if (str_contains($handle, '-block')) {
	if ( strpos( $handle, 'text-domain' ) !== false ) {
		if ( ! is_admin() ) {
			// change the script tag by adding type="module" and return it.
			$tag = '<script id="' . $handle . '-js" type="module" src="' . esc_url( $src ) . '"></script>';
		}
	}

	return $tag;
}


//===================================================
// CREATE CATEGORY
//===================================================
function add_gutenberg_block_categories( $categories ) {
	$category_slugs = wp_list_pluck( $categories, 'slug' );

	return in_array( 'text-domain', $category_slugs, true ) ? $categories : array_merge(
		$categories,
		[
			[
				'slug'  => 'text-domain',
				'title' => __( 'Theme Blocks', 'text-domain' ),
				'icon'  => null,
			],
		]
	);
}

add_action( 'block_categories_all', 'add_gutenberg_block_categories', 10, 2 );


//===================================================
// BLOCKS
//===================================================

// Check function exists.
if ( function_exists( 'acf_register_block_type' ) ) {
	add_action( 'acf/init', 'my_acf_init_block_types' );
}
function my_acf_init_block_types() {

	get_template_part( 'templates-blocks-acf/gallery/config' );
	get_template_part( 'templates-blocks-acf/video-iframe/config' );
	get_template_part( 'templates-blocks-acf/posts-grid/config' );

	//===================================================
	// IFRAME BLOCK
	//===================================================

	acf_register_block_type( array(
		'name'            => 'iframe-block',
		'title'           => __( 'Bloque Iframe', 'text-domain' ),
		'description'     => __( 'Bloque para diferentes iframes.', 'text-domain' ),
		'category'        => 'text-domain',
		'icon'            => 'media-code',
		'keywords'        => array( 'iframe' ),
		'mode'            => 'preview',
		'supports'        => array(
			'mode'   => false,
			'align'  => false,
			'anchor' => true,
		),
		'render_template' => 'templates-blocks-acf/iframe-block/iframe-block.php',
		'enqueue_assets'  => function () {
			if ( ! wp_style_is( 'iframe-block', 'enqueued' ) ) {
				wp_enqueue_style( 'iframe-block', get_stylesheet_directory_uri() . '/templates-blocks-acf/iframe-block/iframe-block.min.css' );
			}
		},
	) );


	//===================================================
	// POST SLIDER BLOCK
	//===================================================

	acf_register_block_type( array(
		'name'            => 'post_slider_block',
		'title'           => __( 'text-domain Bloque Post Slider', 'text-domain' ),
		'description'     => __( 'Bloque para slider de posts.', 'text-domain' ),
		'category'        => 'text-domain',
		'icon'            => 'slides',
		'keywords'        => array( 'post', 'articulos', 'slider' ),
		'mode'            => 'preview',
		'supports'        => array(
			'mode'   => false,
			'align'  => false,
			'anchor' => true,
		),
		'render_template' => 'templates-blocks-acf/post-slider-block/post-slider-block.php',
		'enqueue_assets'  => function () {
			if ( ! wp_script_is( 'swiper', 'enqueued' ) ) {
				if ( ! is_admin() ) {
					wp_enqueue_script( 'swiper', 'https://unpkg.com/swiper@8/swiper-bundle.min.js', '', '', true );
				}
				wp_enqueue_style( 'swiper', 'https://unpkg.com/swiper@8/swiper-bundle.min.css' );
			}
			if ( ! wp_script_is( 'post-slider-block', 'enqueued' ) && ! is_admin() ) {
				wp_enqueue_script( 'post-slider-block', get_stylesheet_directory_uri() . '/templates-blocks-acf/post-slider-block/post-slider-block.min.js', array( 'swiper' ), '', true );
			}
			if ( ! wp_style_is( 'post-slider-block', 'enqueued' ) ) {
				wp_enqueue_style( 'post-slider-block', get_stylesheet_directory_uri() . '/templates-blocks-acf/post-slider-block/post-slider-block.min.css' );
			}
		},
	) );

}


//===================================================
// BLOCKS IN CODE
//===================================================
/**
 * Render an ACF block with your chosen attrs in place
 * Allows you to use the block within PHP templates
 * Note: Block template may need to be adjusted to read this data
 */

function render_acf_block( $block_name, $block_id, $attrs = [] ) {
	$block      = acf_get_block_type( $block_name );
	$content    = '';
	$is_preview = false;

	$block['custom_id'] = $block_id;
	$block['id']        = 'block_' . $block_id;

	foreach ( $attrs as $attr => $val ) {
		$block['data'][ $attr ] = $val;
	}
	echo acf_rendered_block( $block, $content, $is_preview );
}