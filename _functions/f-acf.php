<?php

//===================================================
//ACF OPTIONS PAGE
//===================================================

if ( function_exists( 'acf_add_options_page' ) ) {

	acf_add_options_page( array(
		'page_title' => __( 'Opciones Globales', 'text-domain' ),
		'menu_title' => __( 'Opciones Globales', 'text-domain' ),
		'menu_slug'  => 'acf-options-page',
		'capability' => 'edit_posts',
		'redirect'   => false
	) );

//    acf_add_options_sub_page(array(
//        'page_title' => __('Social', 'text-domain'),
//        'menu_title' => __('Social', 'text-domain'),
//        'parent_slug' => 'acf-options-page',
//    ));
}

//===================================================
//ACF GOOGLE MAPS
//===================================================

add_action( 'acf/init', 'my_acf_init' );
function my_acf_init() {
	if ( ENVIRONMENT === 'PRODUCTION' ) {
		acf_update_setting( 'google_api_key', '' );
	} else {
		acf_update_setting( 'google_api_key', 'AIzaSyAo2xtJrMmSOfH6XbX8BBXP-DoolM3qfT0' );
	}
}


//===================================================
// ACF - READ ONLY FIELDS
//===================================================
add_filter( 'acf/load_field', 'acf_read_only' );
function acf_read_only( $field ) {
	global $pagenow;
	// FOR USER PAGE
	if ( $pagenow === 'user-edit.php' ) {
		$field['readonly'] = 1;
		$field['disabled'] = true;
//        print_r($field);
	}

	return $field;
}

//===================================================
// ACF MENU SELECT
//===================================================
/**
 * ACF Populate Select Field with Menus
 * @link https://www.advancedcustomfields.com/resources/acf-load_field/
 * @link https://www.advancedcustomfields.com/resources/dynamically-populate-a-select-fields-choices/
 *
 * Dynamically populates any ACF field with wd_nav_menus with list of navigation menus
 *
 */
add_filter( 'acf/load_field/name=menu_select', 'wd_nav_menus_load' );
function wd_nav_menus_load( $field ) {
	$menus = wp_get_nav_menus();

	if ( ! empty( $menus ) ) {
		foreach ( $menus as $menu ) {
			$field['choices'][ $menu->slug ] = $menu->name;
		}
	}

	return $field;
}

//===================================================
// GUTENBERG EDIT COLOR PALLETE
//===================================================
// Disables custom colors in block color palette.
add_theme_support( 'disable-custom-colors' );

$color_primary      = '#189aa6';
$color_primary_2    = '#00d1ff';
$color_secondary    = '#010049';
$color_black        = '#000000';
$color_white        = '#ffffff';
$color_gray         = '#d1d1d1';
$color_light - gray = '#f9f9f9';
$color_dark_gray    = '#97999b';

$colors = array(
	array(
		'name'  => __( "Primario del theme", 'text-domain' ),
		'slug'  => 'primary',
		'color' => $color_primary
	),
	array(
		'name'  => __( "Primario 2 del theme", 'text-domain' ),
		'slug'  => 'primary-2',
		'color' => $color_primary_2
	),
	array(
		'name'  => __( "Secundario del theme", 'text-domain' ),
		'slug'  => 'secondary',
		'color' => $color_secondary
	),
	array(
		'name'  => __( "Negro", 'text-domain' ),
		'slug'  => 'black',
		'color' => $color_black
	),
	array(
		'name'  => __( "Blanco", 'text-domain' ),
		'slug'  => 'white',
		'color' => $color_white
	),
	array(
		'name'  => __( "Gris", 'text-domain' ),
		'slug'  => 'gray',
		'color' => $color_gray
	),
	array(
		'name'  => __( "Gris Claro", 'text-domain' ),
		'slug'  => 'light-gray',
		'color' => $color_light - gray
	),
	array(
		'name'  => __( "Gris Oscuro", 'text-domain' ),
		'slug'  => 'dark-gray',
		'color' => $color_dark_gray
	),
);

// Adds support for editor color palette.
add_theme_support( 'editor-color-palette', $colors );

$colors_gradients = array(
	array(
		'name'     => __( 'Mitad Negro', 'tabor' ),
		'slug'     => 'half-black',
		'gradient' => 'linear-gradient(180deg,rgba(0,0,0,0) 50%,rgb(0,0,0) 50%,rgb(0,0,0) 100%)',
	),
	array(
		'name'     => __( 'Primary to Secondary', 'tabor' ),
		'slug'     => 'primary-to-secondary',
		'gradient' => 'linear-gradient(135deg, ' . esc_attr( hex_to_rgb( $color_primary ) ) . ' 0%, ' . hex_to_rgb( $color_secondary ) . ' 100%)',
	),
);

// Adds support for editor gradients presets
add_theme_support( 'editor-gradient-presets', $colors_gradients );


//===================================================
// ACF COLOR PICKER - PALETTE COLORS
//===================================================
if ( ! class_exists( 'ACF' ) ) :
// Get the colors formatted for use with Iris, Automattic's color picker
	function output_the_colors() {

		// get the colors
		$color_palette = current( (array) get_theme_support( 'editor-color-palette' ) );

		// bail if there aren't any colors found
		if ( ! $color_palette ) {
			return;
		}

		// output begins
		ob_start();

		// output the names in a string
		echo '[';
		foreach ( $color_palette as $color ) {
			echo "'" . $color['color'] . "', ";
		}
		echo ']';

		return ob_get_clean();

	}

//Add the colors into Iris - acf color picker
	add_action( 'acf/input/admin_footer', 'gutenberg_sections_register_acf_color_palette' );
	function gutenberg_sections_register_acf_color_palette() {

		$color_palette = output_the_colors();
		if ( ! $color_palette ) {
			return;
		}

		?>
			<script type="text/javascript">
				(function ($) {
					acf.add_filter('color_picker_args', function (args, $field) {

						// add the hexadecimal codes here for the colors you want to appear as swatches
						args.palettes = <?php echo $color_palette; ?>

						// return colors
						return args;

					});
				})(jQuery);
			</script>
		<?php

	}
endif;

add_action( 'acf/input/admin_head', 'gutenberg_acf_color_palette_css' );
function gutenberg_acf_color_palette_css() {
	?>
	<style>
		/* HIDE CUSTOM PICKER */
		.wp-picker-open + .wp-picker-input-wrap, .iris-square, .iris-strip {
			display: none !important;
		}

		.iris-picker {
			height: 20px !important;
			border: 0px !important;
		}

		.iris-palette {
			border-radius: 50px !important;
		}

		/* HIDE CUSTOM GRADIENT */
		.components-custom-gradient-picker {
			display: none !important;
		}
	</style>
	<?php
}

function hex_to_rgb( $color, $opacity = false ) {

	if ( empty( $color ) ) {
		return false;
	}

	if ( '#' === $color[0] ) {
		$color = substr( $color, 1 );
	}

	if ( 6 === strlen( $color ) ) {
		$hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
	} elseif ( 3 === strlen( $color ) ) {
		$hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
	} else {
		$default                 = \Go\Core\get_default_color_scheme();
		$avaliable_color_schemes = get_available_color_schemes();
		if ( isset( $avaliable_color_schemes[ $default ] ) && isset( $avaliable_color_schemes[ $default ]['primary'] ) ) {
			$default = $avaliable_color_schemes[ $default ]['primary'];
		}

		return $default;
	}

	$rgb = array_map( 'hexdec', $hex );

	if ( $opacity ) {
		if ( abs( $opacity ) > 1 ) {
			$opacity = 1.0;
		}

		$output = 'rgba(' . implode( ',', $rgb ) . ',' . $opacity . ')';

	} else {

		$output = 'rgb(' . implode( ',', $rgb ) . ')';

	}

	return esc_attr( $output );

}