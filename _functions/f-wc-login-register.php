<?php

// AVOID LOGIN ON REGISTER
add_filter('woocommerce_registration_auth_new_customer', '__return_false');


// ADD CUSTOM FIELDS REGISTER FORM
add_action('woocommerce_register_form', 'add_custom_fields_register_form', 10);
if (!function_exists('add_custom_fields_register_form')) {
    function add_custom_fields_register_form()
    {
        ?>
        <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async
                defer></script>

        <div id="google_recaptcha"></div>

        <script>
            <?php if (ENVIRONMENT === 'DEVELOPMENT' || ENVIRONMENT === 'TESTING') { ?>
						var onloadCallback = function () {
							grecaptcha.render('google_recaptcha', {
								'sitekey': '6LcyjuYZAAAAACTrexq4tAoGvY39q8M4We6nadnx'
							});
						};
            <?php } ?>
            <?php if (ENVIRONMENT === 'PRODUCTION') { ?>
						var onloadCallback = function () {
							grecaptcha.render('google_recaptcha', {
								'sitekey': '6LfkSCIaAAAAAKTn4WM9vVIg2vA7T2bnKSCuc8BQ'
							});
						};
            <?php } ?>
        </script>
        <?php

        woocommerce_form_field('register_personal_data', array(
            'type' => 'checkbox',
            'label' => '<span>' . __('Autorizzo al trattamento dei miei dati personali ai sensi dell’art. 13 del Regolamento europeo 679/2016', 'airbag-professional') . '</span>',
            'class' => array('form-row-wide', 'checkbox-wrapper'),
            'required' => true
        ), '');

    }
}


// VALIDATE CUSTOM FIELDS REGISTER FORM
add_filter('woocommerce_register_post', 'validate_custom_fields_register_form', 10, 3);
if (!function_exists('validate_custom_fields_register_form')) {
    function validate_custom_fields_register_form($username, $email, $validation_errors)
    {

//        print_r($_POST);
//        die();

        if (isset($_POST['is_login_form'])) {
            if (isset($_POST['g-recaptcha-response'])) {

                if (ENVIRONMENT === 'DEVELOPMENT' || ENVIRONMENT === 'TESTING') {
                    $data = [
                        'secret' => '6LcyjuYZAAAAAA3BAwwyXXDhq_BeJHBrcz-Y1-wc',
                        'response' => $_POST['g-recaptcha-response']
                    ];
                }else if (ENVIRONMENT === 'PRODUCTION') {
                    $data = [
                        'secret' => '6LfkSCIaAAAAAMMeVWcmIlge23CV7pMJuYL9ZcR2',
                        'response' => $_POST['g-recaptcha-response']
                    ];
                }

                $curl = curl_init();

                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));

                $response = curl_exec($curl);
                $response = json_decode($response, true);
//        print_r($response);
                if (!$response['success']) {
                    //$error_codes = $response['error-codes'];
                    $validation_errors->add('recaptcha', __('Recaptcha non è stato convalidato', 'airbag-professional'));
                }
            }

            if (!isset($_POST['register_personal_data'])) {
                $validation_errors->add('register_personal_data', __('Il trattamento dei dati non è stato autorizzato', 'airbag-professional'));
            }
        }


        return $validation_errors;
    }
}

// SAVE CUSTOM FIELDS REGISTER FORM
//add_action('woocommerce_created_customer', 'save_custom_fields_register_form');
if (!function_exists('save_custom_fields_register_form')) {
    function save_custom_fields_register_form($customer_id)
    {
    }
}

// AUTH PARA VER SI ESTA ACTIVADO EL USUARIO - EN EL wp_signon
add_filter('wp_authenticate_user', 'check_user_validation', 10, 2);
if (!function_exists('check_user_validation')) {
    function check_user_validation($user, $password)
    {
        $userID = $user->ID;

        // ACCOUNT VALIDATED - VALIDATION
        $isActivated = get_field('account_is_validated', 'user_' . $userID);
        if (!$isActivated) {
            send_custom_validation_email($user->user_email);
            $user = new WP_Error(
                'user_validation_error',
                __("Il tuo account deve essere attivato prima di poter accedere. Fare clic sul collegamento nell'e-mail di attivazione che ti è stata inviata. Se non ricevi l'e-mail di attivazione entro pochi minuti, controlla la cartella dello spam o <a class='resend-validation-mail' data-id='" . $user->ID . "' href=''>fai clic qui per inviarla di nuovo.</a>.", 'airbag-professional')
            );
        }

        // FIRST LOGIN IMPORTED USER - VALIDATION
        $firstTime = get_field('user_import_first_time', 'user_' . $userID);
        $userAuth = wp_check_password($password, $user->data->user_pass, $userID);
        if ($firstTime && !$userAuth) {
            wc_clear_notices();
            wc_add_notice(sprintf(__('Benvenuto nel nuovo sito di Airbag Professional. <a href="%s">Reimposta qui la tua password</a> per effettuare l’accesso.', 'airbag-professional'), esc_url(wp_lostpassword_url())), 'notice');
            $user = new WP_Error(
                'user_first_time_error', ''
            );
            return $user;
        } elseif ($firstTime && $userAuth) {
            update_field('user_import_first_time', 0, 'user_' . $userID);
        }

        /*
        if ($firstTime && !$userAuth) {
            /*
            $user = new WP_Error(
                'user_first_time_error',
//                sprintf(__('<strong>Error</strong>: Il tuo nome utente è stato importato nel nuovo sito. La password è stata cambiata in una casuale. Per favore cambia la tua password <a href="%s"><strong>cliccando qui</strong></a>.', 'airbag-professional'), esc_url(wp_lostpassword_url()))
                sprintf(__('Benvenuto nel nuovo sito di Airbag Professional. <a href="%s">Reimposta qui la tua password</a> per effettuare l’accesso.', 'airbag-professional'), esc_url(wp_lostpassword_url()))
            );

            wc_add_notice( sprintf(__('Benvenuto nel nuovo sito di Airbag Professional. <a href="%s">Reimposta qui la tua password</a> per effettuare l’accesso.', 'airbag-professional'), esc_url(wp_lostpassword_url())), 'notice' );
            return $user;
            exit;

        } elseif ($firstTime && $userAuth) {
            update_field('user_import_first_time', 0, 'user_' . $userID);
        }
        */

        return $user;

    }
}


//add_action( 'after_password_reset', 'action_after_password_reset' );
function action_after_password_reset($user, $new_pass)
{
    // make action magic happen here...
//
//    print_r($user);
//
//echo $user->ID. '   ';
//    echo "111";
//    die();

//    $userID = $user->ID;
//
//    // SACAR FIRST IMPORT CUANDO CAMBIA EL PASS
//    update_field('user_import_first_time', 0, 'user_'.$userID);

}

// SEND CUSTOM EMAIL FOR VALIDATION - AJAX CALL
add_action('wp_ajax_send_custom_validation_email_ajax', 'send_custom_validation_email_ajax');
add_action('wp_ajax_nopriv_send_custom_validation_email_ajax', 'send_custom_validation_email_ajax');
function send_custom_validation_email_ajax()
{
    $user_info = get_user_by('id', $_POST['user_id']);
    send_custom_validation_email($user_info->user_email);
}

// SEND CUSTOM EMAIL FOR VALIDATION
function send_custom_validation_email($user_email)
{
    $user_info = get_user_by('email', $user_email);
    $wc_emails = WC()->mailer()->get_emails();
    $wc_emails['WC_Validate_Account_Email']->trigger($user_info->ID, $user_pass = '', $password_generated = false);
}
