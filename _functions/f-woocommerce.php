<?php

// HABILITAR PISAR TEMPLATES
function mytheme_add_woocommerce_support()
{
    add_theme_support('woocommerce');
}

add_action('after_setup_theme', 'mytheme_add_woocommerce_support');
// HABILITAR PISAR TEMPLATES


//Add Revision support to WooCommerce Products
add_filter('woocommerce_register_post_type_product', 'cinch_add_revision_support');

function cinch_add_revision_support($supports)
{
    $supports['supports'][] = 'revisions';

    return $supports;
}

// PAGE SHOP TO 404
add_action('wp', 'woocommerce_disable_shop_page');
function woocommerce_disable_shop_page()
{
    global $post;
    if (is_shop() && !is_search()):
        global $wp_query;
        $wp_query->set_404();
        status_header(404);
    endif;
}

add_filter('woocommerce_return_to_shop_redirect', 'custom_woocommerce_return_to_shop_redirect', 20);
function custom_woocommerce_return_to_shop_redirect()
{
    return get_bloginfo('url');
}

// IF IS A WOOCOMMERCE PAGE
//is_realy_woocommerce_page - Returns true if on a page which uses WooCommerce templates (cart and checkout are standard pages with shortcodes and which are also included)
function is_realy_woocommerce_page()
{
    if (function_exists("is_woocommerce") && is_woocommerce()) {
        return true;
    }
    $woocommerce_keys = array("woocommerce_shop_page_id",
        "woocommerce_terms_page_id",
        "woocommerce_cart_page_id",
        "woocommerce_checkout_page_id",
        "woocommerce_pay_page_id",
        "woocommerce_thanks_page_id",
        "woocommerce_myaccount_page_id",
        "woocommerce_edit_address_page_id",
        "woocommerce_view_order_page_id",
        "woocommerce_change_password_page_id",
        "woocommerce_logout_page_id",
        "woocommerce_lost_password_page_id");

    foreach ($woocommerce_keys as $wc_page_id) {
        if (get_the_ID() == get_option($wc_page_id, 0)) {
            return true;
        }
    }
    return false;
}

// IF IS A WOOCOMMERCE PAGE

// ADD COLUMNS TO PRODUCTS LIST ADMIN PANEL
function add_product_column($columns)
{
    //add column
    $columns['attr'] = __('Attributi', 'airbag-professional');

    return $columns;
}

add_filter('manage_edit-product_columns', 'add_product_column', 10, 1);

function add_product_column_content($column, $postid)
{
    if ($column == 'attr') {

        // output variable
        $output = '';

        // Get product object
        $product = wc_get_product($postid);

        // Get Product Variations - WC_Product_Attribute Object
        $product_attributes = $product->get_attributes();

        // Not empty, contains values
        if (!empty($product_attributes)) {

            foreach ($product_attributes as $product_attribute) {
                // Get name
                $attribute_name = str_replace('pa_', '', $product_attribute->get_name());

                // Concatenate
                $output = $attribute_name . ' = ';

                // Get options
                $attribute_options = $product_attribute->get_options();

                // Not empty, contains values
                if (!empty($attribute_options)) {

                    foreach ($attribute_options as $key => $attribute_option) {
                        // WP_Term Object
                        $term = get_term($attribute_option); // <-- your term ID

                        // Not empty, contains values
                        if (!empty($term)) {
                            $term_name = $term->name;

                            // Not empty
                            if ($term_name != '') {

                                // Last loop
                                end($attribute_options);
                                if ($key === key($attribute_options)) {
                                    // Concatenate
                                    $output .= $term_name;
                                } else {
                                    // Concatenate
                                    $output .= $term_name . ', ';
                                }
                            }
                        }
                    }
                }

                echo $output . '<br>';
            }
        }
    }
}

add_action('manage_product_posts_custom_column', 'add_product_column_content', 10, 2);

// ADD COLUMNS TO PRODUCTS LIST ADMIN PANEL

// WOOCOMMERCE EMAIL PREVIEWS
add_filter('woocommerce_order_actions', 'ace_add_email_preview_order_actions', 100);
function ace_add_email_preview_order_actions($actions)
{
    global $current_user;
    wp_get_current_user();
    $current_role = $current_user->roles[0];
    if ($current_user->user_login === 'superadmin') {

        // Filter non-order emails
        $order_emails = array_filter(WC()->mailer()->get_emails(), function ($email) {
            // return strpos($email->id, 'order') !== false;
            return $email->id;
        });

        // print_r($order_emails);

        foreach ($order_emails as $k => $email) {
            $actions['preview_' . $email->id] = sprintf(__('Preview %s email'), $email->get_title());
        }
    }

    return $actions;

}

function ace_preview_order_email($order)
{

    global $current_user;
    wp_get_current_user();
    $current_role = $current_user->roles[0];
    if ($current_user->user_login === 'superadmin') {

        $email = array_reduce(WC()->mailer()->get_emails(), function ($email, $m) {
            $preview_email_id = str_replace('woocommerce_order_action_preview_', '', current_action());

            return $preview_email_id == $m->id ? $m : $email;
        });

        /** @var WC_Email $email */
        if ($email instanceof WC_Email) {
            $email->setup_locale();
            $email->object = $order;
            echo apply_filters('woocommerce_mail_content', $email->style_inline($email->get_content_html()));
            $email->restore_locale();

            die;
        }
    }
}

array_map(function ($email) {
    add_action('woocommerce_order_action_preview_' . $email->id, 'ace_preview_order_email');
}, WC()->mailer()->get_emails());

// WOOCOMMERCE EMAIL PREVIEWS


// WOOCOMMERCE PASSWORD STRENGHT

add_filter('woocommerce_min_password_strength', 'reduce_min_strength_password_requirement');
function reduce_min_strength_password_requirement($strength)
{
    // 3 => Strong (default) | 2 => Medium | 1 => Weak | 0 => Very Weak (anything).
    return 2;
}

// Second, change the wording of the password hint.
add_filter('password_hint', 'smarter_password_hint');
function smarter_password_hint($hint)
{
    $hint = __('Suggerimento: la password deve essere lunga almeno sette caratteri, contenere almeno 2 numeri, almeno 2 lettere e una maiuscola.', 'airbag-professional');
    return $hint;
}

// WOOCOMMERCE PASSWORD STRENGHT


// ADD CUSTOM TAX MARCA
//add_action( 'init', 'custom_product_tax' );

function custom_product_tax()
{
    register_taxonomy(
        'marca',
        array('product'),
        array(
            'label' => __('Marca', 'airbag-professional'),
            'rewrite' => array('slug' => 'marca'),
            'hierarchical' => true,
//            'publicly_queryable' => false
        )
    );
}

// ADD CUSTOM TAX MARCA


// GET CART TAX STATUS
function get_cart_status($cart_items)
{
    $global_tax_status = 'none';
    foreach ($cart_items as $cart_item_key => $cart_item) {
        $product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
        if ($product->get_tax_status() === 'taxable') {
            $global_tax_status = 'taxable';
            break;
        }
    }

    return $global_tax_status;
}