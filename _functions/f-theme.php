<?php

//===================================================
//  REMOVE DEFAULT POST TYPE
//===================================================

function remove_default_post_type( $args, $postType ) {
	if ( $postType === 'post' ) {
		$args['public']              = false;
		$args['show_ui']             = false;
		$args['show_in_menu']        = false;
		$args['show_in_admin_bar']   = false;
		$args['show_in_nav_menus']   = false;
		$args['can_export']          = false;
		$args['has_archive']         = false;
		$args['exclude_from_search'] = true;
		$args['publicly_queryable']  = false;
		$args['show_in_rest']        = false;
	}

	return $args;
}

add_filter( 'register_post_type_args', 'remove_default_post_type', 0, 2 );

//===================================================
//  EMAILS
//===================================================

// Function to change email address
function wpb_sender_email( $original_email_address ) {
	$protocols = array( 'https://', 'http://', 'http://www.', 'www.' );
	$domain    = str_replace( $protocols, '', get_bloginfo( 'url' ) );

	return 'no-reply@' . $domain;
}

// Function to change sender name
function wpb_sender_name( $original_email_from ) {
	return get_bloginfo( 'name' );
}

// Hooking up our functions to WordPress filters
add_filter( 'wp_mail_from', 'wpb_sender_email' );
add_filter( 'wp_mail_from_name', 'wpb_sender_name' );

//===================================================
//  SMTP
//===================================================

// SMTP Authentication
if ( ENVIRONMENT !== 'DEVELOPMENT' ) {
//do_action_ref_array('phpmailer_init', 'send_smtp_email');
	if ( function_exists( 'send_smtp_email' ) ) {
		function send_smtp_email( $phpmailer ) {
//            print_r($phpmailer);
			if ( ENVIRONMENT === 'TESTING' ) {
				$phpmailer->isSMTP();
				$phpmailer->Host       = '';
				$phpmailer->SMTPAuth   = true;
				$phpmailer->Port       = 465;
				$phpmailer->Username   = 'no-reply@.it';
				$phpmailer->Password   = '';
				$phpmailer->SMTPSecure = 'ssl';
				$phpmailer->From       = 'no-reply@.it';
				$phpmailer->FromName   = get_bloginfo( 'name' );
			} elseif ( ENVIRONMENT === 'PRODUCTION' ) {
				$phpmailer->isSMTP();
				$phpmailer->Host       = '';
				$phpmailer->SMTPAuth   = true;
				$phpmailer->Port       = 465;
				$phpmailer->Username   = 'no-reply@.com';
				$phpmailer->Password   = '';
				$phpmailer->SMTPSecure = 'ssl';
				$phpmailer->From       = 'no-reply@.com';
				$phpmailer->FromName   = get_bloginfo( 'name' );
			}
		}
	}
}

//===================================================
//  RECAPTCHA KEYS
//===================================================
/*
if (!function_exists('get_recaptcha_keys')) {
    function get_recaptcha_keys()
    {
        $recaptcha_key = '';
        $recaptcha_secret = '';
        if (ENVIRONMENT === 'DEVELOPMENT' || ENVIRONMENT === 'TESTING') { //SON DE MI MAIL
            $recaptcha_key = '6LcyjuYZAAAAACTrexq4tAoGvY39q8M4We6nadnx';
            $recaptcha_secret = '6LcyjuYZAAAAAA3BAwwyXXDhq_BeJHBrcz-Y1-wc';
        } else {
            // PRODUCCION - SON DE airbagprofessionalweb@gmail.com
            $recaptcha_key = '6LfkSCIaAAAAAKTn4WM9vVIg2vA7T2bnKSCuc8BQ';
            $recaptcha_secret = '6LfkSCIaAAAAAMMeVWcmIlge23CV7pMJuYL9ZcR2';
        }

        return array(
            'key' => $recaptcha_key,
            'secret' => $recaptcha_secret,
        );
    }
}
*/


//===================================================
//GET CURRENT LANG WPML
//===================================================

if ( function_exists( 'icl_object_id' ) ) {
	if ( ! function_exists( 'getCurrentLang' ) ) {
		function getCurrentLang() {
			if ( function_exists( 'icl_object_id' ) ) :
				return ICL_LANGUAGE_CODE;
			else:
				return 'it';
			endif;
		}
	}
}

//===================================================
//CUSTOM IMAGE SIZES
//===================================================

add_filter( 'jpeg_quality', function ( $arg ) {
	return 90;
} );

add_theme_support( 'post-thumbnails' );

//add_action('init', 'add_image_sizes');
function add_image_sizes() {
//    add_image_size('custom-full-1920', 1920, 9999, true);
//    add_image_size('custom-1400', 1400, 9999, false);
//    add_image_size('custom-medium', 600, 600, true);
}

//add_filter('image_size_names_choose', 'custom_image_sizes');
function custom_image_sizes( $sizes ) {
	return array_merge(
		$sizes, array(//'custom-full-1920' => 'Custom Full 1920',
		)
	);
}

//===================================================
// REGISTER MENUS
//===================================================

function register_my_menu() {
	register_nav_menu( 'main-menu', __( 'Main Menu', 'text-domain' ) );
	register_nav_menu( 'top-bar-menu', __( 'Top Bar Menu', 'text-domain' ) );
	register_nav_menu( 'footer-menu', __( 'Footer Menu', 'text-domain' ) );
}

//add_action('init', 'register_my_menu');

//===================================================
// ADD DEFAULT MENU CLASSES TO FUNCTION (wp_get_nav_menu_items)
//===================================================
add_filter( 'wp_get_nav_menu_items', 'prefix_nav_menu_classes', 10, 3 );

function prefix_nav_menu_classes( $items, $menu, $args ) {
	_wp_menu_item_classes_by_context( $items );

	return $items;
}

/// ===================================================
/// CUSTOM EXCERPT
/// ===================================================

function new_excerpt_more( $more ) {
	return '...';
}

add_filter( 'excerpt_more', 'new_excerpt_more' );

add_filter( 'excerpt_length', function ( $length ) {
	return 30;
} );


//===================================================
// PLUGINS DISABLE ENVIROMENTS
//===================================================
//add_action( 'admin_init', 'disable_plugins_for_enviroments' );

function disable_plugins_for_enviroments() {
	if ( ENVIRONMENT === 'DEVELOPMENT' || ENVIRONMENT === 'TESTING' ) {
		$plugins_paths = array(
			'updraftplus/updraftplus.php',
			'wordfence/wordfence.php',
		);
		deactivate_plugins( $plugins_paths );
	} else {
		$plugins_paths = array(
			'updraftplus/updraftplus.php',
			'wordfence/wordfence.php',
		);
		activate_plugins( $plugins_paths );
	}
}

//===================================================
// WP OPTIONS DISABLE ENVIROMENTS
//===================================================
add_action( 'admin_init', 'disable_options_for_enviroments' );

function disable_options_for_enviroments() {

	if ( ENVIRONMENT === 'DEVELOPMENT' || ENVIRONMENT === 'TESTING' ) {
		update_option( 'blog_public', '0' );
	} else {
		update_option( 'blog_public', '1' );
	}

}