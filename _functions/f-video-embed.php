<?php

//===================================================
// GET EMBED VIDEO PLAYER FROM URL
//===================================================

function the_youtubeVimeoURLtoPlayer( $url, $width, $height ) {
	$parsed     = parse_url( $url );
	$hostname   = $parsed['host'];
	$path       = $parsed['path'];
	$vimeo      = preg_match( "@vim@", $hostname );
	$youtube    = preg_match( "@youtube@", $hostname );
	$youtu_be   = preg_match( "@youtu.b@", $hostname );
	$video_info = array();
	//YOUTUBE.COM
	if ( ( isset( $hostname ) ) && $youtube ) {
		$query             = $parsed['query'];
		$Arr               = explode( 'v=', $query );
		$videoIDwithString = $Arr[1];
		$videoID           = substr( $videoIDwithString, 0, 11 ); // 5sRDHnTApSw
		if ( ( isset( $videoID ) ) ) {
			echo '<iframe width="' . $width . '" height="' . $height . '" src="https://www.youtube.com/embed/' . $videoID . '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowfullscreen></iframe>';
			$video_info['id']   = $videoID;
			$video_info['host'] = 'youtube';
			//return $video_info;
		}
	}
	//YOUTU.BE
	if ( ( isset( $hostname ) ) && $youtu_be ) {
		$query             = $parsed['query'];
		$Arr               = explode( 'v=', $query );
		$videoIDwithString = $Arr[1];
		$videoID           = substr( $videoIDwithString, 0, 11 ); // 5sRDHnTApSw
		if ( ( isset( $videoID ) ) ) {
			$ArrV = explode( '://youtu.be/', $path ); // from ID to end of the string
			echo '<iframe width="' . $width . '" height="' . $height . '" src="https://www.youtube.com/embed' . $ArrV[0] . '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowfullscreen></iframe>';
			$video_info['id']   = $videoID;
			$video_info['host'] = 'youtube';
			//return $video_info;
		}
	}
	//VIMEO.COM
	//https://vimeo.com/45980982
	if ( ( isset( $hostname ) ) && $vimeo ) {
		$ArrV        = explode( '://vimeo.com/', $path ); // from ID to end of the string
		$videoID     = substr( $ArrV[0], 1, 9 ); // to get video ID
		$vimdeoIDInt = intval( $videoID ); // ID must be integer
		echo '<iframe width="' . $width . '" height="' . $height . '" src="https://player.vimeo.com/video/' . $vimdeoIDInt . '"  frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
		$video_info['id']   = $videoID;
		$video_info['host'] = 'vimeo';
		//return $video_info;
	}
}

//===================================================
// GET EMBED VIDEO ID FROM URL
//===================================================

function getEmbedVideoID( $url ) {
	$parsed     = parse_url( $url );
	$hostname   = $parsed['host'];
	$path       = $parsed['path'];
	$vimeo      = preg_match( "@vim@", $hostname );
	$youtube    = preg_match( "@youtube@", $hostname );
	$youtu_be   = preg_match( "@youtu.b@", $hostname );
	$video_info = array();
	//YOUTUBE.COM
	if ( ( isset( $hostname ) ) && $youtube ) {
		$query             = $parsed['query'];
		$Arr               = explode( 'v=', $query );
		$videoIDwithString = $Arr[1];
		$videoID           = substr( $videoIDwithString, 0, 11 ); // 5sRDHnTApSw
		if ( ( isset( $videoID ) ) ) {
			return $videoID;
		}
	}
	//YOUTU.BE
	if ( ( isset( $hostname ) ) && $youtu_be ) {
		$query             = $parsed['query'];
		$Arr               = explode( 'v=', $query );
		$videoIDwithString = $Arr[1];
		$videoID           = substr( $videoIDwithString, 0, 11 ); // 5sRDHnTApSw
		if ( ( isset( $videoID ) ) ) {
			return $videoID;
		}
	}
	//VIMEO.COM
	//https://vimeo.com/45980982
	if ( ( isset( $hostname ) ) && $vimeo ) {
		$ArrV        = explode( '://vimeo.com/', $path ); // from ID to end of the string
		$videoID     = substr( $ArrV[0], 1, 9 ); // to get video ID
		$vimdeoIDInt = intval( $videoID ); // ID must be integer
		//echo '<iframe width="' . $width . '" height="' . $height . '" src="https://player.vimeo.com/video/' . $vimdeoIDInt . '"  frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
		if ( ( isset( $videoID ) ) ) {
			return $videoID;
		}
	}
}

//===================================================
// GET VIDEO DATA FROM URL
//===================================================

function getVideoDataFromUrl( $url ) {
	$returnArray = [
		'host'    => '',
		'videoID' => ''
	];
	$parts       = parse_url( $url );

	if ( isset( $parts['host'] ) ) {
		$hostname = $parts['host'];
		if ( preg_match( "@vim@", $hostname ) ) {
			$returnArray['host'] = 'vimeo';
		} else if ( preg_match( "@youtube@", $hostname ) || preg_match( "@youtu.b@", $hostname ) ) {
			$returnArray['host'] = 'youtube';
		}
	}

	if ( isset( $parts['query'] ) ) {
		parse_str( $parts['query'], $qs );
		if ( isset( $qs['v'] ) ) {
			$returnArray['videoID'] = $qs['v'];
		} else if ( isset( $qs['vi'] ) ) {
			$returnArray['videoID'] = $qs['vi'];
		}
	}

	if ( isset( $parts['path'] ) && $returnArray['videoID'] === '' ) {
		$path                   = explode( '/', trim( $parts['path'], '/' ) );
		$returnArray['videoID'] = $path[ count( $path ) - 1 ];
	}

	return $returnArray;
}

//===================================================
// GET YOUTUBE ID FROM URL
//===================================================

function getYoutubeIdFromUrl( $url ) {
	$parts = parse_url( $url );
	if ( isset( $parts['query'] ) ) {
		parse_str( $parts['query'], $qs );
		if ( isset( $qs['v'] ) ) {
			return $qs['v'];
		} else if ( isset( $qs['vi'] ) ) {
			return $qs['vi'];
		}
	}
	if ( isset( $parts['path'] ) ) {
		$path = explode( '/', trim( $parts['path'], '/' ) );

		return $path[ count( $path ) - 1 ];
	}

	return false;
}