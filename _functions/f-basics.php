<?php

//===================================================
//Hide Admin Bar
//===================================================

add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar()
{
    //if (!current_user_can('administrator') && !is_admin()) {
    show_admin_bar(false);
    //}
}

//===================================================
//Desactivar Comentarios
//===================================================

// Disable support for comments and trackbacks in post types
function df_disable_comments_post_types_support()
{
    $post_types = get_post_types();
    foreach ($post_types as $post_type) {
        if (post_type_supports($post_type, 'comments')) {
            remove_post_type_support($post_type, 'comments');
            remove_post_type_support($post_type, 'trackbacks');
        }
    }
}

add_action('admin_init', 'df_disable_comments_post_types_support');

// Close comments on the front-end
function df_disable_comments_status()
{
    return false;
}

add_filter('comments_open', 'df_disable_comments_status', 20, 2);
add_filter('pings_open', 'df_disable_comments_status', 20, 2);

// Hide existing comments
function df_disable_comments_hide_existing_comments($comments)
{
    $comments = array();

    return $comments;
}

add_filter('comments_array', 'df_disable_comments_hide_existing_comments', 10, 2);

// Remove comments page in menu
function df_disable_comments_admin_menu()
{
    remove_menu_page('edit-comments.php');
}

add_action('admin_menu', 'df_disable_comments_admin_menu');

// Redirect any user trying to access comments page
function df_disable_comments_admin_menu_redirect()
{
    global $pagenow;
    if ($pagenow === 'edit-comments.php') {
        wp_redirect(admin_url());
        exit;
    }
}

add_action('admin_init', 'df_disable_comments_admin_menu_redirect');

// Remove comments metabox from dashboard
function df_disable_comments_dashboard()
{
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}

add_action('admin_init', 'df_disable_comments_dashboard');

// Remove comments links from admin bar
function df_disable_comments_admin_bar()
{
    if (is_admin_bar_showing()) {
        remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
    }
}

add_action('init', 'df_disable_comments_admin_bar');


//===================================================
//RemoverMenus
//===================================================

function remove_menus()
{
    global $menu;
    global $current_user;
    wp_get_current_user();
    //print_r($current_user);

    $current_role = $current_user->roles[0];

    if ($current_user->user_login !== 'superadmin') {

        $restricted = array(
            __('Dashboard'),
            //__('Posts'),
            //__('Pages'),
            __('Links'),
            //__('Media'),
            __('Comments'),
            __('Users'),
            __('Tools'),
            __('Settings'),
            __('Profile'),
            //__('Appearance'),
            __('Plugins'),
        );

        end($menu);
        while (prev($menu)) {
            $value = explode(' ', $menu[key($menu)][0]);
            if (in_array($value[0] != null ? $value[0] : "", $restricted)) {
                unset($menu[key($menu)]);
            }
        }// end while


        remove_menu_page('edit.php?post_type=acf-field-group'); // ACF

        remove_submenu_page('themes.php', 'themes.php');
        //remove_submenu_page( 'themes.php', 'customize.php?return=%2Fceihval_wp%2Fwordpress%2Fwp-admin%2Fadmin.php%3Fpage%3Dintro' );
        //remove_submenu_page( 'themes.php', 'customize.php?return=%2Fceihval_wp%2Fwordpress%2Fwp-admin%2Fnav-menus.php' );
        remove_submenu_page('themes.php', 'theme-editor.php');

        // ADVANCED WOO SEARCH
        remove_menu_page('aws-options');

        // WPML
        remove_menu_page('sitepress-multilingual-cms/menu/languages.php');

        // WORDFENCE
        remove_menu_page('admin.php?page=Wordfence');

        global $submenu;
        // Appearance Menu
        unset($submenu['themes.php'][6]); // Customize

        if ($current_role == 'contributor') {
            remove_menu_page('acf-options');
        }

    }// end if
}

// add_action('admin_menu', 'remove_menus', 999);

//===================================================
//Remover Admin Bar Menu
//===================================================

add_action('admin_bar_menu', 'remove_admin_bar_menus', 999);

function remove_admin_bar_menus($wp_admin_bar)
{
    global $menu;
    global $current_user;
    wp_get_current_user();

    if ($current_user->user_login != 'superadmin') {
        $wp_admin_bar->remove_node('wp-logo');

        $wp_admin_bar->remove_node('updates');

        $wp_admin_bar->remove_node('comments');
        $wp_admin_bar->remove_node('new-content');
        $wp_admin_bar->remove_node('new-post');
        $wp_admin_bar->remove_node('new-page');
        $wp_admin_bar->remove_node('new-link');
        $wp_admin_bar->remove_node('new-media');
        $wp_admin_bar->remove_node('new-user');

        $wp_admin_bar->remove_node('user-info');
        $wp_admin_bar->remove_node('edit-profile');
    }
}

//===================================================
//Custom Admin Logo
//===================================================

// login page logo

add_action('login_head', 'custom_login_logo');
function custom_login_logo()
{
    echo '<style type="text/css">

		h1 a {
			background: url("' . get_bloginfo('template_directory') . '/assets/images/logo.png") 50% 50% no-repeat !important;
			width: 141px !important;
            height: 55px !important;
            background-size: contain !important;
		}

		body.login{
		  background: #000000;
		}

		.login label,
		.login #backtoblog a, .login #nav a{
		}

		.login label{
			color: #ffbb00 !important;
		}
		.login #backtoblog a, .login #nav a, .privacy-policy-page-link a{
			color: #ffbb00 !important;
		}

		.wp-core-ui .button.button-large{
			border-radius: 0!important;
			background-color: #ffbb00 !important;
			border: none !important;
			box-shadow: none !important;
			text-transform: uppercase!important;
			text-shadow: none!important;
			letter-spacing: 0.04em!important;
			font-weight: 700!important;
			line-height: 30px!important;
		}

		.login form .input, .login form input[type=checkbox], .login input[type=text] {
	        color: #000 !important;
	        border-color: #000 !important;
		    box-shadow: none !important;
		}

        .login form input[type=checkbox]:before {
            color: #000 !important;
        }

		.wp-core-ui .button.button-large:hover{

		}
	</style>';
}


//======================================
//>>>> BODY CLASS
//======================================

add_filter('body_class', 'browser_body_class');
function browser_body_class($classes)
{
    $classes[] = agent_detect_class();
    $classes[] = logged_in_body_class();

    return $classes;
}

function logged_in_body_class()
{
    if (is_user_logged_in())
        return 'user-logged-in';
    else
        return 'user-not-logged-in';
}

function agent_detect_class()
{
    $ua = $_SERVER["HTTP_USER_AGENT"];

    $browser = '';
    if (strlen(strstr($ua, 'Edge')) > 0) {
        $browser = 'ie-edge';
    } elseif (strlen(strstr($ua, 'Firefox')) > 0) {
        $browser = 'firefox';
    } elseif (strlen(strstr($ua, 'Chrome')) > 0) {
        $browser = 'chrome';
    } else {
        $browser = 'ie';
    }

    $system = '';
    if (strlen(strstr($ua, 'Macintosh')) > 0) {
        $system = 'mac';
    }
    if (strlen(strstr($ua, 'Windows')) > 0) {
        $system = 'win';
    }

    $mobile_device = '';
    $android = false;
    $iphone = false;
    $ipad = false;
    $mobile_phone = false;
    $mobile_tablet = false;

    if (strlen(strstr($ua, 'Android')) > 0 && strlen(strstr($ua, 'Mobile')) > 0) {
        $mobile_device = 'android';
        $android = true;
        $mobile_phone = true;

    } else if (strlen(strstr($ua, 'Android')) > 0) {
        $mobile_device = 'android-tablet';
        $android = true;
        $mobile_tablet = true;
    }
    if (strlen(strstr($ua, 'iPhone')) > 0) {
        $mobile_device = 'iphone';
        $iphone = true;
        $mobile_phone = true;
    }
    if (strlen(strstr($ua, 'iPad')) > 0) {
        $mobile_device = 'ipad';
        $ipad = true;
        $mobile_tablet = true;
    }

    $mobile = false;
    $device = 'desktop';

    if ($android or $iphone or $ipad) {
        $mobile = true;
        $device = 'mobile';
    }

    $agent_class = $browser . ' ' . $system . ' ' . $device . ' ' . $mobile_device;

    return $agent_class;
}


/// ===================================================
/// DISABLE GUTENBERG
/// ===================================================

// disable for posts
add_filter('use_block_editor_for_post', 'remove_gutenberg', 10);
function remove_gutenberg()
{
    // if( get_post_type() == 'post' || get_post_type() == 'product'){
    if (get_post_type() == 'page' || get_post_type() == 'post') {
        return true;
    } else {
        return false;
    }
}

// disable for post types
// add_filter('use_block_editor_for_post_type', '__return_false', 10);


//===================================================
// Reemplaza todos los acentos por sus equivalentes sin ellos
//===================================================
function sanear_string($string)
{

    $string = trim($string);

    $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );

    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );

    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );

    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );

    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );

    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C',),
        $string
    );

    //Esta parte se encarga de eliminar cualquier caracter extraño
    $string = str_replace(
        array(
            "\"", "¨", "º", " - ", "~",
            "#", "@", "|", "!", '"',
            "·", "$", "%", "&", "/",
            "(", ")", "?", "'", "¡",
            "¿", "[", "^", "<code>", "]",
            "+", "}", "{", "¨", "´",
            ">", "< ", ";", ",", ":",
            ".", " "
        ),
        '',
        $string
    );


    return $string;
}

//===================================================
// Get information about available image sizes
//===================================================
function get_image_sizes($size = '')
{
    $wp_additional_image_sizes = wp_get_additional_image_sizes();

    $sizes = array();
    $get_intermediate_image_sizes = get_intermediate_image_sizes();

    // Create the full array with sizes and crop info
    foreach ($get_intermediate_image_sizes as $_size) {
        if (in_array($_size, array('thumbnail', 'medium', 'large'))) {
            $sizes[$_size]['width'] = get_option($_size . '_size_w');
            $sizes[$_size]['height'] = get_option($_size . '_size_h');
            $sizes[$_size]['crop'] = (bool)get_option($_size . '_crop');
        } elseif (isset($wp_additional_image_sizes[$_size])) {
            $sizes[$_size] = array(
                'width' => $wp_additional_image_sizes[$_size]['width'],
                'height' => $wp_additional_image_sizes[$_size]['height'],
                'crop' => $wp_additional_image_sizes[$_size]['crop']
            );
        }
    }

    // Get only 1 size if found
    if ($size) {
        if (isset($sizes[$size])) {
            return $sizes[$size];
        } else {
            return false;
        }
    }
    return $sizes;
}

//===================================================
// RESPONSE PARA AJAX
//===================================================
function response($data)
{
    header("Content-type: application/json");
    echo json_encode($data);
    exit;
}
